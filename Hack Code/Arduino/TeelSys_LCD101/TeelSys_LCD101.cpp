/*!
   @file TeelSys_LCD101.cpp

   This is part of TeelSys SED1330F driver for the Arduino platform.  It is
   designed specifically to work with All Electronics LCD-101.

   TeeSys invests time and resources providing this open source code,
   please support TeeSys and open-source hardware by purchasing
   products from TeeSys!

   Written by Richard Teel and others for TeeSys LLC.

   BSD license, all text here must be included in any redistribution.

*/

#include "Arduino.h"
#include "TeelSys_LCD101.h"

#define DEBUG

#ifdef DEBUG
 #define DEBUG_PRINT(x)  Serial.print (x)
 #define DEBUG_PRINT_HEX(x)  Serial.print (x, HEX)
 #define DEBUG_PRINTLN(x)  Serial.println (x)
#else
 #define DEBUG_PRINT(x)
 #define DEBUG_PRINTLN(x)
#endif

// Constructors
TeelSys_LCD101::TeelSys_LCD101()
{
  
}

TeelSys_LCD101::TeelSys_LCD101(byte res, byte rd, byte wr, byte cs, byte a0, byte d0, byte d1, byte d2, byte d3, byte d4, byte d5, byte d6, byte d7)
{
  _res = res;
  _rd = rd;
  _wr = wr;
  _cs = cs;
  _a0 = a0;
  _d0 = d0;
  _d1 = d1;
  _d2 = d2;
  _d3 = d3;
  _d4 = d4;
  _d5 = d5;
  _d6 = d6;
  _d7 = d7;
}

// Public Methods
void TeelSys_LCD101::ClearScreen() {
  ClearScreen(WHITE);
}

void TeelSys_LCD101::ClearScreen(byte color) {
  byte backcolor = color;

  if(_background > 0) {
    if(color == 0)
      backcolor = 0xFF;
    else
      backcolor = 0x00;
  }
  
  clearTextLayer(0x20);
  clearGraphicsLayer(backcolor);
}

void TeelSys_LCD101::CursorTo(int column, int row) {
  unsigned int charsPerRow = _lcdWidth / _charWidth;
  word posAddress = _displayPage1_Start + (row * charsPerRow) + column;

  lcdWriteCommand(CSRW);
  WriteData(getWordLowByte(posAddress));
  WriteData(getWordHighByte(posAddress));
}

void TeelSys_LCD101::CursorToXY(int x, int y) {
  unsigned int charsPerRow = _lcdWidth / _charWidth;
  word byteOffset = _displayPage2_Start + (y * charsPerRow) + x;
  
  lcdWriteCommand(CSRW);
  WriteData(getWordLowByte(byteOffset));
  WriteData(getWordHighByte(byteOffset));  
}

void TeelSys_LCD101::GotoXY(int x, int y) {
  
}

void TeelSys_LCD101::Init() {
  _background = 0;

  setDefaultParameters();
  setupLcd();
  initialize();
  setupLcdMemory();
}

void TeelSys_LCD101::Init(byte background) {
  SetDisplayMode(background);

  setDefaultParameters();
  setupLcd();
  initialize();
  setupLcdMemory();
}

void TeelSys_LCD101::SetDisplayMode(byte background) {
  _background = background;
  
  if(_background != 0)
    _background = 0xFF;
}

void TeelSys_LCD101::WriteData(byte data) {
  lcdWriteCtrl(0x01);
  lcdWriteJustData(data);
  digitalWrite(_wr, HIGH); // Latch Data
  //delay(10);
}









// Private Functions
void TeelSys_LCD101::clearGraphicsLayer(byte clearColor) {
  DEBUG_PRINTLN(F("Step 7"));
  //  7 Set display off
  lcdWriteCommand(DISP_OFF);
  WriteData(0x56);
  
  // Set Start at 03E8H
  lcdWriteCommand(CSRW);
  WriteData(getWordLowByte(_displayPage2_Start));
  WriteData(getWordHighByte(_displayPage2_Start));

  unsigned int graphicsPageSize = (_displayPage2_End - _displayPage2_Start) + 1;

  // Write 00H (blank data) for 8000 bytes
  lcdWriteCommand(MWRITE);
  for(int i=0; i<graphicsPageSize; i++) {
    WriteData(clearColor);
  }
  
  debugPrint(F("graphicsPageSize = "), graphicsPageSize); 

  DEBUG_PRINTLN(F("Step 10"));
  // 10 Set cursor address
  lcdWriteCommand(CSRW);
  WriteData(0x00);
  WriteData(0x00);

  DEBUG_PRINTLN(F("Step 11"));
  // 11 Set Cursor type
  lcdWriteCommand(CSRFORM);
  WriteData(0x04);
  WriteData(0x86); 
  
  DEBUG_PRINTLN(F("Step 12"));
  // 12 Set display on
  lcdWriteCommand(DISP_ON);
  //WriteData(0x16);

  DEBUG_PRINTLN(F("Step 13"));
  // 13 Set Cursor direction - Right
  lcdWriteCommand(CSRDIR_R);
}

void TeelSys_LCD101::clearTextLayer(byte clearAsciiCode) {
  DEBUG_PRINTLN(F("Step 7"));
  //  7 Set display off
  lcdWriteCommand(DISP_OFF);
  WriteData(0x56);
  
  // Set Start at 0000H
  CursorTo(0, 0);
  //lcdWriteCommand(CSRW);
  //WriteData(getWordLowByte(_displayPage1_Start));
  //WriteData(getWordHighByte(_displayPage1_Start));

  unsigned int textPageSize = (_displayPage1_End - _displayPage1_Start) + 1;

  // Write 20H (space character) for 1000 bytes
  lcdWriteCommand(MWRITE);
  for(int i=0; i<textPageSize; i++) {
    WriteData(clearAsciiCode);
  }
  
  debugPrint(F("textPageSize = "), textPageSize); 

  Serial.println("Step 10");
  // 10 Set cursor address
  lcdWriteCommand(CSRW);
  WriteData(0x00);
  WriteData(0x00);

  Serial.println("Step 11");
  // 11 Set Cursor type
  lcdWriteCommand(CSRFORM);
  WriteData(0x04);
  WriteData(0x86); 
  
  Serial.println("Step 12");
  // 12 Set display on
  lcdWriteCommand(DISP_ON);
  //WriteData(0x16);

  Serial.println("Step 13");
  // 13 Set Cursor direction - Right
  lcdWriteCommand(CSRDIR_R);
}

void TeelSys_LCD101::debugPrint(const char *message, unsigned int parm) {
  DEBUG_PRINT(message);
  DEBUG_PRINT(F(" = "));
  DEBUG_PRINT(parm);
  DEBUG_PRINT(F(" (0x"));
  DEBUG_PRINT_HEX(parm);
  DEBUG_PRINTLN(F(")"));
}

void TeelSys_LCD101::debugPrint(const __FlashStringHelper *message, unsigned int parm) {
  DEBUG_PRINT(message);
  DEBUG_PRINT(F(" = "));
  DEBUG_PRINT(parm);
  DEBUG_PRINT(F(" (0x"));
  DEBUG_PRINT_HEX(parm);
  DEBUG_PRINTLN(F(")"));
}

unsigned int TeelSys_LCD101::divideAndRoundUp(unsigned int dividend, unsigned int divisor) {
  unsigned int quotient = dividend/divisor;
  unsigned int remainder = dividend % divisor;

  if(remainder > 0)
    return quotient + 1;
  else
    return quotient;
}

byte TeelSys_LCD101::getWordHighByte(word value) {
  return value / 256;
}

byte TeelSys_LCD101::getWordLowByte(word value) {
  return value % 256;
}

void TeelSys_LCD101::initialize() {
  bool iv = _background == 0;
  byte p1 = _lcdDualPanel << 7 | 0 << 6 | iv << 5 | 1 << 4 | _lcdDualPanel << 3 | (_charHeight > 8) << 2 | 1 << 1 | 0;
  byte p2 = 1 << 7 | (_charWidth - 1);
  byte p3 = _charHeight - 1;
  byte p4 = _lcdWidth/(divideAndRoundUp(_charWidth, 8)*8)-1;
  byte p5 = p4 + 4;
  byte p6 = _lcdHeight - 1;
  
  word horzAddressRange = _virtualWidth/((divideAndRoundUp(_charWidth, 8))*8);  
  byte p7 = horzAddressRange % 256;
  byte p8 = horzAddressRange / 256; 

  lcdReset();
  
  lcdWriteCommand(SYSTEM_SET);  // C
  WriteData(p1);  // P1 M0, M1, M2, W/S, IV, T/L, & DR
  WriteData(p2);  // P2 FX & WF
  WriteData(p3);  // P3 FY
  WriteData(p4);  // P4 (C/R) Address range covered by one line
  WriteData(p5);  // P5 (TC/R) Length of one line
  WriteData(p6);  // P6 (L/F) Frame height in lines
  WriteData(p7);  // P7 (APL)
  WriteData(p8);  // P8 (APH)

  DEBUG_PRINTLN("");
  DEBUG_PRINTLN(F("SYSTEM SET (Initialization)"));
  DEBUG_PRINTLN(F("------------------------------"));
  debugPrint(F("p1"), p1);
  debugPrint(F("p2"), p2);
  debugPrint(F("p3"), p3);
  debugPrint(F("p4"), p4);
  debugPrint(F("p5"), p5);
  debugPrint(F("p6"), p6);
  debugPrint(F("p7"), p7);
  debugPrint(F("p8"), p8);
}

void TeelSys_LCD101::lcdReset() {
  digitalWrite(_res, LOW);
  // Set init state for wr & cs
  digitalWrite(_wr, LOW);
  digitalWrite(_cs, LOW);
  delay(100);
}

void TeelSys_LCD101::lcdWriteCommand(byte command) {
  lcdWriteCtrl(0x05);
  lcdWriteJustData(command);
  digitalWrite(_wr, HIGH); // Latch Data
}

void TeelSys_LCD101::lcdWriteCtrl(byte ctrl) {
  digitalWrite(_cs, LOW);
  digitalWrite(_res, HIGH);
  
  digitalWrite(_a0, (ctrl & 0x04) == 0x04);
  digitalWrite(_wr, (ctrl & 0x02) == 0x02);
  digitalWrite(_rd, (ctrl & 0x01) == 0x01);
}

void TeelSys_LCD101::lcdWriteJustData(byte data) {
  digitalWrite(_d0, (data & 0x01) == 0x01);
  digitalWrite(_d1, (data & 0x02) == 0x02);
  digitalWrite(_d2, (data & 0x04) == 0x04);
  digitalWrite(_d3, (data & 0x08) == 0x08);
  digitalWrite(_d4, (data & 0x10) == 0x10);
  digitalWrite(_d5, (data & 0x20) == 0x20);
  digitalWrite(_d6, (data & 0x40) == 0x40);
  digitalWrite(_d7, (data & 0x80) == 0x80);
}

void TeelSys_LCD101::setDefaultParameters() {
  // Partition Memory
  word charPageScreenSize = _lcdWidth/((divideAndRoundUp(_charWidth, 8))*8) * _lcdHeight/((divideAndRoundUp(_charHeight, 8))*8);
  word graphicPageScreenSize = _virtualWidth/8 * _virtualHeight;
  word virtualScreenSize = _virtualWidth/((divideAndRoundUp(_charWidth, 8))*8) * _virtualHeight/((divideAndRoundUp(_charHeight, 8))*8);
  
  
  _displayPage1_Start = 0;
  _displayPage1_End = (virtualScreenSize * 2) - 1;
  _characterPage1_Start = _displayPage1_Start;
  _characterPage1_End = charPageScreenSize - 1;
  _characterPage3_Start = charPageScreenSize;
  _characterPage3_End = (charPageScreenSize * 2) - 1;
  
  _displayPage2_Start = _displayPage1_End + 1;
  _displayPage2_End = (_displayPage2_Start + graphicPageScreenSize) - 1;


  DEBUG_PRINTLN("");
  DEBUG_PRINTLN(F("Partition Memory"));
  DEBUG_PRINTLN(F("------------------------------"));
  
  // Check memory size
  if(_displayPage2_End > (_lcdRamKB * 1024) - 64) {
    DEBUG_PRINTLN(F("ERROR: Memory is too small!!!"));
  }
  else {
    DEBUG_PRINTLN(F("PASS: Memory size is fine"));
  }

  _cg_End = (_lcdRamKB * 1024) - 1;
  _cg_Start = _cg_End - 64;

  DEBUG_PRINTLN("");
  debugPrint(F("charPageScreenSize"), charPageScreenSize);
  debugPrint(F("graphicPageScreenSize"), graphicPageScreenSize);
  debugPrint(F("virtualScreenSize"), virtualScreenSize);
  
  debugPrint(F("_displayPage1_Start"), _displayPage1_Start);
  debugPrint(F("_displayPage1_End"), _displayPage1_End);
  debugPrint(F("_characterPage1_Start"), _characterPage1_Start);
  debugPrint(F("_characterPage1_End"), _characterPage1_End);
  debugPrint(F("_characterPage3_Start"), _characterPage3_Start);
  debugPrint(F("_characterPage3_End"), _characterPage3_End);
  
  debugPrint(F("_displayPage2_Start"), _displayPage2_Start);
  debugPrint(F("_displayPage2_End"), _displayPage2_End);
  
  debugPrint(F("_cg_Start"), _cg_Start);
  debugPrint(F("_cg_End"), _cg_End);
}

void TeelSys_LCD101::setupLcd() {
  pinMode(_res, OUTPUT);
  pinMode(_rd, OUTPUT);
  pinMode(_wr, OUTPUT);
  pinMode(_cs, OUTPUT);
  pinMode(_a0, OUTPUT);
  pinMode(_d0, OUTPUT);
  pinMode(_d1, OUTPUT);
  pinMode(_d2, OUTPUT);
  pinMode(_d3, OUTPUT);
  pinMode(_d4, OUTPUT);
  pinMode(_d5, OUTPUT);
  pinMode(_d6, OUTPUT);
  pinMode(_d7, OUTPUT);
}

void TeelSys_LCD101::setupLcdMemory() {
  DEBUG_PRINTLN(F("Step 4"));
  //  4 Set display start address and display regions
  lcdWriteCommand(SCROLL);
  WriteData(getWordLowByte(_displayPage1_Start));  // P1 (SAD 1 L)
  WriteData(getWordHighByte(_displayPage1_Start)); // P2 (SAD 1 H)

  WriteData(_virtualHeight); // P3 (SL 1)
  
  WriteData(getWordLowByte(_displayPage2_Start));  // P4 (SAD 2 L)
  WriteData(getWordHighByte(_displayPage2_Start)); // P5 (SAD 2 H)

  WriteData(_virtualHeight); // P6 (SL 2)
  
  WriteData(getWordLowByte(_characterPage3_Start));  // P7 (SAD 3 L)
  WriteData(getWordHighByte(_characterPage3_Start)); // P8 (SAD 3 H)

  debugPrint("\tP1 (SAD 1 L): ", getWordLowByte(_displayPage1_Start));
  debugPrint("\tP2 (SAD 1 H): ", getWordHighByte(_displayPage1_Start));
  debugPrint("\tP3 (SL 1): ", _virtualHeight);
  debugPrint("\tP4 (SAD 2 L): ", getWordLowByte(_displayPage2_Start));
  debugPrint("\tP5 (SAD 2 H): ", getWordHighByte(_displayPage2_Start));
  debugPrint("\tP6 (SL 2): ", _virtualHeight);
  debugPrint("\tP7 (SAD 3 L): ", getWordLowByte(_characterPage3_Start));
  debugPrint("\tP8 (SAD 3 H): ", getWordHighByte(_characterPage3_Start));

  //  5 Set Horizontal Scroll position
  lcdWriteCommand(HDOT_SCR);
  WriteData(0x00);

  //  6 Set display overlay format
  lcdWriteCommand(OVLAY);
  WriteData(0x01);
}

