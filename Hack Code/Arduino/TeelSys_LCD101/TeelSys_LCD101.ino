#include <Arduino.h>
#include "TeelSys_LCD101.h"

TeelSys_LCD101 lcd = TeelSys_LCD101();
// TeelSys_LCD101 lcd = TeelSys_LCD101(8, 9, 10, 11, 12, 14, 15, 2, 3, 4, 5, 6, 7);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println(F("Starting TeelSys_LCD101 Library Test"));
  delay(2000);// Give reader a chance to see the output.

  lcd.Init();
  lcd.ClearScreen();
  lcd.CursorTo(10, 10);
  //lcd.WriteData(0x41);
  //lcd.print(F("Hello World");
}

void loop() {
  // put your main code here, to run repeatedly:
  
}
