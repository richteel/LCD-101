/*!
   @file TeelSys_LCD101.h

   This is part of TeelSys SED1330F driver for the Arduino platform.  It is
   designed specifically to work with All Electronics LCD-101.

   TeeSys invests time and resources providing this open source code,
   please support TeeSys and open-source hardware by purchasing
   products from TeeSys!

   Written by Richard Teel and others for TeeSys LLC.

   BSD license, all text here must be included in any redistribution.

*/
#ifndef TeelSys_LCD101_h
  #define TeelSys_LCD101_h
#endif

#include "Arduino.h"

#ifndef SYSTEM_SET // LCD Comands
  #define SYSTEM_SET  0x40
  #define SLEEP_IN    0x53
  #define DISP_OFF    0x58
  #define DISP_ON     0x59
  #define SCROLL      0x44
  #define CSRFORM     0x5D
  #define CGRAM_ADR   0x5C
  #define CSRDIR_R    0x4C
  #define CSRDIR_L    0x4D
  #define CSRDIR_U    0x4E
  #define CSRDIR_D    0x4F
  #define HDOT_SCR    0x5A
  #define OVLAY       0x5B
  #define CSRW        0x46
  #define CSRR        0x47
  #define MWRITE      0x42
  #define MREAD       0x43
#endif

#ifndef NON-INVERTED
  #define NON_INVERTED  0
  #define INVERTED      255
  #define WHITE         0
  #define BLACK         255
#endif

class TeelSys_LCD101 {
  public:
    // Constructors
    TeelSys_LCD101();
    TeelSys_LCD101(byte res, byte rd, byte wr, byte cs, byte a0, byte d0, byte d1, byte d2, byte d3, byte d4, byte d5, byte d6, byte d7);
    // Functions
    void ClearScreen();
    void ClearScreen(byte color);
    void CursorTo(int column, int row);
    void CursorToXY(int x, int y);
    void GotoXY(int x, int y);
    void Init();
    void Init(byte background);
    Print;
    void SetDisplayMode(byte background);
    void WriteData(byte data);
    
  private:
    // LCD Pins
    byte _d0 = 14;
    byte _d1 = 15;
    byte _d2 = 2;
    byte _d3 = 3;
    byte _d4 = 4;
    byte _d5 = 5;
    byte _d6 = 6;
    byte _d7 = 7;
    byte _res = 8;
    byte _rd = 9;
    byte _wr = 10;
    byte _cs = 11;
    byte _a0 = 12;
    
    // LCD Parameters
    unsigned int _lcdWidth = 256;
    unsigned int _lcdHeight = 128;
    bool _lcdDualPanel = 0;
    unsigned int _lcdRamKB = 8;
    byte _charWidth = 8;
    byte _charHeight = 8;
    byte _lineSpacing = 2;
    unsigned int _virtualWidth = _lcdWidth * 1;
    unsigned int _virtualHeight = _lcdHeight * 1;

    // Memory Addresses
    word _displayPage1_Start = 0;
    word _displayPage1_End = 0;
    word _characterPage1_Start = 0;
    word _characterPage1_End = 0;
    word _characterPage3_Start = 0;
    word _characterPage3_End = 0;
    
    word _displayPage2_Start = 0;
    word _displayPage2_End = 0;

    word _cg_Start = 0;
    word _cg_End = 0;

    // Misc
    byte _background = 0;

    // Private Functions
    void clearGraphicsLayer(byte clearColor);
    void clearTextLayer(byte clearAsciiCode);
    void debugPrint(const char *message, unsigned int);
    void debugPrint(const __FlashStringHelper *message, unsigned int parm);
    unsigned int divideAndRoundUp(unsigned int dividend, unsigned int divisor);
    byte getWordHighByte(word value);
    byte getWordLowByte(word value);
    void initialize();
    void lcdReset();
    void lcdWriteCommand(byte command);
    void lcdWriteCtrl(byte ctrl);
    void lcdWriteJustData(byte data);
    void setDefaultParameters();
    void setupLcd();
    void setupLcdMemory();
};
