void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println("Hello world");
  delay(2000);// Give reader a chance to see the output.
}

void loop() {
  // put your main code here, to run repeatedly:
  byte data = 0x00;
  byte rdata = 0x1;

  for(int idx=0; idx < 8; idx++) {
    data = data | (rdata << idx);
    Serial.print(idx);
    Serial.print(" - ");
    Serial.println(data);
    delay(500);
  }
  
}
