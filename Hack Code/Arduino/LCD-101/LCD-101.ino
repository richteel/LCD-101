#include <Arduino.h>
#include <SPI.h>
#include <U8g2lib.h>

/* Constructor */
// U8G2_UC1701_DOGS102_1_4W_SW_SPI u8g2(U8G2_R0, /* clock=*/ 13, /* data=*/ 11, /* cs=*/ 10, /* dc=*/ 9, /* reset=*/ 8);

#define rotation 0
#define d0 0
#define d1 1
#define d2 2
#define d3 3
#define d4 4
#define d5 5
#define d6 6
#define d7 7
#define cs 11
#define reset 8
#define enable 10
#define dc 12


// U8G2_SED1330_240X128_F_8080 u8g2(rotation, d0, d1, d2, d3, d4, d5, d6, d7, enable, cs, dc, reset);
U8G2_SED1330_240X128_1_6800 u8g2(rotation, d0, d1, d2, d3, d4, d5, d6, d7, enable, cs, dc, reset);


/* u8g2.begin() is required and will sent the setup/init sequence to the display */
void setup(void) {
  u8g2.begin();
}

/* draw something on the display with the `firstPage()`/`nextPage()` loop*/
void loop(void) {
  u8g2.firstPage();
  do {
    u8g2.setFont(u8g2_font_ncenB14_tr);
    u8g2.drawStr(0,20,"Hello World!");
  } while ( u8g2.nextPage() );
  delay(1000);
}
