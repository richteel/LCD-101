#include <Arduino.h>

#define d0 0
#define d1 1
#define d2 2
#define d3 3
#define d4 4
#define d5 5
#define d6 6
#define d7 7
#define res 8
#define rd 9
#define wr 10
#define cs 11
#define a0 12

void setup() {
  // put your setup code here, to run once:
  // Set pins for output
  pinMode(d0, OUTPUT);
  pinMode(d1, OUTPUT);
  pinMode(d2, OUTPUT);
  pinMode(d3, OUTPUT);
  pinMode(d4, OUTPUT);
  pinMode(d5, OUTPUT);
  pinMode(d6, OUTPUT);
  pinMode(d7, OUTPUT);
  pinMode(res, OUTPUT);
  pinMode(rd, OUTPUT);
  pinMode(wr, OUTPUT);
  pinMode(cs, OUTPUT);
  pinMode(a0, OUTPUT);


  // RESET
  digitalWrite(res, LOW);
  // Set init state for wr & cs
  digitalWrite(wr, LOW);
  digitalWrite(cs, LOW);
  delay(50);

  // Initialize LCD Sequence
  lcd_write(0x40, 0x05);  // C
  lcd_write(0xBA, 0x01);  // P1
  lcd_write(0X87, 0x01);  // P2
  lcd_write(0x0F, 0x01);  // P3
  lcd_write(0x20, 0x01);  // P4
  lcd_write(0x22, 0x01);  // P5
  lcd_write(0x7F, 0x01);  // P6
  lcd_write(0x00, 0x01);  // P7
  lcd_write(0x01, 0x01);  // P8
  
}

void loop() {
  // put your main code here, to run repeatedly:
  lcd_write(0x58, 0x05);
  delay(1000);
  lcd_write(0x59, 0x05);
  delay(1000);
}

void lcd_write(byte data, byte ctrl) {
  lcd_write_data(data);
  lcd_write_ctrl(ctrl);
  //delay(500);
  digitalWrite(wr, HIGH); // Latch Data
}

void lcd_write_ctrl(byte ctrl) {
  digitalWrite(cs, LOW);
  digitalWrite(res, HIGH);
  digitalWrite(a0, ctrl & 0x04);
  digitalWrite(wr, ctrl & 0x02);
  digitalWrite(rd, ctrl & 0x01);
}

void lcd_write_data(byte data) {
  digitalWrite(d7, data & 0x80);
  digitalWrite(d6, data & 0x40);
  digitalWrite(d5, data & 0x20);
  digitalWrite(d4, data & 0x10);
  digitalWrite(d3, data & 0x08);
  digitalWrite(d2, data & 0x04);
  digitalWrite(d1, data & 0x02);
  digitalWrite(d0, data & 0x01);
}

