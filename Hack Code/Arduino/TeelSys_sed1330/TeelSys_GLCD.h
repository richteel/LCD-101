

#include "TeelSys_sed1330.h" 
#include "Print.h" // used when deriving this class in Arduino

#ifndef TeelSys_GLCD_h
  #define TeelSys_GLCD_h
#endif

class TeelSys_GLCD : public Print
{
  public:
    // Constructors
    TeelSys_GLCD();
    TeelSys_GLCD(byte i2cAddress);
    TeelSys_GLCD(byte res, byte rd, byte wr, byte cs, byte a0, byte d0, byte d1, byte d2, byte d3, byte d4, byte d5, byte d6, byte d7);

    // Support for the print functions
    virtual size_t write(uint8_t);
    
    // Functions
    void ClearScreen();
    void ClearScreen(byte color);
    void CursorTo(int column, int row);
    void CursorToXY(int x, int y);
    void GotoXY(int x, int y);
    void Init();
    void Init(byte background);
    void SetDisplayMode(byte background);

  private:
    TeelSys_sed1330 _lcd;
    
    // LCD Parameters
    unsigned int _lcdWidth = 256;
    unsigned int _lcdHeight = 128;
    bool _lcdDualPanel = 0;
    unsigned int _lcdRamKB = 8;
    byte _charWidth = 8;
    byte _charHeight = 8;
    byte _lineSpacing = 2;
    unsigned int _virtualWidth = _lcdWidth * 1;
    unsigned int _virtualHeight = _lcdHeight * 1;

    // Memory Addresses
    word _displayPage1_Start = 0;
    word _displayPage1_End = 0;
    word _characterPage1_Start = 0;
    word _characterPage1_End = 0;
    word _characterPage3_Start = 0;
    word _characterPage3_End = 0;
    
    word _displayPage2_Start = 0;
    word _displayPage2_End = 0;

    word _cg_Start = 0;
    word _cg_End = 0;

    // Misc
    byte _background = 0;
    

    // Private Functions
    void clearGraphicsLayer(byte clearColor);
    void clearTextLayer(byte clearAsciiCode);
    void debugPrint(const char *message, unsigned int);
    void debugPrint(const __FlashStringHelper *message, unsigned int parm);
    unsigned int divideAndRoundUp(unsigned int dividend, unsigned int divisor);
    byte getWordHighByte(word value);
    byte getWordLowByte(word value);
    void initialize();
    void setDefaultParameters();
    void setupLcdMemory();
};
