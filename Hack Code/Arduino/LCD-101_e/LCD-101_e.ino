#include <Arduino.h>

// All Electronics LCD-101
// HG25504 with SED1330F

// LCD Pins
#define d0 14
#define d1 15
#define d2 2
#define d3 3
#define d4 4
#define d5 5
#define d6 6
#define d7 7
#define res 8
#define rd 9
#define wr 10
#define cs 11
#define a0 12

// LCD Comands
#define SYSTEM_SET  0x40
#define SLEEP_IN    0x53
#define DISP_OFF    0x58
#define DISP_ON     0x59
#define SCROLL      0x44
#define CSRFORM     0x5D
#define CGRAM_ADR   0x5C
#define CSRDIR_R    0x4C
#define CSRDIR_L    0x4D
#define CSRDIR_U    0x4E
#define CSRDIR_D    0x4F
#define HDOT_SCR    0x5A
#define OVLAY       0x5B
#define CSRW        0x46
#define CSRR        0x47
#define MWRITE      0x42
#define MREAD       0x43

// LCD Parameters
#define LCD_RES_W 256
#define LCD_RES_H 128
#define CHAR_BITS_WIDE 8  
#define CHARS_PER_LINE  32//8 bit
#define TEXT_ROWS 16

void setup() {
  // put your setup code here, to run once:
  // Set pins for output
  Set_Data_For_Output();
  pinMode(res, OUTPUT);
  pinMode(rd, OUTPUT);
  pinMode(wr, OUTPUT);
  pinMode(cs, OUTPUT);
  pinMode(a0, OUTPUT);

  lcd_reset();

  // Initialize LCD Sequence
  lcd_write_command(SYSTEM_SET);  // C
  lcd_write_data(0x32);  // P1 (Was 0x32 / 0x30)
                            /*
                             * b0 M0  0 - Internal CG ROM
                             * b1 M1  1 - 64 CG RAM + CG RAM2
                             * b2 M2  0 - 8-pixel character height external CG
                             * b3 W/S 0 - Single-panel drive
                             * b4     1
                             * b5 IV  1 - No screen top-line correction (no offset)
                             * b6 T/L 0 - LCD mode
                             * b7 DR  0 - Normal operation
                             */
  lcd_write_data(0X07);  // P2 (Was 007 not cleared with 0x80)
                            /*
                             * b0 FX  1 - char
                             * b1 FX  1 - width
                             * b2 FX  1 - set to 8 bits
                             * b3 FX  0 ?
                             * b4     0
                             * b5     0 
                             * b6     0 
                             * b7 WF  0 - 16-line AC drive
                             */
  lcd_write_data(0x07);  // P3 (Was 0x0F /0x07)
                            /*
                             * b0 FY  1 - char
                             * b1 FY  1 - height
                             * b2 FY  1 - set to
                             * b3 FY  0 - 8 pixels
                             * b4     0
                             * b5     0 
                             * b6     0 
                             * b7     0
                             */
  lcd_write_data(CHARS_PER_LINE-1);  // P4 (C/R) Address range covered by one line (Was 31)
  lcd_write_data(CHARS_PER_LINE+6);  // P5 (TC/R) Length of one line (Was 35 (+3) / +5)
  lcd_write_data(127);  // P6 (L/F) Frame height in lines (Was 15 / 128)
  lcd_write_data(CHARS_PER_LINE-1);  // P7 (APL) (Was 31)
  lcd_write_data(0);  // P8 (APH) (Was 0x00)
  
  Serial.begin(9600);
  Serial.println("Hello world");
  delay(2000);// Give reader a chance to see the output.

  

  //test();
  test2();
}

void loop() {
  // put your main code here, to run repeatedly:
  /*
  lcd_write_command(DISP_ON);
  lcd_write_data(0xFF);
  delay(1000);
  lcd_write_command(DISP_OFF);
  lcd_write_data(0xFF);
  delay(1000);
  
  lcd_write_data(0x4D);
  lcd_write_data(0X49);
  lcd_write_data(0X46);
  lcd_write_data(0X52);
  lcd_write_data(0X4F);
  */

  //Box(20, 20, 40, 40, HIGH);
}

void test2() {
  Serial.println("Running Test 2");

  Serial.println("Step  4");
  //  4 Set display start address and display regions
  lcd_write_command(SCROLL);
  lcd_write_data(0x00);
  lcd_write_data(0x00);
  lcd_write_data(0x40);
  lcd_write_data(0x00);
  lcd_write_data(0x10);
  lcd_write_data(0x40);
  lcd_write_data(0x00);
  lcd_write_data(0x04);
  lcd_write_data(0x00);
  lcd_write_data(0x30);
  
  Serial.println("Step  5");
  //  5 Set Horizontal Scroll position
  lcd_write_command(HDOT_SCR);
  lcd_write_data(0x00);
  
  Serial.println("Step  6");
  //  6 Set display overlay format
  lcd_write_command(OVLAY);
  lcd_write_data(0x01);
  
  Serial.println("Step  7");
  //  7 Set display off
  lcd_write_command(DISP_OFF);
  lcd_write_data(0x56);

  Serial.println("Step  8");
  //  8 Clear data in first layer with 20H (space character)
  Clear_Text_Layer();

  Serial.println("Step  9");
  //  9 Clear data in second layer with 00H (blank data)
  Clear_Graphics_Layer();

  Serial.println("Step 10");
  // 10 Set cursor address
  lcd_write_command(CSRW);
  lcd_write_data(0x00);
  lcd_write_data(0x00);

  Serial.println("Step 11");
  // 11 Set Cursor type
  lcd_write_command(CSRFORM);
  lcd_write_data(0x04);
  lcd_write_data(0x86); 
  
  Serial.println("Step 12");
  // 12 Set display on
  lcd_write_command(DISP_ON);
  //lcd_write_data(0x16);

  Serial.println("Step 13");
  // 13 Set Cursor direction - Right
  lcd_write_command(CSRDIR_R);

  Serial.println("Step 14");
  // 14 Write characters
  lcd_write_command(MWRITE);
  lcd_write_data(0x20);
  lcd_write_data(0x45);
  lcd_write_data(0x50);
  lcd_write_data(0x53);
  lcd_write_data(0x4F);
  lcd_write_data(0x4E);

  Serial.println("Step 15");
  // 15 Set cursor address
  lcd_write_command(CSRW);
  lcd_write_data(0x00);
  lcd_write_data(0x10);

  Serial.println("Step 16");
  // 16 Set Cursor direction - Down
  lcd_write_command(CSRDIR_D);
  
  Serial.println("Step 17");
  // 17 Fill square
  lcd_write_command(MWRITE);
  for(int i0=0; i0<9; i0++) {
    lcd_write_data(0xFF);
  }

  Serial.println("Step 18");
  // 18 Set cursor address
  lcd_write_command(CSRW);
  lcd_write_data(0x01);
  lcd_write_data(0x10);

  Serial.println("Step 19");
  // 19 Fill square
  lcd_write_command(MWRITE);
  for(int i0=0; i0<9; i0++) {
    lcd_write_data(0xFF);
  }

  Serial.println("Step 20");
  // 20 Set cursor address
  lcd_write_command(CSRW);
  lcd_write_data(0x02);
  lcd_write_data(0x10);

  Serial.println("Step 21");
  // 21 Fill square
  lcd_write_command(MWRITE);
  for(int i0=0; i0<9; i0++) {
    lcd_write_data(0xFF);
  }

  Serial.println("Step 22");
  // 22 Set cursor address
  lcd_write_command(CSRW);
  lcd_write_data(0x03);
  lcd_write_data(0x10);

  Serial.println("Step 23");
  // 23 Fill square
  lcd_write_command(MWRITE);
  for(int i0=0; i0<9; i0++) {
    lcd_write_data(0xFF);
  }

  Serial.println("Step 24");
  // 24 Set cursor address
  lcd_write_command(CSRW);
  lcd_write_data(0x04);
  lcd_write_data(0x10);

  Serial.println("Step 25");
  // 25 Fill square
  lcd_write_command(MWRITE);
  for(int i0=0; i0<9; i0++) {
    lcd_write_data(0xFF);
  }

  Serial.println("Step 26");
  // 26 Set cursor address
  lcd_write_command(CSRW);
  lcd_write_data(0x05);
  lcd_write_data(0x10);

  Serial.println("Step 27");
  // 27 Fill square
  lcd_write_command(MWRITE);
  for(int i0=0; i0<9; i0++) {
    lcd_write_data(0xFF);
  }

  Serial.println("Step 28");
  // 28 Set cursor address
  lcd_write_command(CSRW);
  lcd_write_data(0x06);
  lcd_write_data(0x10);

  Serial.println("Step 29");
  // 29 Fill square
  lcd_write_command(MWRITE);
  for(int i0=0; i0<9; i0++) {
    lcd_write_data(0xFF);
  }

  Serial.println("Step 30");
  // 30 Set cursor address
  lcd_write_command(CSRW);
  lcd_write_data(0x00);
  lcd_write_data(0x01);

  Serial.println("Step 31");
  // 31 Set Cursor direction - Right
  lcd_write_command(CSRDIR_R);

  Serial.println("Step 32");
  // 32 Write more text
  lcd_write_command(MWRITE);
  lcd_write_data(0x44);
  lcd_write_data(0x6F);


  
  
  
  Serial.println("Done with Test 2");
}

void Clear_Text_Layer() {
  // Set Start at 0000H
  lcd_write_command(CSRW);
  lcd_write_data(0x00);
  lcd_write_data(0x00);

  // Write 20H (space character) for 1000 bytes
  lcd_write_command(MWRITE);
  for(int i=0; i<1000; i++) {
    lcd_write_data(0x20);
  }
}

void Clear_Graphics_Layer() {
  // Set Start at 03E8H
  lcd_write_command(CSRW);
  lcd_write_data(0x03);
  lcd_write_data(0xE8);

  // Write 00H (blank data) for 8000 bytes
  lcd_write_command(MWRITE);
  for(int i=0; i<8000; i++) {
    lcd_write_data(0x00);
  }
}

void test(){
  Serial.println("Running Test");
  
  // Set display overlay format
  lcd_write_command(OVLAY);
  lcd_write_data(0x00);

  // Set display start address and display regions
  lcd_write_command(SCROLL);
  lcd_write_data(0x00);
  lcd_write_data(0x00);
  lcd_write_data(0xC8);
  lcd_write_data(0xE8);
  lcd_write_data(0x03);
  lcd_write_data(0xC8);
  /*
  */
  lcd_write_data(0x00);
  lcd_write_data(0x00);
  lcd_write_data(0x00);
  lcd_write_data(0x00);

  // Set Cursor type
  lcd_write_command(CSRFORM);
  lcd_write_data(0x04);
  lcd_write_data(0x86);

  // Set Cursor direction - Right
  lcd_write_command(CSRDIR_R);
  
  // Set Horizontal Scroll position
  lcd_write_command(HDOT_SCR);
  lcd_write_data(0x00);
  
  // Set display on
  lcd_write_command(DISP_ON);
  lcd_write_data(0x16);

  Serial.println("Clear LCD");
  clearLCD(); //completely clears LCD
  


  /*
  // Print MICRO
  lcd_write_command(MWRITE);
  lcd_write_data(0x4D);
  lcd_write_data(0X49);
  lcd_write_data(0X46);
  lcd_write_data(0X52);
  lcd_write_data(0X4F);
  */

  //Serial.println("Write string");
  //WriteString("Hello World");

  //Serial.println("Draw Box High");
  //Box(10, 10, 20, 20, HIGH);
  
  //Serial.println("Draw Box Low");
  //Box(100, 100, 120, 120, LOW);

  Serial.println("Diagonal Line");
  //Pixel(unsigned char x, unsigned char y, unsigned char i)
  for(int x=0; x < LCD_RES_H; x++) {
    Pixel(x, x, HIGH);
  }
  Serial.println("Done");
}

void lcd_reset() {
  digitalWrite(res, LOW);
  // Set init state for wr & cs
  digitalWrite(wr, LOW);
  digitalWrite(cs, LOW);
  delay(50);
}

void lcd_write_command(byte command) {
  lcd_write_ctrl(0x05);
  lcd_write_justdata(command);
  digitalWrite(wr, HIGH); // Latch Data
  //delay(10);
}

void lcd_write_data(byte data) {
  lcd_write_ctrl(0x01);
  lcd_write_justdata(data);
  digitalWrite(wr, HIGH); // Latch Data
  //delay(10);
}

void lcd_write_ctrl(byte ctrl) {
  digitalWrite(cs, LOW);
  digitalWrite(res, HIGH);
  
  digitalWrite(a0, ctrl & 0x04);
  digitalWrite(wr, ctrl & 0x02);
  digitalWrite(rd, ctrl & 0x01);
}

void lcd_write_justdata(byte data) {
  digitalWrite(d7, data & 0x80);
  digitalWrite(d6, data & 0x40);
  digitalWrite(d5, data & 0x20);
  digitalWrite(d4, data & 0x10);
  digitalWrite(d3, data & 0x08);
  digitalWrite(d2, data & 0x04);
  digitalWrite(d1, data & 0x02);
  digitalWrite(d0, data & 0x01);
}

void clearLCD(void)
{
 unsigned short i;
  MoveCursor(0); //set cursor to the beginning of the character layer
  lcd_write_command(MWRITE);
  
  for (i=0; i<1000; i++) 
  {
    lcd_write_data(0x20); // write " " to character memory
  }
  
  Blank(0,0,32,128);
}


void MoveCursor(int addr)
{
  lcd_write_command(CSRW);
  lcd_write_data(addr);
  lcd_write_data(addr>>8);
}

void Blank(unsigned char x, unsigned char y, unsigned char w, unsigned char h)
{
  unsigned char xi; 
  unsigned int address;

  address=1000+(unsigned int)y*32+x;
  
  MoveCursor(address); 
  lcd_write_command(MWRITE);

  while(h--)
  {
    xi=w;
    while(xi--)
    {    
      lcd_write_data(0);
    }
    
    address+=32;        
    MoveCursor(address);
    lcd_write_command(MWRITE);
  }
  
  return;
}

void Box(unsigned char x1, unsigned char y1, unsigned char x2, unsigned char y2, unsigned char i)
{
  Serial.println("Start: Draw Box");
  Serial.print("x1: ");
  Serial.print(x1);
  Serial.print(" y1: ");
  Serial.print(y1);
  Serial.print(" x2: ");
  Serial.print(x2);
  Serial.print(" y2: ");
  Serial.print(y2);
  Serial.print(" i: ");
  Serial.println(i);
  Line(x1,y1,x1,y2,i);
  Line(x1,y1,x2,y1,i);
  Line(x2,y1,x2,y2,i);
  Line(x1,y2,x2,y2,i);
  Serial.println("End: Draw Box");
  return;
} 

void Line(unsigned char x1, unsigned char y1, unsigned char x2, unsigned char y2, unsigned char i)
{
        float x;
        float xi;
        float y;
        float yi;
        signed int steps;
        signed int deltax;
        signed int deltay;
        
  x=x1;
  y=y1;
  
        deltax=(signed int)x1-(signed int)x2;
        if (deltax<0)
          deltax*=-1;
          
        deltay=(signed int)y1-(signed int)y2;
        if (deltay<0)
          deltay*=-1;
          
        steps=deltax+deltay;
        
        xi=((float)x2-x)/steps;
        yi=((float)y2-y)/steps;
        
        do
        {
          Pixel((unsigned char)x, (unsigned char)y, i);
          x+=xi;
          y+=yi;
        }while(steps--);
        
    return;
}      

void Pixel(unsigned char x, unsigned char y, unsigned char i)
{
  unsigned char data; 
  unsigned int address;
  address=1000+(unsigned int)y*32+(x/8);
  data=ReadData(address);
  if (i)
    data|=(1<<(7-x%8));  
  else
    data&=(255-(1<<(7-x%8)));

  MoveCursor(address);
  lcd_write_command(MWRITE);
  lcd_write_data(data);  

  return;
}

byte ReadData(unsigned int address)
{
  unsigned char data = 0x00;
  MoveCursor(address);
  lcd_write_command(MREAD); //Set LCD to be ready to be read
  
  // Set pins for input
  Set_Data_For_Input();
  digitalWrite(wr, HIGH);
  digitalWrite(a0, HIGH);
  digitalWrite(rd, LOW);

  delay(10);

  data = data | (digitalRead(d0) | 0);
  data = data | (digitalRead(d1) | 1);
  data = data | (digitalRead(d2) | 2);
  data = data | (digitalRead(d3) | 3);
  data = data | (digitalRead(d4) | 4);
  data = data | (digitalRead(d5) | 5);
  data = data | (digitalRead(d6) | 6);
  data = data | (digitalRead(d7) | 7);

  digitalWrite(rd, HIGH);
  digitalWrite(a0, LOW);
  
  Set_Data_For_Output();

  // Serial.print("Read: ");
  // Serial.println(data);
  
  return data;
}

void Set_Data_For_Input() {
  pinMode(d0, INPUT);
  pinMode(d1, INPUT);
  pinMode(d2, INPUT);
  pinMode(d3, INPUT);
  pinMode(d4, INPUT);
  pinMode(d5, INPUT);
  pinMode(d6, INPUT);
  pinMode(d7, INPUT);
}

void Set_Data_For_Output() {
  pinMode(d0, OUTPUT);
  pinMode(d1, OUTPUT);
  pinMode(d2, OUTPUT);
  pinMode(d3, OUTPUT);
  pinMode(d4, OUTPUT);
  pinMode(d5, OUTPUT);
  pinMode(d6, OUTPUT);
  pinMode(d7, OUTPUT);
}

void WriteString(char *input)
{
  char i=0;
  lcd_write_command(MWRITE);
  while (input[i]!=0)
  {
    lcd_write_data(input[i]);
    i++;
  }
}
