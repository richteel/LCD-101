void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println("Hello world");
  delay(2000);// Give reader a chance to see the output.
  
  
  Serial.print("HIGH ");
  Serial.println(HIGH);
  
  Serial.print("LOW ");
  Serial.println(LOW);
  
  Serial.print("0x80 & 0x81 ");
  Serial.println(0x80 & 0x81);
  
  Serial.print("(0x80 & 0x81) == 0x80 ");
  Serial.println((0x80 & 0x81) == 0x80);
}

void loop() {
  // put your main code here, to run repeatedly:
}
