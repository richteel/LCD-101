#include <Arduino.h>



// LCD Parameters
#define LCD_RES_W 256
#define LCD_RES_H 128
#define CHAR_BITS_WIDE 8  
#define CHARS_PER_LINE  32//8 bit
#define TEXT_ROWS 16

// LCD Configuration
// P1
#define USE_EXTERNAL_CG_ROM  0 // M0 (bit) 0=Internal / 1=External
#define CG_RAM_AREA_64_CHAR  0 // M1 (bit) 0=32 character / 1=64 character
#define CG_CHARACTER_HEIGHT  0 // M2 (bit) 0=8-pixel / 1=16-pixel
#define DUAL_PANEL_CONFIGUR  0 // W/S (bit) 0=Single-panel / 1=Dual-panel
#define NO_TOP_LINE_OFFSET   1 // IV (bit) 0=Screen top-line correction / 1=No screen correction (No offset)
#define TV_MODE              0 // T/L (bit) 0=LCD mode / 1= TV Mode
#define ADD_SHIFT_CLK_CYCLE  0 // DR (bit) 0=Normal operation / 1=Additional shift-clock cycles (Required if dual-panel)
// P2
#define CHAR_WIDTH_PIXELS    7 // FX (0 to 7) Character width in pixels minus one
#define AC_FRAME_WF_PERIOD   1 // WF (bit) 0=16-line AC drive / 1=two-frame AC drive (normal)
//P3-P6
#define CHAR_HEIGHT_PIXELS   7 // FY (0 to 15) Character line height in pixels minus one
#define ADDRESSES_PER_LINE  31 // C/R (0 to 239) Memory addresses per line ([LCD Width]
#define LINE_LENGTH_W_BLANK 35 // TC/R (0 to 255) Length of line including horz blanking (Must be >= C/R + 4)
#define LCD_HEIGHT_PIXELS  127 // L/F Sets the height in lines (pixels), of a frame - 1
// P7 & P8
#define HORZ_ADDRESS_RANGE  32 // (2-bytes) Address range for one line of the virtual screen




void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println(F("Hello world"));
  delay(2000);// Give reader a chance to see the output.
  
  // Set pins for output
  setDataPinsForOutput();
  pinMode(res, OUTPUT);
  pinMode(rd, OUTPUT);
  pinMode(wr, OUTPUT);
  pinMode(cs, OUTPUT);
  pinMode(a0, OUTPUT);

  lcdReset();
  lcdInit();

  //testDataSheetSection9();
  test2(288);
  writeInternalCG();

  
}

void scroll(int hz, int vert) {
  
}

void loop() {
  // put your main code here, to run repeatedly:
}

/*** Functions ***/

void clearGraphicsLayer() {
  // Set Start at 03E8H
  lcdWriteCommand(CSRW);
  lcdWriteData(0x03);
  lcdWriteData(0xE8);

  // Write 00H (blank data) for 8000 bytes
  lcdWriteCommand(MWRITE);
  for(int i=0; i<8000; i++) {
    lcdWriteData(0x00);
  }
}

void clearTextLayer() {
  // Set Start at 0000H
  lcdWriteCommand(CSRW);
  lcdWriteData(0x00);
  lcdWriteData(0x00);

  // Write 20H (space character) for 1000 bytes
  lcdWriteCommand(MWRITE);
  for(int i=0; i<1000; i++) {
    lcdWriteData(0x20);
  }
}

void lcdInit0() {
  Serial.println(F("Step  3 - Initialize LCD Sequence"));
  //  3 Initialize LCD Sequence
  lcdWriteCommand(SYSTEM_SET);  // C
  lcdWriteData(0x30);  // P1 M0, M1, M2, W/S, IV, T/L, & DR
  lcdWriteData(0x87);  // P2 FX & WF
  lcdWriteData(0x07);  // P3 FY
  lcdWriteData(0x1F);  // P4 (C/R) Address range covered by one line
  lcdWriteData(0x23);  // P5 (TC/R) Length of one line
  lcdWriteData(0x7F);  // P6 (L/F) Frame height in lines
  lcdWriteData(0x20);  // P7 (APL)
  lcdWriteData(0x00);  // P8 (APH)
}

void lcdInit() {
  Serial.println(F("Step  3 - Initialize LCD Sequence"));
  lcdWriteCommand(SYSTEM_SET);  // C
  byte P1 = ADD_SHIFT_CLK_CYCLE << 7 | TV_MODE << 6 | NO_TOP_LINE_OFFSET << 5 | 1 << 4 | DUAL_PANEL_CONFIGUR << 3 | CG_CHARACTER_HEIGHT << 2 | CG_RAM_AREA_64_CHAR << 1 | USE_EXTERNAL_CG_ROM;
  byte P2 = AC_FRAME_WF_PERIOD << 7 | CHAR_WIDTH_PIXELS;
  byte P3 = CHAR_HEIGHT_PIXELS;
  byte P4 = ADDRESSES_PER_LINE;
  byte P5 = LINE_LENGTH_W_BLANK;
  byte P6 = LCD_HEIGHT_PIXELS;
  byte P7 = HORZ_ADDRESS_RANGE;
  byte P8 = HORZ_ADDRESS_RANGE >> 8;
  
  Serial.print(F("P1 = "));
  Serial.println(P1);
  Serial.print(F("P2 = "));
  Serial.println(P2);
  Serial.print(F("P3 = "));
  Serial.println(P3);
  Serial.print(F("P4 = "));
  Serial.println(P4);
  Serial.print(F("P5 = "));
  Serial.println(P5);
  Serial.print(F("P6 = "));
  Serial.println(P6);
  Serial.print(F("P7 = "));
  Serial.println(P7);
  Serial.print(F("P8 = "));
  Serial.println(P8);
  
  lcdWriteData(P1);  // P1 M0, M1, M2, W/S, IV, T/L, & DR
  lcdWriteData(P2);  // P2 FX & WF
  lcdWriteData(P3);  // P3 FY
  lcdWriteData(P4);  // P4 (C/R) Address range covered by one line
  lcdWriteData(P5);  // P5 (TC/R) Length of one line
  lcdWriteData(P6);  // P6 (L/F) Frame height in lines
  lcdWriteData(P7);  // P7 (APL)
  lcdWriteData(P8);  // P8 (APH)
}

void lcdReset() {
  digitalWrite(res, LOW);
  // Set init state for wr & cs
  digitalWrite(wr, LOW);
  digitalWrite(cs, LOW);
  delay(50);
}

void lcdWriteCommand(byte command) {
  lcdWriteCtrl(0x05);
  lcdWriteJustData(command);
  digitalWrite(wr, HIGH); // Latch Data
  //delay(10);
}

void lcdWriteCtrl(byte ctrl) {
  digitalWrite(cs, LOW);
  digitalWrite(res, HIGH);
  
  digitalWrite(a0, ctrl & 0x04);
  digitalWrite(wr, ctrl & 0x02);
  digitalWrite(rd, ctrl & 0x01);
}

void lcdWriteData(byte data) {
  lcdWriteCtrl(0x01);
  lcdWriteJustData(data);
  digitalWrite(wr, HIGH); // Latch Data
  //delay(10);
}

void lcdWriteJustData(byte data) {
  digitalWrite(d7, (data & 0x80) == 0x80);
  digitalWrite(d6, (data & 0x40) == 0x40);
  digitalWrite(d5, (data & 0x20) == 0x20);
  digitalWrite(d4, (data & 0x10) == 0x10);
  digitalWrite(d3, (data & 0x08) == 0x08);
  digitalWrite(d2, (data & 0x04) == 0x04);
  digitalWrite(d1, (data & 0x02) == 0x02);
  digitalWrite(d0, (data & 0x01) == 0x01);
}

void setDataPinsForOutput() {
  pinMode(d0, OUTPUT);
  pinMode(d1, OUTPUT);
  pinMode(d2, OUTPUT);
  pinMode(d3, OUTPUT);
  pinMode(d4, OUTPUT);
  pinMode(d5, OUTPUT);
  pinMode(d6, OUTPUT);
  pinMode(d7, OUTPUT);
}

void testDataSheetSection9() {
  Serial.println(F("Running Test 2"));

  Serial.println(F("Step  4 - Set display start address and display regions"));
  lcdWriteCommand(SCROLL);
  lcdWriteData(0x00);     // P1 (SAD 1 L)
  lcdWriteData(0x00);     // P2 (SAD 1 H)
  lcdWriteData(0x80);     // P3 (SL 1)
  lcdWriteData(0x00);     // P4 (SAD 2 L)
  lcdWriteData(0x10);     // P5 (SAD 2 H)
  lcdWriteData(0x80);     // P6 (SL 2)
  lcdWriteData(0x00);     // P7 (SAD 3 L)
  lcdWriteData(0x04);     // P8 (SAD 3 H)
  //lcdWriteData(0x00);     // P9 (SAD 4 L)
  //lcdWriteData(0x30);     // P10 (SAD 4 H)
  
  Serial.println(F("Step  5 - Set Horizontal Scroll position"));
  lcdWriteCommand(HDOT_SCR);
  lcdWriteData(0x00);
  
  Serial.println(F("Step  6 - Set display overlay format"));
  lcdWriteCommand(OVLAY);
  lcdWriteData(0x01);
  
  Serial.println(F("Step  7 - Set display off"));
  lcdWriteCommand(DISP_OFF);
  lcdWriteData(0x56);

  Serial.println(F("Step  8 - Clear data in first layer with 20H (space character)"));
  clearTextLayer();

  Serial.println(F("Step  9 - Clear data in second layer with 00H (blank data)"));
  clearGraphicsLayer();

  Serial.println(F("Step 10 - Set cursor address"));
  lcdWriteCommand(CSRW);
  lcdWriteData(0x00);
  lcdWriteData(0x00);

  Serial.println(F("Step 11 - Set Cursor type"));
  // 11 
  lcdWriteCommand(CSRFORM);
  lcdWriteData(0x04);
  lcdWriteData(0x86); 
  
  Serial.println(F("Step 12 - Set display on"));
  lcdWriteCommand(DISP_ON);

  Serial.println(F("Step 13 - Set Cursor direction - Right"));
  lcdWriteCommand(CSRDIR_R);

  Serial.println(F("Step 14 - Write characters (EPSON)"));
  lcdWriteCommand(MWRITE);
  lcdWriteData(0x20);
  lcdWriteData(0x45);
  lcdWriteData(0x50);
  lcdWriteData(0x53);
  lcdWriteData(0x4F);
  lcdWriteData(0x4E);

  Serial.println(F("Step 15 - Set cursor address"));
  lcdWriteCommand(CSRW);
  lcdWriteData(0x00);
  lcdWriteData(0x10);

  Serial.println(F("Step 16 - Set Cursor direction - Down"));
  lcdWriteCommand(CSRDIR_D);
  
  Serial.println(F("Step 17 - Fill square"));
  lcdWriteCommand(MWRITE);
  for(int i0=0; i0<9; i0++) {
    lcdWriteData(0xFF);
  }

  Serial.println(F("Step 18 - Set cursor address"));
  lcdWriteCommand(CSRW);
  lcdWriteData(0x01);
  lcdWriteData(0x10);

  Serial.println(F("Step 19 - Fill square"));
  // 19 Fill square
  lcdWriteCommand(MWRITE);
  for(int i0=0; i0<9; i0++) {
    lcdWriteData(0xFF);
  }

  Serial.println(F("Step 20 - Set cursor address"));
  // 20 Set cursor address
  lcdWriteCommand(CSRW);
  lcdWriteData(0x02);
  lcdWriteData(0x10);

  Serial.println(F("Step 21 - Fill square"));
  // 21 Fill square
  lcdWriteCommand(MWRITE);
  for(int i0=0; i0<9; i0++) {
    lcdWriteData(0xFF);
  }

  Serial.println(F("Step 22 - Set cursor address"));
  // 22 Set cursor address
  lcdWriteCommand(CSRW);
  lcdWriteData(0x03);
  lcdWriteData(0x10);

  Serial.println(F("Step 23 - Fill square"));
  // 23 Fill square
  lcdWriteCommand(MWRITE);
  for(int i0=0; i0<9; i0++) {
    lcdWriteData(0xFF);
  }

  Serial.println(F("Step 24 - Set cursor address"));
  // 24 Set cursor address
  lcdWriteCommand(CSRW);
  lcdWriteData(0x04);
  lcdWriteData(0x10);

  Serial.println(F("Step 25 - Fill square"));
  // 25 Fill square
  lcdWriteCommand(MWRITE);
  for(int i0=0; i0<9; i0++) {
    lcdWriteData(0xFF);
  }

  Serial.println(F("Step 26 - Set cursor address"));
  // 26 Set cursor address
  lcdWriteCommand(CSRW);
  lcdWriteData(0x05);
  lcdWriteData(0x10);

  Serial.println(F("Step 27 - Fill square"));
  // 27 Fill square
  lcdWriteCommand(MWRITE);
  for(int i0=0; i0<9; i0++) {
    lcdWriteData(0xFF);
  }

  Serial.println(F("Step 28 - Set cursor address"));
  // 28 Set cursor address
  lcdWriteCommand(CSRW);
  lcdWriteData(0x06);
  lcdWriteData(0x10);

  Serial.println(F("Step 29 - Fill square"));
  // 29 Fill square
  lcdWriteCommand(MWRITE);
  for(int i0=0; i0<9; i0++) {
    lcdWriteData(0xFF);
  }

  Serial.println(F("Step 30 - Set cursor address"));
  // 30 Set cursor address
  lcdWriteCommand(CSRW);
  lcdWriteData(0x40);
  lcdWriteData(0x00);

  Serial.println(F("Step 31 - Set Cursor direction - Right"));
  lcdWriteCommand(CSRDIR_R);

  Serial.println(F("Step 32 - Write more text (Dot matrix LCD)"));
  lcdWriteCommand(MWRITE);
  lcdWriteData(0x44);
  lcdWriteData(0x6F);
  lcdWriteData(0x74);
  lcdWriteData(0x20);
  lcdWriteData(0x4D);
  lcdWriteData(0x61);
  lcdWriteData(0x74);
  lcdWriteData(0x72);
  lcdWriteData(0x69);
  lcdWriteData(0x78);
  lcdWriteData(0x20);
  lcdWriteData(0x4C);
  lcdWriteData(0x43);
  lcdWriteData(0x44);  
  
  
  Serial.println(F("Done with Datasheet Section 9 Sample"));
}

void test2(int testNum) {
  
  Serial.println(F("Running Test 2"));

  Serial.println(F("Step  4 - Set display start address and display regions"));
  lcdWriteCommand(SCROLL);
  lcdWriteData(0x00);     // P1 (SAD 1 L)
  lcdWriteData(0x00);     // P2 (SAD 1 H)
  lcdWriteData(0x80);     // P3 (SL 1)
  lcdWriteData(0x00);     // P4 (SAD 2 L)
  lcdWriteData(0x10);     // P5 (SAD 2 H)
  lcdWriteData(0x80);     // P6 (SL 2)
  lcdWriteData(0x00);     // P7 (SAD 3 L)
  lcdWriteData(0x04);     // P8 (SAD 3 H)
  
  Serial.println(F("Step  5 - Set Horizontal Scroll position"));
  lcdWriteCommand(HDOT_SCR);
  lcdWriteData(0x00);
  
  Serial.println(F("Step  6 - Set display overlay format"));
  lcdWriteCommand(OVLAY);
  lcdWriteData(0x01);
  
  Serial.println(F("Step  7 - Set display off"));
  lcdWriteCommand(DISP_OFF);
  lcdWriteData(0x56);

  Serial.println(F("Step  8 - Clear data in first layer with 20H (space character)"));
  clearTextLayer();

  Serial.println(F("Step  9 - Clear data in second layer with 00H (blank data)"));
  clearGraphicsLayer();

  Serial.println(F("Step 10 - Set cursor address"));
  lcdWriteCommand(CSRW);
  lcdWriteData(0x00);
  lcdWriteData(0x00);

  Serial.println(F("Step 11 - Set Cursor type"));
  lcdWriteCommand(CSRFORM);
  lcdWriteData(0x04);
  lcdWriteData(0x86); 
  
  Serial.println(F("Step 12 - Set display on"));
  lcdWriteCommand(DISP_ON);
  //lcdWriteData(0x16);

  Serial.println(F("Step 13 - Set Cursor direction - Right"));
  lcdWriteCommand(CSRDIR_R);

  Serial.println(F("Step 14 - Write numbers"));
  writeNumbers(testNum);

  
  Serial.println(F("Done with Test 2"));
}


void writeNumbers(int numQty) {
  byte numZero = 0x30;
  int idx = 0;
  byte data = 0x00;
  byte offset = 1;
  
  lcdWriteCommand(MWRITE);
  while(idx < numQty) {
    if(offset > 9)
      offset = 0;

    data = numZero + offset;
    lcdWriteData(data);
    
    offset++;
    idx++;

    //if(idx % 16 == 0)
    //  delay(100);
  }

  //lcdWriteCommand(CSRW);
  //lcdWriteData(0xE0);
  //lcdWriteData(0x01);

  
  //lcdWriteCommand(DISP_ON);
  //lcdWriteData(0x5B);
  
}

void writeInternalCG() {
  lcdWriteCommand(MWRITE);

  for(int i=0; i < 96; i++) {
    lcdWriteData(32 + i);
  }
  
  for(int i=0; i < 64; i++) {
    lcdWriteData(160 + i);
  }
  
  for(int i=0; i < 16; i++) {
    lcdWriteData(16 + i);
  }
}

