/*!
   @file sed1330.h

   This is part of TeelSys SED1330F driver for the Arduino platform.  It is
   designed specifically to work with All Electronics LCD-101.

   TeeSys invests time and resources providing this open source code,
   please support TeeSys and open-source hardware by purchasing
   products from TeeSys!

   Written by Richard Teel and others for TeeSys LLC.

   BSD license, all text here must be included in any redistribution.

*/

#include <Arduino.h>

#ifndef SYSTEM_SET // LCD Comands
  #define SYSTEM_SET  0x40
  #define SLEEP_IN    0x53
  #define DISP_OFF    0x58
  #define DISP_ON     0x59
  #define SCROLL      0x44
  #define CSRFORM     0x5D
  #define CGRAM_ADR   0x5C
  #define CSRDIR_R    0x4C
  #define CSRDIR_L    0x4D
  #define CSRDIR_U    0x4E
  #define CSRDIR_D    0x4F
  #define HDOT_SCR    0x5A
  #define OVLAY       0x5B
  #define CSRW        0x46
  #define CSRR        0x47
  #define MWRITE      0x42
  #define MREAD       0x43
#endif


class TeelSys_sed1330 {
  public:
    // LCD Pins
    byte d0 = 14;
    byte d1 = 15;
    byte d2 = 2;
    byte d3 = 3;
    byte d4 = 4;
    byte d5 = 5;
    byte d6 = 6;
    byte d7 = 7;
    byte res = 8;
    byte rd = 9;
    byte wr = 10;
    byte cs = 11;
    byte a0 = 12;
}

