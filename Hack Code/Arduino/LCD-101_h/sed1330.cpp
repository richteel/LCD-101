/*!
   @file sed1330.cpp

   This is part of TeelSys SED1330F driver for the Arduino platform.  It is
   designed specifically to work with All Electronics LCD-101.

   TeeSys invests time and resources providing this open source code,
   please support TeeSys and open-source hardware by purchasing
   products from TeeSys!

   Written by Richard Teel and others for TeeSys LLC.

   BSD license, all text here must be included in any redistribution.

*/

#include "sed1330.h"

// All Electronics LCD-101
// HG25504 with SED1330F

// LCD Pins
byte d0 = 14;
byte d1 = 15;
byte d2 = 2;
byte d3 = 3;
byte d4 = 4;
byte d5 = 5;
byte d6 = 6;
byte d7 = 7;
byte res = 8;
byte rd = 9;
byte wr = 10;
byte cs = 11;
byte a0 = 12;


