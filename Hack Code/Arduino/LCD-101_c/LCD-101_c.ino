#include <Arduino.h>

#define wr 10

void setup() {
  // put your setup code here, to run once:
  // Set Ports D & B for output
  DDRD = 0xFF;  // Pins 0 to 7
  DDRD = 0x1F;  // Pins 8 to 12

  // Initialize LCD Sequence
  lcd_write(0x40, 0x12);  // Reset
  lcd_write(0x40, 0x13);  // C
  lcd_write(0xBA, 0x03);  // P1
  lcd_write(0X87, 0x03);  // P2
  lcd_write(0x0F, 0x03);  // P3
  lcd_write(0x20, 0x03);  // P4
  lcd_write(0x22, 0x03);  // P5
  lcd_write(0x7F, 0x03);  // P6
  lcd_write(0x00, 0x03);  // P7
  lcd_write(0x01, 0x03);  // P8
  
  //lcd_write(0xAB, 0x13);
}

void loop() {
  // put your main code here, to run repeatedly:
  lcd_write(0x58, 0x13);
  delay(1000);
  lcd_write(0x59, 0x13);
  delay(1000);
}

void lcd_write(byte data, byte instruction) {
  PORTD = data;
  PORTB = instruction;
  //delay(500);
  digitalWrite(wr, HIGH); // Latch Data  
}

