/*!
   @file TeelSys_sed1330.h

   This is part of TeelSys SED1330F driver for the Arduino platform.  It is
   designed specifically to work with All Electronics LCD-101.

   TeeSys invests time and resources providing this open source code,
   please support TeeSys and open-source hardware by purchasing
   products from TeeSys!

   Written by Richard Teel and others for TeeSys LLC.

   BSD license, all text here must be included in any redistribution.

*/

#if defined(ARDUINO) && ARDUINO >= 100
  #include "Arduino.h"
#else
  #include "WProgram.h"
#endif

//#include <Stream.h>
//#include <avr/pgmspace.h>

#ifndef TeelSys_sed1330_h
  #define TeelSys_sed1330_h
#endif

#ifndef NON_INVERTED
  // useful user constants
  #define NON_INVERTED false
  #define INVERTED     true
  
  // Colors
  #define BLACK       0xFF
  #define WHITE       0x00
#endif

#ifndef SYSTEM_SET // LCD Comands
  #define SYSTEM_SET  0x40
  #define SLEEP_IN    0x53
  #define DISP_OFF    0x58
  #define DISP_ON     0x59
  #define SCROLL      0x44
  #define CSRFORM     0x5D
  #define CGRAM_ADR   0x5C
  #define CSRDIR_R    0x4C
  #define CSRDIR_L    0x4D
  #define CSRDIR_U    0x4E
  #define CSRDIR_D    0x4F
  #define HDOT_SCR    0x5A
  #define OVLAY       0x5B
  #define CSRW        0x46
  #define CSRR        0x47
  #define MWRITE      0x42
  #define MREAD       0x43
#endif

class TeelSys_sed1330   
{
  public:
    // Constructors
    TeelSys_sed1330();
    TeelSys_sed1330(byte i2cAddress);
    TeelSys_sed1330(byte res, byte rd, byte wr, byte cs, byte a0, byte d0, byte d1, byte d2, byte d3, byte d4, byte d5, byte d6, byte d7);

    // Basic LCD Functions
    void WriteData(byte data);
    void WriteCommand(byte command);

    // Other Public Functions
    void begin();
    void Reset();

  private:
    // LCD Pins (Parallel)
    byte _d0 = 14;
    byte _d1 = 15;
    byte _d2 = 2;
    byte _d3 = 3;
    byte _d4 = 4;
    byte _d5 = 5;
    byte _d6 = 6;
    byte _d7 = 7;
    byte _res = 8;
    byte _rd = 9;
    byte _wr = 10;
    byte _cs = 11;
    byte _a0 = 12;

    // LCD I2C Address
    byte _i2cAddress = 0;
    bool useI2C = false;
    
    // Private Functions
    void lcdWriteCtrl(byte ctrl);
    void lcdWriteJustData(byte data);
    void setupLcdPins4Read();
    void setupLcdPins4Write();
};
