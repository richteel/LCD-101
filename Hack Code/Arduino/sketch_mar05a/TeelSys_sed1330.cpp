/*!
   @file TeelSys_sed1330.cpp

   This is part of TeelSys SED1330F driver for the Arduino platform.  It is
   designed specifically to work with All Electronics LCD-101.

   TeeSys invests time and resources providing this open source code,
   please support TeeSys and open-source hardware by purchasing
   products from TeeSys!

   Written by Richard Teel and others for TeeSys LLC.

   BSD license, all text here must be included in any redistribution.

*/

#include "TeelSys_sed1330.h"


// Public Methods
// Constructors
TeelSys_sed1330::TeelSys_sed1330()
{
  useI2C = false;
}

TeelSys_sed1330::TeelSys_sed1330(byte i2cAddress) {
  useI2C = true;
  _i2cAddress = i2cAddress;
}

TeelSys_sed1330::TeelSys_sed1330(byte res, byte rd, byte wr, byte cs, byte a0, byte d0, byte d1, byte d2, byte d3, byte d4, byte d5, byte d6, byte d7)
{
  useI2C = false;
  _res = res;
  _rd = rd;
  _wr = wr;
  _cs = cs;
  _a0 = a0;
  _d0 = d0;
  _d1 = d1;
  _d2 = d2;
  _d3 = d3;
  _d4 = d4;
  _d5 = d5;
  _d6 = d6;
  _d7 = d7;
}

// Write to the LCD
void TeelSys_sed1330::WriteData(byte data) {
  lcdWriteCtrl(0x01);
  lcdWriteJustData(data);
  digitalWrite(_wr, HIGH); // Latch Data
  //delay(10);
}

void TeelSys_sed1330::WriteCommand(byte command) {
  lcdWriteCtrl(0x05);
  lcdWriteJustData(command);
  digitalWrite(_wr, HIGH); // Latch Data
}

void TeelSys_sed1330::begin() {
  setupLcdPins4Write();
  Reset();
}

void TeelSys_sed1330::Reset() {
  digitalWrite(_res, LOW);
  // Set init state for wr & cs
  digitalWrite(_wr, LOW);
  digitalWrite(_cs, LOW);
  delay(500);
}

// Private Functions
void TeelSys_sed1330::lcdWriteCtrl(byte ctrl) {
  if (useI2C) {

  }
  else {
    digitalWrite(_cs, LOW);
    digitalWrite(_res, HIGH);

    digitalWrite(_a0, (ctrl & 0x04) == 0x04);
    digitalWrite(_wr, (ctrl & 0x02) == 0x02);
    digitalWrite(_rd, (ctrl & 0x01) == 0x01);
  }
}

void TeelSys_sed1330::lcdWriteJustData(byte data) {
  if (useI2C) {

  }
  else {
    digitalWrite(_d0, (data & 0x01) == 0x01);
    digitalWrite(_d1, (data & 0x02) == 0x02);
    digitalWrite(_d2, (data & 0x04) == 0x04);
    digitalWrite(_d3, (data & 0x08) == 0x08);
    digitalWrite(_d4, (data & 0x10) == 0x10);
    digitalWrite(_d5, (data & 0x20) == 0x20);
    digitalWrite(_d6, (data & 0x40) == 0x40);
    digitalWrite(_d7, (data & 0x80) == 0x80);
  }
}

void TeelSys_sed1330::setupLcdPins4Read() {
  pinMode(_res, OUTPUT);
  pinMode(_rd, OUTPUT);
  pinMode(_wr, OUTPUT);
  pinMode(_cs, OUTPUT);
  pinMode(_a0, OUTPUT);
  pinMode(_d0, INPUT);
  pinMode(_d1, INPUT);
  pinMode(_d2, INPUT);
  pinMode(_d3, INPUT);
  pinMode(_d4, INPUT);
  pinMode(_d5, INPUT);
  pinMode(_d6, INPUT);
  pinMode(_d7, INPUT);
}

void TeelSys_sed1330::setupLcdPins4Write() {
  pinMode(_res, OUTPUT);
  pinMode(_rd, OUTPUT);
  pinMode(_wr, OUTPUT);
  pinMode(_cs, OUTPUT);
  pinMode(_a0, OUTPUT);
  pinMode(_d0, OUTPUT);
  pinMode(_d1, OUTPUT);
  pinMode(_d2, OUTPUT);
  pinMode(_d3, OUTPUT);
  pinMode(_d4, OUTPUT);
  pinMode(_d5, OUTPUT);
  pinMode(_d6, OUTPUT);
  pinMode(_d7, OUTPUT);
}


