/*
   REF
    https://forum.arduino.cc/index.php?topic=145287.15
    https://forum.arduino.cc/index.php?topic=155961.0
    https://forum.arduino.cc/index.php?topic=503782.0
*/


#include "TeelSys_GLCD.h"

// Constructors
TeelSys_GLCD::TeelSys_GLCD() {
  _lcd = TeelSys_sed1330();
}

TeelSys_GLCD::TeelSys_GLCD(byte i2cAddress) {
  _lcd = TeelSys_sed1330(i2cAddress);
}

TeelSys_GLCD::TeelSys_GLCD(byte res, byte rd, byte wr, byte cs, byte a0, byte d0, byte d1, byte d2, byte d3, byte d4, byte d5, byte d6, byte d7) {
  _lcd = TeelSys_sed1330(res, rd, wr, cs, a0, d0, d1, d2, d3, d4, d5, d6, d7);
}

#if ARDUINO < 100
  size_t TeelSys_GLCD::write(uint8_t character) {
    /*Code to display letter when given the ASCII code for it*/
    _lcd.WriteCommand(MWRITE);
    _lcd.WriteData(character);
  }
#else
  //  Support for the print function
  size_t TeelSys_GLCD::write(uint8_t character) { /*blahblah is the name of your class*/
    /*Code to display letter when given the ASCII code for it*/
    _lcd.WriteCommand(MWRITE);
    _lcd.WriteData(character);
  }
#endif

// Public Functions
void TeelSys_GLCD::begin() {
  _lcd.begin();
}

void TeelSys_GLCD::ClearScreen() {
  ClearScreen(WHITE);
}

void TeelSys_GLCD::ClearScreen(byte color) {
  byte backcolor = color;

  if (_background > 0) {
    if (color == 0)
      backcolor = 0xFF;
    else
      backcolor = 0x00;
  }

  clearTextLayer(0x20);
  clearGraphicsLayer(backcolor);
}

void TeelSys_GLCD::CursorTo(int column, int row) {
  unsigned int charsPerRow = _lcdWidth / _charWidth;
  word posAddress = _displayPage1_Start + (row * charsPerRow) + column;

  _lcd.WriteCommand(CSRW);
  _lcd.WriteData(getWordLowByte(posAddress));
  _lcd.WriteData(getWordHighByte(posAddress));
}

void TeelSys_GLCD::CursorToXY(int x, int y) {
  unsigned int charsPerRow = _lcdWidth / _charWidth;
  word byteOffset = _displayPage2_Start + (y * charsPerRow) + x;

  _lcd.WriteCommand(CSRW);
  _lcd.WriteData(getWordLowByte(byteOffset));
  _lcd.WriteData(getWordHighByte(byteOffset));
}

void TeelSys_GLCD::DebugSetSerial(Stream *streamObject) {
  _serial = streamObject;
}

void TeelSys_GLCD::GotoXY(int x, int y) {

}

void TeelSys_GLCD::Init() {
  Init(0);
}

void TeelSys_GLCD::Init(byte background) {
  debugPrint(F("Staring LCD Initialization"));
  SetDisplayMode(background);

  setDefaultParameters();
  initialize();
  setupLcdMemory();
  debugPrint(F("Completed LCD Initialization"));
}

void TeelSys_GLCD::SetDisplayMode(byte background) {
  _background = background;

  if (_background != 0)
    _background = 0xFF;
}

// Private Functions
void TeelSys_GLCD::clearGraphicsLayer(byte clearColor) {
  //  7 Set display off
  _lcd.WriteCommand(DISP_OFF);
  _lcd.WriteData(0x56);

  // Set Start at 03E8H
  _lcd.WriteCommand(CSRW);
  _lcd.WriteData(getWordLowByte(_displayPage2_Start));
  _lcd.WriteData(getWordHighByte(_displayPage2_Start));

  unsigned int graphicsPageSize = (_displayPage2_End - _displayPage2_Start) + 1;

  // Write 00H (blank data) for 8000 bytes
  _lcd.WriteCommand(MWRITE);
  for (int i = 0; i < graphicsPageSize; i++) {
    _lcd.WriteData(clearColor);
  }

  // 10 Set cursor address
  _lcd.WriteCommand(CSRW);
  _lcd.WriteData(0x00);
  _lcd.WriteData(0x00);

  // 11 Set Cursor type
  _lcd.WriteCommand(CSRFORM);
  _lcd.WriteData(0x04);
  _lcd.WriteData(0x86);

  // 12 Set display on
  _lcd.WriteCommand(DISP_ON);

  // 13 Set Cursor direction - Right
  _lcd.WriteCommand(CSRDIR_R);
}

void TeelSys_GLCD::clearTextLayer(byte clearAsciiCode) {
  //  7 Set display off
  _lcd.WriteCommand(DISP_OFF);
  _lcd.WriteData(0x56);

  CursorTo(0, 0);

  unsigned int textPageSize = (_displayPage1_End - _displayPage1_Start) + 1;

  // Write 20H (space character) for 1000 bytes
  _lcd.WriteCommand(MWRITE);
  for (int i = 0; i < textPageSize; i++) {
    _lcd.WriteData(clearAsciiCode);
  }

  // 10 Set cursor address
  _lcd.WriteCommand(CSRW);
  _lcd.WriteData(0x00);
  _lcd.WriteData(0x00);

  // 11 Set Cursor type
  _lcd.WriteCommand(CSRFORM);
  _lcd.WriteData(0x04);
  _lcd.WriteData(0x86);

  // 12 Set display on
  _lcd.WriteCommand(DISP_ON);

  // 13 Set Cursor direction - Right
  _lcd.WriteCommand(CSRDIR_R);
}

void TeelSys_GLCD::debugPrint(const char *message) {
  if (!_serial)
    return;
    
   _serial->println(message);
}

void TeelSys_GLCD::debugPrint(const __FlashStringHelper *message) {
  if (!_serial)
    return;

   _serial->println(message);
}

void TeelSys_GLCD::debugPrint(const char *message, unsigned int parm) {
  if (!_serial)
    return;
    
   _serial->print(message);
   _serial->print(F(" = "));
   _serial->print(parm);
   _serial->print(F(" (0x"));
   _serial->print(parm, HEX);
   _serial->println(F(")"));
}

void TeelSys_GLCD::debugPrint(const __FlashStringHelper *message, unsigned int parm) {
  if (!_serial)
    return;

   _serial->print(message);
   _serial->print(F(" = "));
   _serial->print(parm);
   _serial->print(F(" (0x"));
   _serial->print(parm, HEX);
   _serial->println(F(")"));
}

unsigned int TeelSys_GLCD::divideAndRoundUp(unsigned int dividend, unsigned int divisor) {
  unsigned int quotient = dividend / divisor;
  unsigned int remainder = dividend % divisor;

  if (remainder > 0)
    return quotient + 1;
  else
    return quotient;
}

byte TeelSys_GLCD::getWordHighByte(word value) {
  return value / 256;
}

byte TeelSys_GLCD::getWordLowByte(word value) {
  return value % 256;
}

void TeelSys_GLCD::initialize() {
  bool iv = _background == 0;
  byte p1 = _lcdDualPanel << 7 | 0 << 6 | iv << 5 | 1 << 4 | _lcdDualPanel << 3 | (_charHeight > 8) << 2 | 1 << 1 | 0;
  byte p2 = 1 << 7 | (_charWidth - 1);
  byte p3 = _charHeight - 1;
  byte p4 = _lcdWidth / (divideAndRoundUp(_charWidth, 8) * 8) - 1;
  byte p5 = p4 + 4;
  byte p6 = _lcdHeight - 1;

  word horzAddressRange = _virtualWidth / ((divideAndRoundUp(_charWidth, 8)) * 8);
  byte p7 = horzAddressRange % 256;
  byte p8 = horzAddressRange / 256;

  _lcd.Reset();

  _lcd.WriteCommand(SYSTEM_SET);  // C
  _lcd.WriteData(p1);  // P1 M0, M1, M2, W/S, IV, T/L, & DR
  _lcd.WriteData(p2);  // P2 FX & WF
  _lcd.WriteData(p3);  // P3 FY
  _lcd.WriteData(p4);  // P4 (C/R) Address range covered by one line
  _lcd.WriteData(p5);  // P5 (TC/R) Length of one line
  _lcd.WriteData(p6);  // P6 (L/F) Frame height in lines
  _lcd.WriteData(p7);  // P7 (APL)
  _lcd.WriteData(p8);  // P8 (APH)

  debugPrint("");
  debugPrint(F("SYSTEM SET (Initialization)"));
  debugPrint(F("------------------------------"));
  debugPrint(F("\tP1"), p1);
  debugPrint(F("\tP2"), p2);
  debugPrint(F("\tP3"), p3);
  debugPrint(F("\tP4"), p4);
  debugPrint(F("\tP5"), p5);
  debugPrint(F("\tP6"), p6);
  debugPrint(F("\tP7"), p7);
  debugPrint(F("\tP8"), p8);
}

void TeelSys_GLCD::setDefaultParameters() {
  // Partition Memory
  word charPageScreenSize = _lcdWidth / ((divideAndRoundUp(_charWidth, 8)) * 8) * _lcdHeight / ((divideAndRoundUp(_charHeight, 8)) * 8);
  word graphicPageScreenSize = _virtualWidth / 8 * _virtualHeight;
  word virtualScreenSize = _virtualWidth / ((divideAndRoundUp(_charWidth, 8)) * 8) * _virtualHeight / ((divideAndRoundUp(_charHeight, 8)) * 8);

  _displayPage1_Start = 0;
  _displayPage1_End = (virtualScreenSize * 2) - 1;
  _characterPage1_Start = _displayPage1_Start;
  _characterPage1_End = charPageScreenSize - 1;
  _characterPage3_Start = charPageScreenSize;
  _characterPage3_End = (charPageScreenSize * 2) - 1;

  _displayPage2_Start = _displayPage1_End + 1;
  _displayPage2_End = (_displayPage2_Start + graphicPageScreenSize) - 1;


  debugPrint("");
  debugPrint(F("Partition Memory"));
  debugPrint(F("------------------------------"));
  
  // Check memory size
  if(_displayPage2_End > (_lcdRamKB * 1024) - 64) {
    debugPrint(F("ERROR: Memory is too small!!!"));
  }
  else {
    debugPrint(F("PASS: Memory size is fine"));
  }

  _cg_End = (_lcdRamKB * 1024) - 1;
  _cg_Start = _cg_End - 64;

  debugPrint("");
  debugPrint(F("charPageScreenSize"), charPageScreenSize);
  debugPrint(F("graphicPageScreenSize"), graphicPageScreenSize);
  debugPrint(F("virtualScreenSize"), virtualScreenSize);
  
  debugPrint(F("_displayPage1_Start"), _displayPage1_Start);
  debugPrint(F("_displayPage1_End"), _displayPage1_End);
  debugPrint(F("_characterPage1_Start"), _characterPage1_Start);
  debugPrint(F("_characterPage1_End"), _characterPage1_End);
  debugPrint(F("_characterPage3_Start"), _characterPage3_Start);
  debugPrint(F("_characterPage3_End"), _characterPage3_End);
  
  debugPrint(F("_displayPage2_Start"), _displayPage2_Start);
  debugPrint(F("_displayPage2_End"), _displayPage2_End);
  
  debugPrint(F("_cg_Start"), _cg_Start);
  debugPrint(F("_cg_End"), _cg_End);
}

void TeelSys_GLCD::setupLcdMemory() {
  debugPrint(F("Step 4 - Set display start address and display regions"));
  //  4 Set display start address and display regions
  _lcd.WriteCommand(SCROLL);
  _lcd.WriteData(getWordLowByte(_displayPage1_Start));  // P1 (SAD 1 L)
  _lcd.WriteData(getWordHighByte(_displayPage1_Start)); // P2 (SAD 1 H)

  _lcd.WriteData(_virtualHeight); // P3 (SL 1)

  _lcd.WriteData(getWordLowByte(_displayPage2_Start));  // P4 (SAD 2 L)
  _lcd.WriteData(getWordHighByte(_displayPage2_Start)); // P5 (SAD 2 H)

  _lcd.WriteData(_virtualHeight); // P6 (SL 2)

  _lcd.WriteData(getWordLowByte(_characterPage3_Start));  // P7 (SAD 3 L)
  _lcd.WriteData(getWordHighByte(_characterPage3_Start)); // P8 (SAD 3 H)

  debugPrint("\tP1 (SAD 1 L): ", getWordLowByte(_displayPage1_Start));
  debugPrint("\tP2 (SAD 1 H): ", getWordHighByte(_displayPage1_Start));
  debugPrint("\tP3 (SL 1): ", _virtualHeight);
  debugPrint("\tP4 (SAD 2 L): ", getWordLowByte(_displayPage2_Start));
  debugPrint("\tP5 (SAD 2 H): ", getWordHighByte(_displayPage2_Start));
  debugPrint("\tP6 (SL 2): ", _virtualHeight);
  debugPrint("\tP7 (SAD 3 L): ", getWordLowByte(_characterPage3_Start));
  debugPrint("\tP8 (SAD 3 H): ", getWordHighByte(_characterPage3_Start));

  //  5 Set Horizontal Scroll position
  _lcd.WriteCommand(HDOT_SCR);
  _lcd.WriteData(0x00);

  //  6 Set display overlay format
  _lcd.WriteCommand(OVLAY);
  _lcd.WriteData(0x01);
}


