
#include "TeelSys_GLCD.h"

TeelSys_GLCD glcd = TeelSys_GLCD();
// TeelSys_GLCD glcd = TeelSys_GLCD(8, 9, 10, 11, 12, 14, 15, 2, 3, 4, 5, 6, 7);

unsigned long previousMillis = 0;
const long interval = 1000;

int testInt = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println(F("Starting TeelSys_LCD101 Library Test"));
  delay(2000);// Give reader a chance to see the output.
  
  pinMode(13, OUTPUT); 

  glcd.begin();

  glcd.DebugSetSerial(&Serial);
  
  glcd.Init();
  glcd.ClearScreen();
  glcd.CursorTo(5, 5);
  //glcd.print(0x41);
  glcd.CursorTo(5, 6);
  glcd.print(F("Hello World"));
  Serial.println(F("Completed Setup of TeelSys_LCD101 Library Test"));
  glcd.CursorTo(0, 0);
}

void loop() {
  // put your main code here, to run repeatedly:
  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis >= interval) {
    previousMillis = currentMillis;
    digitalWrite(13, !digitalRead(13));

    glcd.print(testInt);
    testInt++;
    if(testInt > 9)
      testInt = 0;
  }
}
