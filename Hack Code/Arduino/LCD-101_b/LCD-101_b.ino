#include <Arduino.h>

/* Constructor */
// U8G2_UC1701_DOGS102_1_4W_SW_SPI u8g2(U8G2_R0, /* clock=*/ 13, /* data=*/ 11, /* cs=*/ 10, /* dc=*/ 9, /* reset=*/ 8);

#define rotation 0
#define d0 0
#define d1 1
#define d2 2
#define d3 3
#define d4 4
#define d5 5
#define d6 6
#define d7 7
#define reset 8   // /RES
#define rd 9      // /RD
#define enable 10 // /WR
#define cs 11     // /CS
#define dc 12     // A0

void setup() {
  // put your setup code here, to run once:
  pinMode(d0, OUTPUT);
  pinMode(d1, OUTPUT);
  pinMode(d2, OUTPUT);
  pinMode(d3, OUTPUT);
  pinMode(d4, OUTPUT);
  pinMode(d5, OUTPUT);
  pinMode(d6, OUTPUT);
  pinMode(d7, OUTPUT);
  pinMode(reset, OUTPUT);
  pinMode(rd, OUTPUT);
  pinMode(enable, OUTPUT);
  pinMode(cs, OUTPUT);
  pinMode(dc, OUTPUT);

  digitalWrite(d0, LOW);
  digitalWrite(d1, LOW);
  digitalWrite(d2, LOW);
  digitalWrite(d3, LOW);
  digitalWrite(d4, LOW);
  digitalWrite(d5, LOW);
  digitalWrite(d6, LOW);
  digitalWrite(d7, LOW);
  digitalWrite(reset, HIGH);
  digitalWrite(rd, HIGH);
  digitalWrite(enable, HIGH);
  digitalWrite(cs, HIGH);
  digitalWrite(dc, LOW);
}

void loop() {
  // put your main code here, to run repeatedly:

}
