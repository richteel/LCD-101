#include <Arduino.h>

// All Electronics LCD-101
// HG25504 with SED1330F

// LCD Pins
#define d0 14
#define d1 15
#define d2 2
#define d3 3
#define d4 4
#define d5 5
#define d6 6
#define d7 7
#define res 8
#define rd 9
#define wr 10
#define cs 11
#define a0 12

// LCD Comands
#define SYSTEM_SET  0x40
#define SLEEP_IN    0x53
#define DISP_OFF    0x58
#define DISP_ON     0x59
#define SCROLL      0x44
#define CSRFORM     0x5D
#define CGRAM_ADR   0x5C
#define CSRDIR_R    0x4C
#define CSRDIR_L    0x4D
#define CSRDIR_U    0x4E
#define CSRDIR_D    0x4F
#define HDOT_SCR    0x5A
#define OVLAY       0x5B
#define CSRW        0x46
#define CSRR        0x47
#define MWRITE      0x42
#define MREAD       0x43

// LCD Parameters
#define LCD_RES_W 256
#define LCD_RES_H 128
#define CHAR_BITS_WIDE 8  
#define CHARS_PER_LINE  32//8 bit
#define TEXT_ROWS 16

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println("Hello world");
  delay(2000);// Give reader a chance to see the output.
  
  // Set pins for output
  setDataPinsForOutput();
  pinMode(res, OUTPUT);
  pinMode(rd, OUTPUT);
  pinMode(wr, OUTPUT);
  pinMode(cs, OUTPUT);
  pinMode(a0, OUTPUT);

  lcdReset();
  lcdInit();

  //testDataSheetSection9();
  test2(256);
  writeInternalCG();
}

void loop() {
  // put your main code here, to run repeatedly:
}

/*** Functions ***/

void clearGraphicsLayer() {
  // Set Start at 03E8H
  lcdWriteCommand(CSRW);
  lcdWriteData(0x03);
  lcdWriteData(0xE8);

  // Write 00H (blank data) for 8000 bytes
  lcdWriteCommand(MWRITE);
  for(int i=0; i<8000; i++) {
    lcdWriteData(0x00);
  }
}

void clearTextLayer() {
  // Set Start at 0000H
  lcdWriteCommand(CSRW);
  lcdWriteData(0x00);
  lcdWriteData(0x00);

  // Write 20H (space character) for 1000 bytes
  lcdWriteCommand(MWRITE);
  for(int i=0; i<1000; i++) {
    lcdWriteData(0x20);
  }
}

void lcdInit() {
  Serial.println("Step  3");
  //  3 Initialize LCD Sequence
  lcdWriteCommand(SYSTEM_SET);  // C
  lcdWriteData(0x32);  // P1 M0, M1, M2, W/S, IV, T/L, & DR
  lcdWriteData(0x87);  // P2 FX & WF
  lcdWriteData(0x07);  // P3 FY
  lcdWriteData(0x1F);  // P4 (C/R) Address range covered by one line
  lcdWriteData(0x23);  // P5 (TC/R) Length of one line
  lcdWriteData(0x7F);  // P6 (L/F) Frame height in lines
  lcdWriteData(0x20);  // P7 (APL)
  lcdWriteData(0x00);  // P8 (APH)
}

void lcdReset() {
  digitalWrite(res, LOW);
  // Set init state for wr & cs
  digitalWrite(wr, LOW);
  digitalWrite(cs, LOW);
  delay(50);
}

void lcdWriteCommand(byte command) {
  lcdWriteCtrl(0x05);
  lcdWriteJustData(command);
  digitalWrite(wr, HIGH); // Latch Data
  //delay(10);
}

void lcdWriteCtrl(byte ctrl) {
  digitalWrite(cs, LOW);
  digitalWrite(res, HIGH);
  
  digitalWrite(a0, ctrl & 0x04);
  digitalWrite(wr, ctrl & 0x02);
  digitalWrite(rd, ctrl & 0x01);
}

void lcdWriteData(byte data) {
  lcdWriteCtrl(0x01);
  lcdWriteJustData(data);
  digitalWrite(wr, HIGH); // Latch Data
  //delay(10);
}

void lcdWriteJustData(byte data) {
  digitalWrite(d7, (data & 0x80) == 0x80);
  digitalWrite(d6, (data & 0x40) == 0x40);
  digitalWrite(d5, (data & 0x20) == 0x20);
  digitalWrite(d4, (data & 0x10) == 0x10);
  digitalWrite(d3, (data & 0x08) == 0x08);
  digitalWrite(d2, (data & 0x04) == 0x04);
  digitalWrite(d1, (data & 0x02) == 0x02);
  digitalWrite(d0, (data & 0x01) == 0x01);
}

void setDataPinsForOutput() {
  pinMode(d0, OUTPUT);
  pinMode(d1, OUTPUT);
  pinMode(d2, OUTPUT);
  pinMode(d3, OUTPUT);
  pinMode(d4, OUTPUT);
  pinMode(d5, OUTPUT);
  pinMode(d6, OUTPUT);
  pinMode(d7, OUTPUT);
}

void testDataSheetSection9() {
  Serial.println("Running Test 2");

  Serial.println("Step  4");
  //  4 Set display start address and display regions
  lcdWriteCommand(SCROLL);
  lcdWriteData(0x00);     // P1 (SAD 1 L)
  lcdWriteData(0x00);     // P2 (SAD 1 H)
  lcdWriteData(0x80);     // P3 (SL 1)
  lcdWriteData(0x00);     // P4 (SAD 2 L)
  lcdWriteData(0x10);     // P5 (SAD 2 H)
  lcdWriteData(0x80);     // P6 (SL 2)
  lcdWriteData(0x00);     // P7 (SAD 3 L)
  lcdWriteData(0x04);     // P8 (SAD 3 H)
  //lcdWriteData(0x00);     // P9 (SAD 4 L)
  //lcdWriteData(0x30);     // P10 (SAD 4 H)
  
  Serial.println("Step  5");
  //  5 Set Horizontal Scroll position
  lcdWriteCommand(HDOT_SCR);
  lcdWriteData(0x00);
  
  Serial.println("Step  6");
  //  6 Set display overlay format
  lcdWriteCommand(OVLAY);
  lcdWriteData(0x01);
  
  Serial.println("Step  7");
  //  7 Set display off
  lcdWriteCommand(DISP_OFF);
  lcdWriteData(0x56);

  Serial.println("Step  8");
  //  8 Clear data in first layer with 20H (space character)
  clearTextLayer();

  Serial.println("Step  9");
  //  9 Clear data in second layer with 00H (blank data)
  clearGraphicsLayer();

  Serial.println("Step 10");
  // 10 Set cursor address
  lcdWriteCommand(CSRW);
  lcdWriteData(0x00);
  lcdWriteData(0x00);

  Serial.println("Step 11");
  // 11 Set Cursor type
  lcdWriteCommand(CSRFORM);
  lcdWriteData(0x04);
  lcdWriteData(0x86); 
  
  Serial.println("Step 12");
  // 12 Set display on
  lcdWriteCommand(DISP_ON);

  Serial.println("Step 13");
  // 13 Set Cursor direction - Right
  lcdWriteCommand(CSRDIR_R);

  Serial.println("Step 14");
  // 14 Write characters
  lcdWriteCommand(MWRITE);
  lcdWriteData(0x20);
  lcdWriteData(0x45);
  lcdWriteData(0x50);
  lcdWriteData(0x53);
  lcdWriteData(0x4F);
  lcdWriteData(0x4E);

  Serial.println("Step 15");
  // 15 Set cursor address
  lcdWriteCommand(CSRW);
  lcdWriteData(0x00);
  lcdWriteData(0x10);

  Serial.println("Step 16");
  // 16 Set Cursor direction - Down
  lcdWriteCommand(CSRDIR_D);
  
  Serial.println("Step 17");
  // 17 Fill square
  lcdWriteCommand(MWRITE);
  for(int i0=0; i0<9; i0++) {
    lcdWriteData(0xFF);
  }

  Serial.println("Step 18");
  // 18 Set cursor address
  lcdWriteCommand(CSRW);
  lcdWriteData(0x01);
  lcdWriteData(0x10);

  Serial.println("Step 19");
  // 19 Fill square
  lcdWriteCommand(MWRITE);
  for(int i0=0; i0<9; i0++) {
    lcdWriteData(0xFF);
  }

  Serial.println("Step 20");
  // 20 Set cursor address
  lcdWriteCommand(CSRW);
  lcdWriteData(0x02);
  lcdWriteData(0x10);

  Serial.println("Step 21");
  // 21 Fill square
  lcdWriteCommand(MWRITE);
  for(int i0=0; i0<9; i0++) {
    lcdWriteData(0xFF);
  }

  Serial.println("Step 22");
  // 22 Set cursor address
  lcdWriteCommand(CSRW);
  lcdWriteData(0x03);
  lcdWriteData(0x10);

  Serial.println("Step 23");
  // 23 Fill square
  lcdWriteCommand(MWRITE);
  for(int i0=0; i0<9; i0++) {
    lcdWriteData(0xFF);
  }

  Serial.println("Step 24");
  // 24 Set cursor address
  lcdWriteCommand(CSRW);
  lcdWriteData(0x04);
  lcdWriteData(0x10);

  Serial.println("Step 25");
  // 25 Fill square
  lcdWriteCommand(MWRITE);
  for(int i0=0; i0<9; i0++) {
    lcdWriteData(0xFF);
  }

  Serial.println("Step 26");
  // 26 Set cursor address
  lcdWriteCommand(CSRW);
  lcdWriteData(0x05);
  lcdWriteData(0x10);

  Serial.println("Step 27");
  // 27 Fill square
  lcdWriteCommand(MWRITE);
  for(int i0=0; i0<9; i0++) {
    lcdWriteData(0xFF);
  }

  Serial.println("Step 28");
  // 28 Set cursor address
  lcdWriteCommand(CSRW);
  lcdWriteData(0x06);
  lcdWriteData(0x10);

  Serial.println("Step 29");
  // 29 Fill square
  lcdWriteCommand(MWRITE);
  for(int i0=0; i0<9; i0++) {
    lcdWriteData(0xFF);
  }

  Serial.println("Step 30");
  // 30 Set cursor address
  lcdWriteCommand(CSRW);
  lcdWriteData(0x40);
  lcdWriteData(0x00);

  Serial.println("Step 31");
  // 31 Set Cursor direction - Right
  lcdWriteCommand(CSRDIR_R);

  Serial.println("Step 32");
  // 32 Write more text
  lcdWriteCommand(MWRITE);
  lcdWriteData(0x44);
  lcdWriteData(0x6F);
  lcdWriteData(0x74);
  lcdWriteData(0x20);
  lcdWriteData(0x4D);
  lcdWriteData(0x61);
  lcdWriteData(0x74);
  lcdWriteData(0x72);
  lcdWriteData(0x69);
  lcdWriteData(0x78);
  lcdWriteData(0x20);
  lcdWriteData(0x4C);
  lcdWriteData(0x43);
  lcdWriteData(0x44);  
  
  
  Serial.println("Done with Datasheet Section 9 Sample");
}

void test2(int testNum) {
  
  Serial.println("Running Test 2");

  Serial.println("Step  4");
  //  4 Set display start address and display regions
  lcdWriteCommand(SCROLL);
  lcdWriteData(0x00);     // P1 (SAD 1 L)
  lcdWriteData(0x00);     // P2 (SAD 1 H)
  lcdWriteData(0x80);     // P3 (SL 1)
  lcdWriteData(0x00);     // P4 (SAD 2 L)
  lcdWriteData(0x10);     // P5 (SAD 2 H)
  lcdWriteData(0x80);     // P6 (SL 2)
  lcdWriteData(0x00);     // P7 (SAD 3 L)
  lcdWriteData(0x04);     // P8 (SAD 3 H)
  
  Serial.println("Step  5");
  //  5 Set Horizontal Scroll position
  lcdWriteCommand(HDOT_SCR);
  lcdWriteData(0x00);
  
  Serial.println("Step  6");
  //  6 Set display overlay format
  lcdWriteCommand(OVLAY);
  lcdWriteData(0x01);
  
  Serial.println("Step  7");
  //  7 Set display off
  lcdWriteCommand(DISP_OFF);
  lcdWriteData(0x56);

  Serial.println("Step  8");
  //  8 Clear data in first layer with 20H (space character)
  clearTextLayer();

  Serial.println("Step  9");
  //  9 Clear data in second layer with 00H (blank data)
  clearGraphicsLayer();

  Serial.println("Step 10");
  // 10 Set cursor address
  lcdWriteCommand(CSRW);
  lcdWriteData(0x00);
  lcdWriteData(0x00);

  Serial.println("Step 11");
  // 11 Set Cursor type
  lcdWriteCommand(CSRFORM);
  lcdWriteData(0x04);
  lcdWriteData(0x86); 
  
  Serial.println("Step 12");
  // 12 Set display on
  lcdWriteCommand(DISP_ON);
  //lcdWriteData(0x16);

  Serial.println("Step 13");
  // 13 Set Cursor direction - Right
  lcdWriteCommand(CSRDIR_R);

  Serial.println("Step 14");
  // 14 Write characters
  writeNumbers(testNum);

  
  Serial.println("Done with Test 2");
}


void writeNumbers(int numQty) {
  byte numZero = 0x30;
  int idx = 0;
  byte data = 0x00;
  byte offset = 1;
  
  lcdWriteCommand(MWRITE);
  while(idx < numQty) {
    if(offset > 9)
      offset = 0;

    data = numZero + offset;
    lcdWriteData(data);
    
    offset++;
    idx++;

    //if(idx % 16 == 0)
    //  delay(100);
  }

  //lcdWriteCommand(CSRW);
  //lcdWriteData(0xE0);
  //lcdWriteData(0x01);

  
  //lcdWriteCommand(DISP_ON);
  //lcdWriteData(0x5B);
  
}

void writeInternalCG() {
  lcdWriteCommand(MWRITE);

  for(int i=0; i < 96; i++) {
    lcdWriteData(32 + i);
  }
  
  for(int i=0; i < 64; i++) {
    lcdWriteData(160 + i);
  }
  
  for(int i=0; i < 16; i++) {
    lcdWriteData(16 + i);
  }
}

