# Using All Electronics LCD-101 with Arduino and Raspberry Pi #

![All Electronics LCD-101 LCD Display](./Documentation/images/15746.Jpg)

## All Electronics LCD-101 Item Description ##
Product Page: [All Electronics LCD-101](https://www.allelectronics.com/item/lcd-101/256-x-128-lcd-panel/1.html)

Hyundai # HG25504. 5.8" x 4.58" graphic display module with SED1330F onboard controller. Viewing area is 5" x 2.75". 5 Vdc logic. 18 Vdc LCD voltage. Includes hook-up diagram.

## Introduction ##

 
## Display Pinouts ##

![LCD-101 showing connectors and pin numbers](./Documentation/images/LCD-101.png)

### J1 – Unknown ###
|Pin No.|Symbol|Level|Function|
|:---------:|:---------:|:------------:|:---------------------------------|
|1|Vdd(Vcc)|+5V|Power supply voltage for logic and LCD|
|2|Vss(GND)|0V|Ground|
|3|Vo|-|Operating Voltage for LCD (variable)||
|4|LP|&nbsp;|LP Latch pulse. Pulses once per line and supplies the Y-driver (rows) shift clock.|
|5|WF|&nbsp;|Frame signal AC Drive. Depending on how you set this up in the control software, toggles once per frame to control the polarity of the AC drive.|
|6|&nbsp;|&nbsp;|No connection (open pin)|
|7|&nbsp;|&nbsp;|No connection (open pin)|
|8|&nbsp;|&nbsp;|Y Goes high for the duration of the last line of each frame|
|9|XSCL|&nbsp;|XSCL X shift clock - its falling edge latches XD[0:3] into the X-driver chips.|
|10|&nbsp;|&nbsp;|No connection (open pin)|
|11|XD0|&nbsp;|X-driver data to X-driver chips (column drive outputs). Since the decay time of the LCD pixels is so slow, do not even consider trying to use these pins to do some kind of light-pen feature. I would surmise that these pins were brought out to allow card-edge testing to verify that the LCD control pins were properly soldered into the board instead of doing some kind of visual test that the LCD is being properly scanned.|
|12|XD1|&nbsp;|&nbsp;|
|13|XD2|&nbsp;|&nbsp;|

J1 Information from an anonymous comment on the All Electronics product page for the LCD-101.


### J2 – LCD Control and Data Pins ###

|Pin No.|Symbol|Level|Function|
|:---------:|:---------:|:------------:|:---------------------------------|
|1|FG|0V|Frame Ground|
|2|Vss(GND)|0V|Ground|
|3|Vdd(Vcc)|+5V|Power supply voltage for logic and LCD|
|4|Vo|-|Operating Voltage for LCD (variable)|
|5|/RES|H/L|Reset signal|
|6|/RD|H/L|Read signal|
|7|/WR|H/L|Write signal|
|8|/CS|H/L|Chip select signal|
|9|A0|H/L|Data type select signal|
|10|DB0|H/L|Display data bit 0|
|11|DB1|H/L|Display data bit 1|
|12|DB2|H/L|Display data bit 2|
|13|DB3|H/L|Display data bit 3|
|14|DB4|H/L|Display data bit 4|
|15|DB5|H/L|Display data bit 5|
|16|DB6|H/L|Display data bit 6|
|17|DB7|H/L|Display data bit 7|


## Components ##

![Image showing placement of components of LCD-101](./Documentation/images/LCD_Components2.png)

|Component|Quantity|Description|
|:---------:|:---------:|:---------------------------------|
|SED1330F|1|LCD Controller|
|HY6264A|1|High speed, low power, 8,192 words by 8-bit cmos static ram|
|HD66204FC|4|DOT MATRIX LIQUID CRYSTAL GRAPHIC DISPLAY COLUMN DRIVER WITH 80-CHANNEL OUPUTS|
|HD66205FC|2|DOT MATRIX LIQUID CRYSTAL GRAPHIC DISPLAY COMMMON DRIVER WITH 80-CHANNEL OUPUTS|
|#1|2|4.7uF 25V Filter Capacitors|
|#2|4|1K ohm Resistors|
|#3|1|4.7K ohm Resistor|
|#4|1|KA324A Operational Amplifier Quad High Gain|
|#5|1|shunt (0 ohm resistor)|
|#6|1|4.00MHz Crystal Oscillator|


![Application Example from Hitachi HD66205 datasheet](./Documentation/images/ApplicationExample.png)
From the application note in the Hitachi datasheet for HD66205, we can see that the Hyundai LCD follows the design closely. The values for R1 and R2 are 1K and 4.7K ohms. From these values, we may use the calculation in the application note to verify the LCD duty cycle of 1/128.

R1/(4 x R1 + R2) = 1000/(4 x 1000 + 4700) = 1000/8700 = 10/87 = 0.1149
This is not close to the duty cycle of 1/128 = 0.0078 printed on the HG25504NG-01 datasheet. Not sure what is going on here but it does not match the application note.

The application note is useful for understanding how the LCD is being driven and a bit more about the operation of the SED1330F.

## Hack Programs ##

- LCD-101_a:
- LCD-101_b:
- LCD-101_c:
- LCD-101_d:
- LCD-101_e:
- LCD-101_f: Test code for displaying sample from datasheet and another to display numbers to fill up the screen
	- Uses 8-pixel characters
	- Datasheet sample includes graphics
- LCD-101_g: Added test to display internal character generator (CG) ROM characters
- LCD-101_h: Started parameterizing LCD settings


## Blog Post Pages ##

- [All Electronics LCD-101 (256×128 LCD) with Arduino](https://teelsys.com/2018/02/11/all-electronics-lcd-101-256x128-lcd-with-arduino/)
- [All Electronics LCD-101 (256×128 LCD) with Arduino – Part 2](https://teelsys.com/2020/07/16/all-electronics-lcd-101-256x128-lcd-with-arduino-part-2/)

## Images of LCD in stand with back-pack ##
![LCD in stand - Front](./Documentation/images/IMG_9093.JPG)
Front of LCD in stand


![LCD in stand - Back (I2C)](./Documentation/images/IMG_9096.JPG)
Back of the LCD in stand with I2C populated PCB and wiring to Arduino Uno


![LCD in stand - Back (SPI)](./Documentation/images/IMG_9099.JPG)
Back of the LCD in stand with SPI populated PCB and wiring to Arduino Uno

