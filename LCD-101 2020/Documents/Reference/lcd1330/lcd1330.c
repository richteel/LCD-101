
//*************************************************************
// LCD routines
//*************************************************************

// WriteCommand() : Sends Command to LCD controller
void WriteCommand(unsigned char CommandCode) 
{
	LCD_DATA_PORT=CommandCode;

	LCD_CONTROL_PORT.A0=1;
	LCD_CONTROL_PORT.LCD_WRITE=1;
	LCD_CONTROL_PORT.LCD_READ=1;

	LCD_CONTROL_PORT.LCD_WRITE=0;

	LCD_CONTROL_PORT.LCD_WRITE=1;
} // end WriteCommand

//*************************************************************
// WriteData() : Sends parameters or data to LCD controller
void WriteData(unsigned char data)
{
	LCD_DATA_PORT=data;
	LCD_CONTROL_PORT.A0=0;
      	LCD_CONTROL_PORT.LCD_WRITE=1;
      	LCD_CONTROL_PORT.LCD_READ=1;
      	
	LCD_CONTROL_PORT.LCD_WRITE=0;
		
	LCD_CONTROL_PORT.LCD_WRITE=1;

}

void lcdinit(void)
{
	// set I/O port directions
	LCD_DATA_DDR=0xFF; //set to outputs 
	LCD_CONTROL_DDR.LCD_RESET=OUT;
	LCD_CONTROL_DDR.A0=OUT;
	LCD_CONTROL_DDR.LCD_READ=OUT;
	LCD_CONTROL_DDR.LCD_WRITE=OUT;
	
	LCD_CONTROL_PORT.LCD_RESET=1;
	LCD_CONTROL_PORT.A0=1;
	LCD_CONTROL_PORT.LCD_READ=1;
	LCD_CONTROL_PORT.LCD_WRITE=1;
        	
	#if DELAYS
		delay_ms(2);
	#endif
	
	LCD_CONTROL_PORT.LCD_RESET=0;
	
	#if DELAYS
		delay_ms(2);
	#endif
	
	LCD_CONTROL_PORT.LCD_RESET=1;
	
	#if DELAYS
		delay_ms(2);
	#endif
	
	WriteCommand(SystemSet); //C Set LCD system
	WriteData(0b00110000);//P1
	WriteData(0x80|(CHAR_BITS_WIDE-1));//P2
	WriteData(0x07);//P3
	WriteData(BYTES_PER_LINE-1);//P4
	WriteData(BYTES_PER_LINE+5);//P5
	WriteData(/*199*/128);//P6
	WriteData(BYTES_PER_LINE);//P7
	WriteData(0);//P8
	WriteCommand(Overlay);
	WriteData(0x00);
	WriteCommand(Scroll);
	WriteData(0x00);
	WriteData(0x00);
	WriteData(0xC8);
	WriteData(0xE8);
	WriteData(0x03);
	WriteData(0xC8);
	WriteCommand(CSRForm);
	WriteData(0x04);
	WriteData(0x86);
	WriteCommand(CSRRight);
	WriteCommand(HorzScroll);
	WriteData(0x00);
	WriteCommand(DispON);
	WriteData(0x16);
	clearLCD(); //completely clears LCD
}

void clearLCD(void)
{
	unsigned short i;
	MoveCursor(0); //set cursor to the beginning of the character layer
	WriteCommand(MWRITE);
	
	for (i=0; i<1000; i++) 
	{
		WriteData(0x20); // write " " to character memory
	}
	
	Blank(0,0,32,128);
}

unsigned char ReadData(unsigned int address)
{

	unsigned char data;
	MoveCursor(address);
	WriteCommand(MREAD); //Set LCD to be ready to be read
	LCD_DATA_DDR=0x00; //set PORTC to be inputs
	LCD_DATA_PORT=0x00; //turn off all pull ups
	LCD_CONTROL_PORT.A0=1;
	LCD_CONTROL_PORT.LCD_READ=0;
	#asm("nop");
	data=LCD_DATA_PIN; //read in the old data
	LCD_CONTROL_PORT.LCD_READ=1;
	LCD_CONTROL_PORT.A0=0;
	LCD_DATA_DDR=0xFF;
	return data;
}


void MoveCursor(int addr)
{
	WriteCommand(CSRW);
	WriteData(addr);
	WriteData(addr>>8);
}

void DrawBitmap(unsigned char flash* bitmap, unsigned char x, unsigned char y)
{
	unsigned char w;
	unsigned char h;
	unsigned int i;
	unsigned char xi; 
	unsigned int address;

	address=1000+(unsigned int)y*32+x;
	
	MoveCursor(address); 
	WriteCommand(MWRITE);
	
	i=0;
	w=bitmap[i++];
	h=bitmap[i++]; 

	while(h--)
	{
		xi=w;
		while(xi--)
		{    
			WriteData(bitmap[i++]);
		}
		
		address+=32;				
		MoveCursor(address);
		WriteCommand(MWRITE);
	}
	
	return;
} 

void Blank(unsigned char x, unsigned char y, unsigned char w, unsigned char h)
{
	unsigned char xi; 
	unsigned int address;

	address=1000+(unsigned int)y*32+x;
	
	MoveCursor(address); 
	WriteCommand(MWRITE);

	while(h--)
	{
		xi=w;
		while(xi--)
		{    
			WriteData(0);
		}
		
		address+=32;				
		MoveCursor(address);
		WriteCommand(MWRITE);
	}
	
	return;
}           

void Pixel(unsigned char x, unsigned char y, unsigned char i)
{
	unsigned char data; 
	unsigned int address;
	address=1000+(unsigned int)y*32+(x/8);
	data=ReadData(address);
	if (i)
		data|=(1<<(7-x%8));  
	else
		data&=(255-(1<<(7-x%8)));

	MoveCursor(address);
	WriteCommand(MWRITE);
	WriteData(data);	

	return;
}

void Line(unsigned char x1, unsigned char y1, unsigned char x2, unsigned char y2, unsigned char i)
{
        float x;
        float xi;
        float y;
        float yi;
        signed int steps;
        signed int deltax;
        signed int deltay;
        
	x=x1;
	y=y1;
	
        deltax=(signed int)x1-(signed int)x2;
        if (deltax<0)
        	deltax*=-1;
        	
        deltay=(signed int)y1-(signed int)y2;
        if (deltay<0)
        	deltay*=-1;
        	
        steps=deltax+deltay;
        
        xi=((float)x2-x)/steps;
        yi=((float)y2-y)/steps;
        
        do
        {
        	Pixel((unsigned char)x, (unsigned char)y, i);
        	x+=xi;
        	y+=yi;
        }while(steps--);
        
  	return;
}              

void Box(unsigned char x1, unsigned char y1, unsigned char x2, unsigned char y2, unsigned char i)
{
	Line(x1,y1,x1,y2,i);
	Line(x1,y1,x2,y1,i);
	Line(x2,y1,x2,y2,i);
	Line(x1,y2,x2,y2,i);
	return;
} 

void WriteString(char *input)
{
	char i=0;
	WriteCommand(MWRITE);
	while (input[i]!=0)
	{
		WriteData(input[i]);
		i++;
	}
}

void LocateText(unsigned char x, unsigned char y)
{
	unsigned int address;
	address=(unsigned int)y*32+(unsigned int)x;
	WriteCommand(CSRW);
	WriteData(address);
	WriteData(address>>8);   
	WriteCommand(MWRITE);
	return;
}

#if LCD_HAS_EEPROM_SUPPORT
unsigned char DrawBitmapFromFile(unsigned char chEEPROM, unsigned char index, unsigned char x, unsigned char y) 
{
	unsigned char pchReadData[33];
	unsigned int nCurrentBitmap;
	unsigned char chBytesWide;	//width is a multiple of 4
	unsigned char chBytesHigh;	
	unsigned char chActualWide;	//actual width based on num of pixels wide
	unsigned int nStartofDataOffset;
	unsigned int nCurrentFileSize;
	unsigned int nBitmapSize;
	unsigned char chI;
	unsigned int nLCDAddress; 
	unsigned int nPixelsWide;  
	                           
	if(eeprom_rdsr(chEEPROM)&0x01)
		return 0;	//return failed- no eeprom or eeprom busy
		
	nCurrentBitmap=0;

	chI=0;
		
	while(chI++<index)
	{
		eeprom_read(chEEPROM, nCurrentBitmap+0x0002, pchReadData, 2);
		nCurrentFileSize=((unsigned int)(*(pchReadData+1))<<8)+(unsigned int)*pchReadData;
		nCurrentBitmap+=nCurrentFileSize;//nCurrentBitmap is the address of the start of the BMP file.		
	}  
       	
       	eeprom_read(chEEPROM, nCurrentBitmap, pchReadData, 1);
       	if (*pchReadData!='B')
       		return 0; //is not a bitmap
       		
	eeprom_read(chEEPROM, nCurrentBitmap+0x0002, pchReadData, 2);
	nCurrentFileSize=((unsigned int)(*(pchReadData+1))<<8)+(unsigned int)*pchReadData;
	
	eeprom_read(chEEPROM, nCurrentBitmap+0x0022, pchReadData, 2);
	nBitmapSize=((unsigned int)(*(pchReadData+1))<<8)+(unsigned int)*pchReadData;

	eeprom_read(chEEPROM, nCurrentBitmap+0x000A, pchReadData, 2); 
	nStartofDataOffset=((unsigned int)(*(pchReadData+1))<<8)+(unsigned int)*pchReadData;
	
	eeprom_read(chEEPROM, nCurrentBitmap+0x0012, pchReadData, 2);
	nPixelsWide=((unsigned int)(*(pchReadData+1))<<8)+(unsigned int)*pchReadData;;
	chActualWide=(unsigned char)(nPixelsWide>>3);
	if(nPixelsWide%8)
		chActualWide++;  
		
	eeprom_read(chEEPROM, nCurrentBitmap+0x0016, pchReadData, 1);
	chBytesHigh=*pchReadData;
	
	chBytesWide=nBitmapSize/chBytesHigh;
	
	/*
	sprintf(string, "Complete file size = 0x%4X %4i",nCurrentFileSize, nCurrentFileSize);
	LocateText(0,0);
	WriteString(string);	

	sprintf(string, "Bitmap size = 0x%4X %4i",nBitmapSize, nBitmapSize);
	LocateText(0,1);
	WriteString(string);
	
	sprintf(string, "Start of Data  = 0x%4X %4i",nStartofDataOffset, nStartofDataOffset);
	LocateText(0,2);
	WriteString(string);
	
	sprintf(string, "Width = %2i Height = %3i", chBytesWide, chBytesHigh);
	LocateText(0,3);
	WriteString(string);  
	
	sprintf(string, "Actual Wide = %3i", chActualWide, chActualWide);
	LocateText(0,4);
	WriteString(string);
	
	sprintf(string, "Pixels Wide = %3i", nPixelsWide, nPixelsWide);
	LocateText(0,5);
	WriteString(string);
	*/
	
	nLCDAddress=1000+(unsigned int)y*32+x;//calculate the address of the starting address on the LCD

	MoveCursor(nLCDAddress); 
	WriteCommand(MWRITE);  
	
	//the bitmap is up-side-down in the file, read from the end to beginning
	while(chBytesHigh--)
	{
		chI=chBytesWide;//I think this line is unnecessary?
		
		eeprom_read(chEEPROM, nCurrentBitmap+nStartofDataOffset+
			((unsigned int)chBytesHigh*(unsigned int)chBytesWide),
			pchReadData, chBytesWide);//read an entire line
			
		for(chI=0; chI<chActualWide; chI++)
			WriteData(~(*(pchReadData+chI)));
				
		nLCDAddress+=32;//move down the number of bytes in one LCD line.				
		MoveCursor(nLCDAddress);
		WriteCommand(MWRITE);
	}
			
	return 1; 
	
}  
#endif
      
#if LCD_HAS_EEPROM_SUPPORT
unsigned char CountBitmapFiles (unsigned char chEEPROM)
{
	unsigned char pchReadData[5];
	unsigned int nCurrentBitmap;
	unsigned int nCurrentFileSize;
	unsigned char chI;
	                           
	if(eeprom_rdsr(chEEPROM)&0x01)
		return 0;	//return failed- no eeprom or eeprom busy
		
	nCurrentBitmap=0;

	chI=0;
	
	do
	{
       	eeprom_read(chEEPROM, nCurrentBitmap, pchReadData, 1);
       	if (*pchReadData!='B')
       		return chI; //is not a bitmap
       	eeprom_read(chEEPROM, nCurrentBitmap+0x0002, pchReadData, 2);
	nCurrentFileSize=((unsigned int)(*(pchReadData+1))<<8)+(unsigned int)*pchReadData;
	nCurrentBitmap+=nCurrentFileSize;		
	chI++;
	}while(chI);	//will return 0 eventually if all else fails 
	
	return chI; //shouldn't execute, but if all else fails      	

}  
#endif                

void ShiftTextScreenUpOneLine(void)
{
	unsigned char chRow=0;
	unsigned char chColumn=0;
	unsigned char chData;   
	unsigned int nAddress;
	
	for(chRow=1; chRow<TEXT_ROWS; chRow++)
	{
		for(chColumn=0; chColumn<BYTES_PER_LINE; chColumn++)
		{
			nAddress=(unsigned int)chRow*32+(unsigned int)chColumn;
			chData=ReadData(nAddress);
			LocateText(chColumn, chRow-1);
			WriteData(chData);
		}
	} 
	
	LocateText(0, TEXT_ROWS-1);
	for(chColumn=0; chColumn<BYTES_PER_LINE; chColumn++)
	{
		WriteData(0);
	}
	
	return;
}
			
