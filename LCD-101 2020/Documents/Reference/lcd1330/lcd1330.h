#ifndef TRUE
	#define	TRUE	1
	#define	FALSE	0
#endif

#ifndef LCD_HAS_EEPROM_SUPPORT
	#define HAS_EEPROM_SUPPORT	FALSE
#endif

//definitions for LCD

/*
These definitions should be copied and pasted into the main program.
#define	DELAYS TRUE

#define	LCD_HAS_EEPROM_SUPPORT	_____

#define	LCD_CONTROL_PORT	PORT_
#define	LCD_CONTROL_PIN		PIN_
#define	LCD_CONTROL_DDR		DDR_

#define	LCD_RESET	_
#define	LCD_READ        _
#define	LCD_WRITE	_
#define	A0		_  

#define	LCD_DATA_PORT		PORT_
#define	LCD_DATA_PIN		PIN_
#define	LCD_DATA_DDR		DDR_  

for example,
#define	DELAYS TRUE

#define	LCD_HAS_EEPROM_SUPPORT	FALSE

#define	LCD_CONTROL_PORT	PORTB
#define	LCD_CONTROL_PIN		PINB
#define	LCD_CONTROL_DDR		DDRB

#define	LCD_RESET	0
#define	LCD_READ        1
#define	LCD_WRITE	2
#define	A0		3  

#define	LCD_DATA_PORT		PORTC
#define	LCD_DATA_PIN		PINC
#define	LCD_DATA_DDR		DDRC 

*/

//#ifndef	IN
  //	#define	IN	0
//#endif

#ifndef	OUT
	#define	OUT	1
#endif

#define LCDreset 0x00 // 00000000 Reset the Display
#define LCDnop 0x47 // 01000111 No operation
#define CmdSetup 0x47 // 01000111 Set A0 high
#define CmdWrite 0x43 // 01000011 Set WR low
#define DataSetup 0x07 // 00000111 Set A0 low
#define DataWrite 0x03 // 00000011 Set WR low
#define StatusRead 0x05 // 00000101 Set RD low, A0 low
#define DataRead 0x45 // 01000101 Set RD low, A0 high

// LCD Commands (PORTC)
#define SystemSet 0x40 // Initialize system
#define SleepIn 0x53 // Enter standby mode
#define DispOFF 0x58 // Display Off
#define DispON 0x59 // Display On
#define Scroll 0x44 // Initialize Address & Regions
#define CSRForm 0x5D // Set cursor type
#define CharAddr 0x5C // Set address of character RAM
#define CSRRight 0x4C // Cursor direction = right
#define CSRLeft 0x4D // Cursor direction = left
#define CSRUp 0x4E // Cursor direction = up
#define CSRDown 0x4F // Cursor direction = down
#define HorzScroll 0x5A // Set horz scroll position
#define Overlay 0x5B // Set Display Format
#define CSRW 0x46 // Set cursor address
#define CSRR 0x47 // Read cursor address
#define MWRITE 0x42 // Write to display memory
#define MREAD 0x43 // Read from display memory  

#define CHAR_BITS_WIDE	8  
#define BYTES_PER_LINE	32//8 bit
#define	TEXT_ROWS	16

// LCD drivers
#pragma used+
void WriteData(unsigned char CommandCode);
void lcdinit(void);
void clearLCD(void);
void WriteCommand(unsigned char CommandCode);
void MoveCursor(int addr); 
unsigned char ReadData(unsigned int address);  
void DrawBitmap(unsigned char flash* bitmap, unsigned char x, unsigned char y);
void Blank(unsigned char x, unsigned char y, unsigned char w, unsigned char h);
void Pixel(unsigned char x, unsigned char y, unsigned char i);
void Line(unsigned char x1, unsigned char y1, unsigned char x2, unsigned char y2, unsigned char i);
void Box(unsigned char x1, unsigned char y1, unsigned char x2, unsigned char y2, unsigned char i);
void WriteString(char *input);
void LocateText(unsigned char x, unsigned char y);
unsigned char DrawBitmapFromFile(unsigned char chEEPROM, unsigned char index, unsigned char x, unsigned char y);
unsigned char CountBitmapFiles (unsigned char chEEPROM);     
void ShiftTextScreenUpOneLine(void);
#pragma used-

#include "lcd1330.c"