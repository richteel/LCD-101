#ifndef _TeelSys_SED1330_
#define _TeelSys_SED1330_

#include "TeelSys_GFX.h"
#include "Arduino.h"
#include "Print.h"
#include "LCD_Connection.h"
#include "Wire.h"

#define SED1330_TFTWIDTH 256  ///< SED1330 max TFT width
#define SED1330_TFTHEIGHT 128 ///< SED1330 max TFT height

// Control Lines
#define SED1330_CTRL_RESET 0x00
#define SED1330_CTRL_NOP 0x0F
#define SED1330_CTRL_DATA_WRITE 0x03
#define SED1330_CTRL_STATUS_READ 0x05
#define SED1330_CTRL_COMMAND_WRITE 0x13
#define SED1330_CTRL_DISPLAY_READ 0x15

// Commands and Parameters
#define SED1330_NOP 0x00     /// No-op register

// System control
#define SED1330_SYSTEMSET 0x40  // Initialize device and display (8 Bytes)
#define SED1330_SLEEPIN 0x53  // Enter standby mode (0 Bytes)

// Display control
#define SED1330_DISP_OFF 0x58  // Enable and disable display and display flashing (1 Byte)
#define SED1330_DISP_ON 0x59  // Enable and disable display and display flashing (1 Byte)

#define SED1330_SCROLL 0x44  // Set display start address and display regions (10 Bytes)

#define SED1330_CSRFORM 0x5D  // Set cursor type (2 Bytes)

#define SED1330_CGRAM_ADR 0x5C  // Set start address of character generator RAM (2 Bytes)

#define SED1330_CSRDIR_RIGHT 0x4C  // Set direction of cursor movement (0 Bytes)
#define SED1330_CSRDIR_LEFT 0x4D  // Set direction of cursor movement (0 Bytes)
#define SED1330_CSRDIR_UP 0x4E  // Set direction of cursor movement (0 Bytes)
#define SED1330_CSRDIR_DOWN 0x4F  // Set direction of cursor movement (0 Bytes)

#define SED1330_HDOT_SCR 0x5A  // Set horizontal scroll position (1 Bytes)

#define SED1330_OVLAY 0x5B  // Set display overlay format (1 Bytes)

// Drawing control
#define SED1330_CSRW 0x46  // Set cursor address (2 Bytes)

#define SED1330_CSRR 0x47  // Read cursor address (2 Bytes)

#define SED1330_MWRITE 0x42  // Write to display memory (- Bytes)

#define SED1330_MREAD 0x43  // Read from display memory (- Bytes)



class TeelSys_SED1330   
{
  public:
    // Constructors
    TeelSys_SED1330();
    TeelSys_SED1330(byte i2cAddress);
    TeelSys_SED1330(byte res, byte rd, byte wr, byte cs, byte a0, byte d0, byte d1, byte d2, byte d3, byte d4, byte d5, byte d6, byte d7);

    // Basic LCD Functions
    void WriteData(byte data);
    void WriteCommand(byte command);
    void WriteControlAndData(byte command, byte data);

    // Other Public Functions
    void begin();
    void Reset();

    LCD_Connection SED1330_Connection;

  private:
    byte _lastControl = 0;
    
    
    // Private Functions
    void clearGraphicsLayer();
    void clearTextLayer();
    void lcdCommandWrite(byte command);
    void lcdDataWrite(byte data);
    void lcdWriteJustData(byte data);
    void lcdReset();
    void setupLcdPins4Read();
    void setupLcdPins4Write();
};



#endif // _TeelSys_SED1330_
