
#ifndef _TFT_CONNECTION_H_
#define _TFT_CONNECTION_H_

#define I2C_IODIR_REGISTER_A 0x00
#define I2C_IODIR_REGISTER_B 0x01
#define I2C_PORT_A 0x12
#define I2C_PORT_B 0x13

enum connection_types {
  unknown,
  parallel,
  spi,
  i2c
};

struct LCD_Connection {
  connection_types ConnectionType;
  byte RESET;
  byte RD;
  byte WR;
  byte CS;
  byte A0;
  byte D0;
  byte D1;
  byte D2;
  byte D3;
  byte D4;
  byte D5;
  byte D6;
  byte D7;
  byte I2cAddress;
};

#endif // _TFT_CONNECTION_H_
