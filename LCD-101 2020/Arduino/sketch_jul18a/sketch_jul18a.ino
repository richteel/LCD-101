#include "SPI.h"
#include "Adafruit_GFX.h"
#include "Adafruit_ILI9341.h"
#include "Wire.h"

#include "TeelSys_SED1330.h"
#include "TeelSys_GFX.h"
//#include "LCD_Connection.h"

#define TFT_DC 9
#define TFT_CS 10


//TeelSys_SED1330 tft = TeelSys_SED1330();  // I2C Connection Default is 0x20
//TeelSys_SED1330 tft = TeelSys_SED1330(0x20);  // I2C Connection
TeelSys_SED1330 tft = TeelSys_SED1330(16, 3, 17, 14, 15, 8, 9, 10, 11, 4, 5, 6, 7); // Parallel Connection

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  Serial.begin(9600);
  Serial.println(F("TeelSys_SED1330 Test!")); 
 
  tft.begin();
  
  //Serial.print(F("unknown -> "));
  //Serial.println(unknown);
  //Serial.print(F("parallel -> "));
  //Serial.println(parallel);
  //Serial.print(F("spi -> "));
  //Serial.println(spi);
  //Serial.print(F("i2c -> "));
  //Serial.println(i2c);
  
  //Serial.println(F(""));
  //Serial.print(F("SED1330_Connection.ConnectionType = "));
  //Serial.println(tft.SED1330_Connection.ConnectionType);
  //Serial.print(F("SED1330_Connection.A0 = "));
  //Serial.println(tft.SED1330_Connection.A0);
  //Serial.print(F("SED1330_Connection.CS = "));
  //Serial.println(tft.SED1330_Connection.CS);
  //Serial.print(F("SED1330_Connection.WR = "));
  //Serial.println(tft.SED1330_Connection.WR);
  //Serial.print(F("SED1330_Connection.RD = "));
  //Serial.println(tft.SED1330_Connection.RD);
  //Serial.print(F("SED1330_Connection.RESET = "));
  //Serial.println(tft.SED1330_Connection.RESET);
  //Serial.print(F("SED1330_Connection.D7 = "));
  //Serial.println(tft.SED1330_Connection.D7);
  //Serial.print(F("SED1330_Connection.D6 = "));
  //Serial.println(tft.SED1330_Connection.D6);
  //Serial.print(F("SED1330_Connection.D5 = "));
  //Serial.println(tft.SED1330_Connection.D5);
  //Serial.print(F("SED1330_Connection.D4 = "));
  //Serial.println(tft.SED1330_Connection.D4);
  //Serial.print(F("SED1330_Connection.D3 = "));
  //Serial.println(tft.SED1330_Connection.D3);
  //Serial.print(F("SED1330_Connection.D2 = "));
  //Serial.println(tft.SED1330_Connection.D2);
  //Serial.print(F("SED1330_Connection.D1 = "));
  //Serial.println(tft.SED1330_Connection.D1);
  //Serial.print(F("SED1330_Connection.D0 = "));
  //Serial.println(tft.SED1330_Connection.D0);
  //Serial.print(F("SED1330_Connection.I2cAddress = "));
  //Serial.println(tft.SED1330_Connection.I2cAddress);
  

  Serial.println(F("Done!"));
  digitalWrite(LED_BUILTIN, LOW);
}

void loop() {
  
}
