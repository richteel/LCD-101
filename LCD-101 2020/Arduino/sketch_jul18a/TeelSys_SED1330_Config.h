#ifndef _TeelSys_SED1330_Config_
#define _TeelSys_SED1330_Config_

#include "Arduino.h"

class TeelSys_SED1330_Config
{
  public:
    // Parameters
    // * Screen
    unsigned int LCD_WidthInPixels = 0x100;
    unsigned int LCD_HeightInPixels = 0x80;
    bool LCD_IsDualDisplay = false;
    bool LCD_IsPositiveDisplay = true;

    // * Character Size
    byte CharacterWidthInPixels = 0x8;
    byte CharacterHeightInPixels = 0x8;

    // * Memory
    unsigned int MemoryLayerStart_1 = 0x0;
    unsigned int MemoryLayerSize_1 = 0x200;
    unsigned int MemoryLayerStart_2 = 0x400;
    unsigned int MemoryLayerSize_2 = 0x1000;
    unsigned int MemoryLayerStart_3 = 0x200;
    unsigned int MemoryLayerSize_3 = 0x200;
    unsigned int MemoryLayerStart_4 = 0x0;
    unsigned int MemoryLayerSize_4 = 0x0;

    // * Cursor
    byte CursorFlashRate = 0x2;
    byte CursorWidthInPixels = 0x5;
    byte CursorHeightInPixels = 0x7;
    bool CursorBlock = true;

    // * Other
    bool UseExternalCharacterGeneratorROM = false;
    bool UseSED1336TvMode = false;
    bool Use2FrameAcDrive = true;
    byte ScreenFlashRate1 = 0x1;
    byte ScreenFlashRate2 = 0x1;
    byte ScreenFlashRate3 = 0x1;
    byte ScreenCompositionMethod = 0x1;
    bool DisplayOnlyGraphics = false;

    // Constructors

  private:
    // * Screen
    unsigned int LCD_WidthInCharacters = 0x20;
};



#endif // _TeelSys_SED1330_Config_
