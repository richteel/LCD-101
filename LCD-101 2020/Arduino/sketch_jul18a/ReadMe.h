/*

All Electronics LCD-101
https://www.allelectronics.com/item/lcd-101/256-x-128-lcd-panel/1.html

Hyundai # HG25504. 5.8" x 4.58" graphic display module with SED1330F onboard controller. Viewing area is 5" x 2.75". 5 Vdc logic. 18 Vdc LCD voltage.
  - Display format: 256 x 128 dots
  - Driving method: 1/128 Duty
  - Interface: 8080
  - Passive LCD capable of 16 shades of gray

TeelSys Blog Posts
  2018-02-11 - https://teelsys.com/2018/02/11/all-electronics-lcd-101-256x128-lcd-with-arduino/


Template for this solution is the Adafruit GFX library

*/
