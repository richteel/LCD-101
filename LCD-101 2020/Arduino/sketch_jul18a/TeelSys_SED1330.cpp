/*!
   @file TeelSys_SED1330.cpp

   This is part of TeelSys SED1330F driver for the Arduino platform.  It is
   designed specifically to work with All Electronics LCD-101.

   TeeSys invests time and resources providing this open source code,
   please support TeeSys and open-source hardware by purchasing
   products from TeeSys!

   Written by Richard Teel for TeeSys LLC.

   BSD license, all text here must be included in any redistribution.

  REF: I2C http://tronixstuff.com/2011/08/26/tutorial-maximising-your-arduinos-io-ports/
*/

#include "TeelSys_SED1330.h"

#define DELAY 2

// Public Methods
// Constructors
TeelSys_SED1330::TeelSys_SED1330()
{
  SED1330_Connection.ConnectionType = i2c;
  SED1330_Connection.I2cAddress = 0x20;
}

TeelSys_SED1330::TeelSys_SED1330(byte i2cAddress) {
  SED1330_Connection.ConnectionType = i2c;
  SED1330_Connection.I2cAddress = i2cAddress;
}

TeelSys_SED1330::TeelSys_SED1330(byte res, byte rd, byte wr, byte cs, byte a0, byte d0, byte d1, byte d2, byte d3, byte d4, byte d5, byte d6, byte d7)
{
  SED1330_Connection.ConnectionType = parallel;
  SED1330_Connection.A0 = a0;
  SED1330_Connection.CS = cs;
  SED1330_Connection.WR = wr;
  SED1330_Connection.RD = rd;
  SED1330_Connection.RESET = res;
  SED1330_Connection.D0 = d0;
  SED1330_Connection.D1 = d1;
  SED1330_Connection.D2 = d2;
  SED1330_Connection.D3 = d3;
  SED1330_Connection.D4 = d4;
  SED1330_Connection.D5 = d5;
  SED1330_Connection.D6 = d6;
  SED1330_Connection.D7 = d7;
}

void TeelSys_SED1330::begin() {
  Wire.begin();
  setupLcdPins4Write();
  Reset();
}

void TeelSys_SED1330::Reset() {
  lcdReset();

  lcdCommandWrite(SED1330_SYSTEMSET);  // No. 3
  lcdDataWrite(0x30); // P1
  lcdDataWrite(0x87); // P2
  lcdDataWrite(0x07); // P3
  lcdDataWrite(0x1F); // P4
  lcdDataWrite(0x23); // P5
  lcdDataWrite(0x7F); // P6
  lcdDataWrite(0x20); // P7
  lcdDataWrite(0x00); // P8

  lcdCommandWrite(SED1330_SCROLL);  // No. 4
  lcdDataWrite(0x00); // P1
  lcdDataWrite(0x00); // P2
  lcdDataWrite(0x7F); // P3
  lcdDataWrite(0x00); // P4
  lcdDataWrite(0x04); // P5
  lcdDataWrite(0x7F); // P6

  lcdCommandWrite(SED1330_HDOT_SCR);  // No. 5
  lcdDataWrite(0x00); // P1

  lcdCommandWrite(SED1330_OVLAY);  // No. 6
  lcdDataWrite(0x01); // P1 // 0x01

  lcdCommandWrite(SED1330_DISP_OFF);  // No. 7
  lcdDataWrite(0x56); // P1

  clearTextLayer(); // No. 8

  clearGraphicsLayer(); // No. 9

  lcdCommandWrite(SED1330_CSRW);  // No. 10
  lcdDataWrite(0x00); // P1
  lcdDataWrite(0x00); // P2

  lcdCommandWrite(SED1330_CSRFORM);  // No. 11
  lcdDataWrite(0x04); // P1
  lcdDataWrite(0x86); // P2

  lcdCommandWrite(SED1330_DISP_ON);  // No. 12
  lcdDataWrite(0x56); // P1


  
  
  
  
  
  
  
  
  lcdCommandWrite(SED1330_CSRDIR_RIGHT);  // No. 13

  lcdCommandWrite(SED1330_MWRITE);  // No. 14
  lcdDataWrite(0x20); // P1 " "
  lcdDataWrite(0x45); // P2 "E"
  lcdDataWrite(0x50); // P3 "P"
  lcdDataWrite(0x53); // P4 "S"
  lcdDataWrite(0x4F); // P5 "O"
  lcdDataWrite(0x4E); // P6 "N"

  lcdCommandWrite(SED1330_CSRW);  // No. 15
  lcdDataWrite(0x00); // P1
  lcdDataWrite(0x04); // P2

  lcdCommandWrite(SED1330_CSRDIR_DOWN);  // No. 16

  lcdCommandWrite(SED1330_MWRITE);  // No. 17
  lcdDataWrite(0xFF); // P1
  lcdDataWrite(0xFF); // P2
  lcdDataWrite(0xFF); // P3
  lcdDataWrite(0xFF); // P4
  lcdDataWrite(0xFF); // P5
  lcdDataWrite(0xFF); // P6
  lcdDataWrite(0xFF); // P7
  lcdDataWrite(0xFF); // P8
  lcdDataWrite(0xFF); // P9

  lcdCommandWrite(SED1330_CSRW);  // No. 18
  lcdDataWrite(0x01); // P1
  lcdDataWrite(0x04); // P2

  lcdCommandWrite(SED1330_MWRITE);  // No. 19
  lcdDataWrite(0xFF); // P1
  lcdDataWrite(0xFF); // P2
  lcdDataWrite(0xFF); // P3
  lcdDataWrite(0xFF); // P4
  lcdDataWrite(0xFF); // P5
  lcdDataWrite(0xFF); // P6
  lcdDataWrite(0xFF); // P7
  lcdDataWrite(0xFF); // P8
  lcdDataWrite(0xFF); // P9

  lcdCommandWrite(SED1330_CSRW);  // No. 20
  lcdDataWrite(0x02); // P1
  lcdDataWrite(0x04); // P2

  lcdCommandWrite(SED1330_MWRITE);  // No. 21
  lcdDataWrite(0xFF); // P1
  lcdDataWrite(0xFF); // P2
  lcdDataWrite(0xFF); // P3
  lcdDataWrite(0xFF); // P4
  lcdDataWrite(0xFF); // P5
  lcdDataWrite(0xFF); // P6
  lcdDataWrite(0xFF); // P7
  lcdDataWrite(0xFF); // P8
  lcdDataWrite(0xFF); // P9

  lcdCommandWrite(SED1330_CSRW);  // No. 22
  lcdDataWrite(0x03); // P1
  lcdDataWrite(0x04); // P2

  lcdCommandWrite(SED1330_MWRITE);  // No. 23
  lcdDataWrite(0xFF); // P1
  lcdDataWrite(0xFF); // P2
  lcdDataWrite(0xFF); // P3
  lcdDataWrite(0xFF); // P4
  lcdDataWrite(0xFF); // P5
  lcdDataWrite(0xFF); // P6
  lcdDataWrite(0xFF); // P7
  lcdDataWrite(0xFF); // P8
  lcdDataWrite(0xFF); // P9

  lcdCommandWrite(SED1330_CSRW);  // No. 24
  lcdDataWrite(0x04); // P1
  lcdDataWrite(0x04); // P2

  lcdCommandWrite(SED1330_MWRITE);  // No. 25
  lcdDataWrite(0xFF); // P1
  lcdDataWrite(0xFF); // P2
  lcdDataWrite(0xFF); // P3
  lcdDataWrite(0xFF); // P4
  lcdDataWrite(0xFF); // P5
  lcdDataWrite(0xFF); // P6
  lcdDataWrite(0xFF); // P7
  lcdDataWrite(0xFF); // P8
  lcdDataWrite(0xFF); // P9

  lcdCommandWrite(SED1330_CSRW);  // No. 26
  lcdDataWrite(0x05); // P1
  lcdDataWrite(0x04); // P2

  lcdCommandWrite(SED1330_MWRITE);  // No. 27
  lcdDataWrite(0xFF); // P1
  lcdDataWrite(0xFF); // P2
  lcdDataWrite(0xFF); // P3
  lcdDataWrite(0xFF); // P4
  lcdDataWrite(0xFF); // P5
  lcdDataWrite(0xFF); // P6
  lcdDataWrite(0xFF); // P7
  lcdDataWrite(0xFF); // P8
  lcdDataWrite(0xFF); // P9

  lcdCommandWrite(SED1330_CSRW);  // No. 28
  lcdDataWrite(0x06); // P1
  lcdDataWrite(0x04); // P2

  lcdCommandWrite(SED1330_MWRITE);  // No. 29
  lcdDataWrite(0xFF); // P1
  lcdDataWrite(0xFF); // P2
  lcdDataWrite(0xFF); // P3
  lcdDataWrite(0xFF); // P4
  lcdDataWrite(0xFF); // P5
  lcdDataWrite(0xFF); // P6
  lcdDataWrite(0xFF); // P7
  lcdDataWrite(0xFF); // P8
  lcdDataWrite(0xFF); // P9
  
  lcdCommandWrite(SED1330_CSRW);  // No. 30
  lcdDataWrite(0x00); // P1
  lcdDataWrite(0x01); // P2

  lcdCommandWrite(SED1330_CSRDIR_RIGHT);  // No. 31

  lcdCommandWrite(SED1330_MWRITE);  // No. 32
  lcdDataWrite(0x44); // P1
  lcdDataWrite(0x6F); // P2
  lcdDataWrite(0x74); // P3
  lcdDataWrite(0x20); // P4
  lcdDataWrite(0x4D); // P5
  lcdDataWrite(0x61); // P6
  lcdDataWrite(0x74); // P7
  lcdDataWrite(0x72); // P8
  lcdDataWrite(0x69); // P9
  lcdDataWrite(0x78); // P10
  lcdDataWrite(0x20); // P11
  lcdDataWrite(0x4C); // P12
  lcdDataWrite(0x43); // P13
  lcdDataWrite(0x44); // P14

  //*******************************************
  
  lcdCommandWrite(SED1330_CSRW);  // No. 30
  lcdDataWrite(0x20); // P1
  lcdDataWrite(0x01); // P2

  lcdCommandWrite(SED1330_CSRDIR_RIGHT);  // No. 31

  lcdCommandWrite(SED1330_MWRITE);  // No. 32
  lcdDataWrite(0x48); // P1
  lcdDataWrite(0x65); // P2
  lcdDataWrite(0x6C); // P3
  lcdDataWrite(0x6C); // P4
  lcdDataWrite(0x6F); // P5
  lcdDataWrite(0x20); // P6
  lcdDataWrite(0x57); // P7
  lcdDataWrite(0x6F); // P8
  lcdDataWrite(0x72); // P9
  lcdDataWrite(0x6C); // P10
  lcdDataWrite(0x64); // P11
}

// Private Functions
void TeelSys_SED1330::clearGraphicsLayer() {
  // Set Start at 03E8H
  lcdCommandWrite(SED1330_CSRW);
  lcdDataWrite(0x00);
  lcdDataWrite(0x04);
  
  lcdCommandWrite(SED1330_CSRDIR_RIGHT);  // No. 31

  // Write 00H (blank data) for 8000 bytes
  lcdCommandWrite(SED1330_MWRITE);
  //for (int i = 0; i < 8000; i++) {
  for (int i = 0x0400; i <= 0x13FF; i++) {
    lcdDataWrite(0x00);
    lcdDataWrite(0x00);
  }
}

void TeelSys_SED1330::clearTextLayer() {
  // Set Start at 0000H
  lcdCommandWrite(SED1330_CSRW);
  lcdDataWrite(0x00);
  lcdDataWrite(0x00);
  
  lcdCommandWrite(SED1330_CSRDIR_RIGHT);  // No. 31

  // Write 20H (space character) for 1000 bytes
  lcdCommandWrite(SED1330_MWRITE);
  for (int i = 0x0000; i <= 0x01FF; i++) {
    lcdDataWrite(0x20);
  }
}

void TeelSys_SED1330::lcdCommandWrite(byte command) {
  _lastControl = SED1330_CTRL_COMMAND_WRITE; // Used for I2C

  WriteControlAndData(_lastControl, command);
}

void TeelSys_SED1330::lcdDataWrite(byte data) {
  _lastControl = SED1330_CTRL_DATA_WRITE; // Used for I2C

  WriteControlAndData(_lastControl, data);
}

void TeelSys_SED1330::lcdWriteJustData(byte data) {
  if (SED1330_Connection.ConnectionType == i2c) {
    Wire.beginTransmission(SED1330_Connection.I2cAddress);
    Wire.write(I2C_PORT_B); // GPIOB
    Wire.write(data); // port B
    Wire.endTransmission();
  }
  else if (SED1330_Connection.ConnectionType == parallel) {
    digitalWrite(SED1330_Connection.D0, (data & 0x01) == 0x01);
    digitalWrite(SED1330_Connection.D1, (data & 0x02) == 0x02);
    digitalWrite(SED1330_Connection.D2, (data & 0x04) == 0x04);
    digitalWrite(SED1330_Connection.D3, (data & 0x08) == 0x08);
    digitalWrite(SED1330_Connection.D4, (data & 0x10) == 0x10);
    digitalWrite(SED1330_Connection.D5, (data & 0x20) == 0x20);
    digitalWrite(SED1330_Connection.D6, (data & 0x40) == 0x40);
    digitalWrite(SED1330_Connection.D7, (data & 0x80) == 0x80);
  }
}

void TeelSys_SED1330::setupLcdPins4Write() {
  if (SED1330_Connection.ConnectionType == i2c) {
    Wire.beginTransmission(SED1330_Connection.I2cAddress);
    Wire.write(I2C_IODIR_REGISTER_A); // IODIRA register
    Wire.write(0x00); // set all of port A to outputs
    Wire.endTransmission();

    Wire.beginTransmission(SED1330_Connection.I2cAddress);
    Wire.write(I2C_IODIR_REGISTER_B); // IODIRB register
    Wire.write(0x00); // set all of port B to outputs
    Wire.endTransmission();
  }
  else if (SED1330_Connection.ConnectionType == parallel) {
    pinMode(SED1330_Connection.A0, OUTPUT);
    pinMode(SED1330_Connection.CS, OUTPUT);
    pinMode(SED1330_Connection.WR, OUTPUT);
    pinMode(SED1330_Connection.RD, OUTPUT);
    pinMode(SED1330_Connection.RESET, OUTPUT);
    pinMode(SED1330_Connection.D0, OUTPUT);
    pinMode(SED1330_Connection.D1, OUTPUT);
    pinMode(SED1330_Connection.D2, OUTPUT);
    pinMode(SED1330_Connection.D3, OUTPUT);
    pinMode(SED1330_Connection.D4, OUTPUT);
    pinMode(SED1330_Connection.D5, OUTPUT);
    pinMode(SED1330_Connection.D6, OUTPUT);
    pinMode(SED1330_Connection.D7, OUTPUT);
  }
}

void TeelSys_SED1330::lcdReset() {
  if (SED1330_Connection.ConnectionType == i2c) {
    Wire.beginTransmission(SED1330_Connection.I2cAddress);
    Wire.write(I2C_PORT_A); // GPIOA
    Wire.write(0x17);
    Wire.endTransmission();

    //delay(2);
    Wire.beginTransmission(SED1330_Connection.I2cAddress);
    Wire.write(I2C_PORT_A); // GPIOA
    Wire.write(0x16);
    Wire.endTransmission();

    delay(2);
    Wire.beginTransmission(SED1330_Connection.I2cAddress);
    Wire.write(I2C_PORT_A); // GPIOA
    Wire.write(0x17);
    Wire.endTransmission();
  }
  else if (SED1330_Connection.ConnectionType == parallel) {
    digitalWrite(SED1330_Connection.CS, 0);
    digitalWrite(SED1330_Connection.WR, 1);
    digitalWrite(SED1330_Connection.RD, 1);
    digitalWrite(SED1330_Connection.A0, 1);
    digitalWrite(SED1330_Connection.RESET, 1);

    //delay(2);
    digitalWrite(SED1330_Connection.RESET, 0);

    delay(2);
    digitalWrite(SED1330_Connection.RESET, 1);
  }

  delay(4);
}

void TeelSys_SED1330::WriteControlAndData(byte control, byte data) {
  if (SED1330_Connection.ConnectionType == i2c) {
    Wire.beginTransmission(SED1330_Connection.I2cAddress);
    Wire.write(I2C_PORT_A); // GPIOA
    Wire.write(control | 0x04); // port A
    Wire.endTransmission();

    lcdWriteJustData(data);

    Wire.beginTransmission(SED1330_Connection.I2cAddress);
    Wire.write(I2C_PORT_A); // GPIOA
    Wire.write(control); // port A
    Wire.endTransmission();

    Wire.beginTransmission(SED1330_Connection.I2cAddress);
    Wire.write(I2C_PORT_A); // GPIOA
    Wire.write(control | 0x04); // port A
    Wire.endTransmission();
  }
  else if (SED1330_Connection.ConnectionType == parallel) {
    //**digitalWrite(SED1330_Connection.CS, 1);
    //delayMicroseconds(1);
    digitalWrite(SED1330_Connection.WR, 1);

    lcdWriteJustData(data);

    digitalWrite(SED1330_Connection.RESET, (control & 0x01) == 0x01);
    digitalWrite(SED1330_Connection.RD, (control & 0x02) == 0x02);
    digitalWrite(SED1330_Connection.A0, (control & 0x10) == 0x10);
    digitalWrite(SED1330_Connection.WR, (control & 0x04) == 0x04);
    digitalWrite(SED1330_Connection.CS, (control & 0x08) == 0x08);

    delayMicroseconds(20);

    digitalWrite(SED1330_Connection.WR, 1);
  }
}
