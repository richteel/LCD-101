/*!
   @file TeelSys_SED1330.cpp

   This is part of TeelSys SED1330F driver for the Arduino platform.  It is
   designed specifically to work with All Electronics LCD-101.

   TeeSys invests time and resources providing this open source code,
   please support TeeSys and open-source hardware by purchasing
   products from TeeSys!

   Written by Richard Teel for TeeSys LLC.

   BSD license, all text here must be included in any redistribution.

  REF: I2C http://tronixstuff.com/2011/08/26/tutorial-maximising-your-arduinos-io-ports/
*/

#include "SED1330_GFX.h"

// Public Methods
// Constructors
SED1330_GFX::SED1330_GFX()
{
  LCD_Connection.ConnectionType = i2c;
  LCD_Connection.I2cAddress = 0x20;
}

SED1330_GFX::SED1330_GFX(byte i2cAddress) {
  LCD_Connection.ConnectionType = i2c;
  LCD_Connection.I2cAddress = i2cAddress;
}

SED1330_GFX::SED1330_GFX(byte RESET, byte RD, byte WR, byte CS, byte A0, byte D0, byte D1, byte D2, byte D3, byte D4, byte D5, byte D6, byte D7)
{
  LCD_Connection.ConnectionType = parallel;
  LCD_Connection.A0 = A0;
  LCD_Connection.CS = CS;
  LCD_Connection.WR = WR;
  LCD_Connection.RD = RD;
  LCD_Connection.RESET = RESET;
  LCD_Connection.D0 = D0;
  LCD_Connection.D1 = D1;
  LCD_Connection.D2 = D2;
  LCD_Connection.D3 = D3;
  LCD_Connection.D4 = D4;
  LCD_Connection.D5 = D5;
  LCD_Connection.D6 = D6;
  LCD_Connection.D7 = D7;
}

void SED1330_GFX::begin() {
  Wire.begin();
  Reset();
  Init();
}

void SED1330_GFX::ClearDisplay() {
  lcdClearDisplay(LCD_Parameters.MemoryLayerStart_1, LCD_Parameters.MemoryLayerSize_1, 1);
  lcdClearDisplay(LCD_Parameters.MemoryLayerStart_2, LCD_Parameters.MemoryLayerSize_2, 2);
  lcdClearDisplay(LCD_Parameters.MemoryLayerStart_3, LCD_Parameters.MemoryLayerSize_3, 3);
  lcdClearDisplay(LCD_Parameters.MemoryLayerStart_4, LCD_Parameters.MemoryLayerSize_4, 4);
}

void SED1330_GFX::Init() {
  WriteCommand(CMD_SYSTEMSET);  // No. 3
  WriteData(0x30); // P1
  WriteData(0x87); // P2
  WriteData(0x07); // P3
  WriteData(0x1F); // P4
  WriteData(0x23); // P5
  WriteData(0x7F); // P6
  WriteData(0x20); // P7
  WriteData(0x00); // P8

  WriteCommand(CMD_SCROLL);  // No. 4
  WriteData(0x00); // P1
  WriteData(0x00); // P2
  WriteData(0x7F); // P3
  WriteData(0x00); // P4
  WriteData(0x04); // P5
  WriteData(0x7F); // P6

  WriteCommand(CMD_HDOT_SCR);  // No. 5
  WriteData(0x00); // P1

  WriteCommand(CMD_OVLAY);  // No. 6
  WriteData(0x01); // P1 // 0x01

  WriteCommand(CMD_DISP_OFF);  // No. 7
  WriteData(0x56); // P1

  ClearDisplay(); // No. 8 & 9

  WriteCommand(CMD_CSRW);  // No. 10
  WriteData(0x00); // P1
  WriteData(0x00); // P2

  WriteCommand(CMD_CSRFORM);  // No. 11
  WriteData(0x04); // P1
  WriteData(0x86); // P2

  WriteCommand(CMD_DISP_ON);  // No. 12
  WriteData(0x56); // P1
}

void SED1330_GFX::Reset() {
  setupConnection4Write();

  if (LCD_Connection.ConnectionType == i2c) {
    Wire.beginTransmission(LCD_Connection.I2cAddress);
    Wire.write(I2C_PORT_A); // GPIOA
    Wire.write(0x17);
    Wire.endTransmission();

    //delay(2);
    Wire.beginTransmission(LCD_Connection.I2cAddress);
    Wire.write(I2C_PORT_A); // GPIOA
    Wire.write(0x16);
    Wire.endTransmission();

    delay(2);
    Wire.beginTransmission(LCD_Connection.I2cAddress);
    Wire.write(I2C_PORT_A); // GPIOA
    Wire.write(0x17);
    Wire.endTransmission();
  }
  else if (LCD_Connection.ConnectionType == parallel) {
    digitalWrite(LCD_Connection.CS, 0);
    digitalWrite(LCD_Connection.WR, 1);
    digitalWrite(LCD_Connection.RD, 1);
    digitalWrite(LCD_Connection.A0, 1);
    digitalWrite(LCD_Connection.RESET, 1);

    //delay(2);
    digitalWrite(LCD_Connection.RESET, 0);

    delay(2);
    digitalWrite(LCD_Connection.RESET, 1);
  }

  delay(4);
}

void SED1330_GFX::SetParameters(unsigned int LCD_WidthInPixels,
                                unsigned int LCD_HeightInPixels,
                                bool LCD_IsDualDisplay,
                                bool LCD_IsPositiveDisplay,
                                byte CharacterWidthInPixels,
                                byte CharacterHeightInPixels,
                                unsigned int MemoryLayerStart_1,
                                unsigned int MemoryLayerSize_1,
                                unsigned int MemoryLayerStart_2,
                                unsigned int MemoryLayerSize_2,
                                unsigned int MemoryLayerStart_3,
                                unsigned int MemoryLayerSize_3,
                                unsigned int MemoryLayerStart_4,
                                unsigned int MemoryLayerSize_4,
                                bool UseExternalCharacterGeneratorROM,
                                unsigned int LCD_WidthInCharacters,
                                bool UseSED1336TvMode,
                                bool Use2FrameAcDrive,
                                byte CursorFlashRate,
                                byte ScreenFlashRate1,
                                byte ScreenFlashRate2,
                                byte ScreenFlashRate3,
                                byte CursorWidthInPixels,
                                byte CursorHeightInPixels,
                                bool CursorBlock,
                                byte ScreenCompositionMethod,
                                bool DisplayOnlyGraphics) {
  LCD_Parameters.LCD_WidthInPixels = LCD_WidthInPixels;
  LCD_Parameters.LCD_HeightInPixels = LCD_HeightInPixels;
  LCD_Parameters.LCD_IsDualDisplay = LCD_IsDualDisplay;
  LCD_Parameters.LCD_IsPositiveDisplay = LCD_IsPositiveDisplay;
  LCD_Parameters.CharacterWidthInPixels = CharacterWidthInPixels;
  LCD_Parameters.CharacterHeightInPixels = CharacterHeightInPixels;
  LCD_Parameters.MemoryLayerStart_1 = MemoryLayerStart_1;
  LCD_Parameters.MemoryLayerSize_1 = MemoryLayerSize_1;
  LCD_Parameters.MemoryLayerStart_2 = MemoryLayerStart_2;
  LCD_Parameters.MemoryLayerSize_2 = MemoryLayerSize_2;
  LCD_Parameters.MemoryLayerStart_3 = MemoryLayerStart_3;
  LCD_Parameters.MemoryLayerSize_3 = MemoryLayerSize_3;
  LCD_Parameters.MemoryLayerStart_4 = MemoryLayerStart_4;
  LCD_Parameters.MemoryLayerSize_4 = MemoryLayerSize_4;
  LCD_Parameters.UseExternalCharacterGeneratorROM = UseExternalCharacterGeneratorROM;
  LCD_Parameters.LCD_WidthInCharacters = LCD_WidthInCharacters;
  LCD_Parameters.UseSED1336TvMode = UseSED1336TvMode;
  LCD_Parameters.Use2FrameAcDrive = Use2FrameAcDrive;
  LCD_Parameters.CursorFlashRate = CursorFlashRate;
  LCD_Parameters.ScreenFlashRate1 = ScreenFlashRate1;
  LCD_Parameters.ScreenFlashRate2 = ScreenFlashRate2;
  LCD_Parameters.ScreenFlashRate3 = ScreenFlashRate3;
  LCD_Parameters.CursorWidthInPixels = CursorWidthInPixels;
  LCD_Parameters.CursorHeightInPixels = CursorHeightInPixels;
  LCD_Parameters.CursorBlock = CursorBlock;
  LCD_Parameters.ScreenCompositionMethod = ScreenCompositionMethod;
  LCD_Parameters.DisplayOnlyGraphics = DisplayOnlyGraphics;
}

void SED1330_GFX::Test() {
  WriteCommand(CMD_CSRDIR_RIGHT);  // No. 13

  WriteCommand(CMD_MWRITE);  // No. 14
  WriteData(0x20); // P1 " "
  WriteData(0x45); // P2 "E"
  WriteData(0x50); // P3 "P"
  WriteData(0x53); // P4 "S"
  WriteData(0x4F); // P5 "O"
  WriteData(0x4E); // P6 "N"

  WriteCommand(CMD_CSRW);  // No. 15
  WriteData(0x00); // P1
  WriteData(0x04); // P2

  WriteCommand(CMD_CSRDIR_DOWN);  // No. 16

  WriteCommand(CMD_MWRITE);  // No. 17
  WriteData(0xFF); // P1
  WriteData(0xFF); // P2
  WriteData(0xFF); // P3
  WriteData(0xFF); // P4
  WriteData(0xFF); // P5
  WriteData(0xFF); // P6
  WriteData(0xFF); // P7
  WriteData(0xFF); // P8
  WriteData(0xFF); // P9

  WriteCommand(CMD_CSRW);  // No. 18
  WriteData(0x01); // P1
  WriteData(0x04); // P2

  WriteCommand(CMD_MWRITE);  // No. 19
  WriteData(0xFF); // P1
  WriteData(0xFF); // P2
  WriteData(0xFF); // P3
  WriteData(0xFF); // P4
  WriteData(0xFF); // P5
  WriteData(0xFF); // P6
  WriteData(0xFF); // P7
  WriteData(0xFF); // P8
  WriteData(0xFF); // P9

  WriteCommand(CMD_CSRW);  // No. 20
  WriteData(0x02); // P1
  WriteData(0x04); // P2

  WriteCommand(CMD_MWRITE);  // No. 21
  WriteData(0xFF); // P1
  WriteData(0xFF); // P2
  WriteData(0xFF); // P3
  WriteData(0xFF); // P4
  WriteData(0xFF); // P5
  WriteData(0xFF); // P6
  WriteData(0xFF); // P7
  WriteData(0xFF); // P8
  WriteData(0xFF); // P9

  WriteCommand(CMD_CSRW);  // No. 22
  WriteData(0x03); // P1
  WriteData(0x04); // P2

  WriteCommand(CMD_MWRITE);  // No. 23
  WriteData(0xFF); // P1
  WriteData(0xFF); // P2
  WriteData(0xFF); // P3
  WriteData(0xFF); // P4
  WriteData(0xFF); // P5
  WriteData(0xFF); // P6
  WriteData(0xFF); // P7
  WriteData(0xFF); // P8
  WriteData(0xFF); // P9

  WriteCommand(CMD_CSRW);  // No. 24
  WriteData(0x04); // P1
  WriteData(0x04); // P2

  WriteCommand(CMD_MWRITE);  // No. 25
  WriteData(0xFF); // P1
  WriteData(0xFF); // P2
  WriteData(0xFF); // P3
  WriteData(0xFF); // P4
  WriteData(0xFF); // P5
  WriteData(0xFF); // P6
  WriteData(0xFF); // P7
  WriteData(0xFF); // P8
  WriteData(0xFF); // P9

  WriteCommand(CMD_CSRW);  // No. 26
  WriteData(0x05); // P1
  WriteData(0x04); // P2

  WriteCommand(CMD_MWRITE);  // No. 27
  WriteData(0xFF); // P1
  WriteData(0xFF); // P2
  WriteData(0xFF); // P3
  WriteData(0xFF); // P4
  WriteData(0xFF); // P5
  WriteData(0xFF); // P6
  WriteData(0xFF); // P7
  WriteData(0xFF); // P8
  WriteData(0xFF); // P9

  WriteCommand(CMD_CSRW);  // No. 28
  WriteData(0x06); // P1
  WriteData(0x04); // P2

  WriteCommand(CMD_MWRITE);  // No. 29
  WriteData(0xFF); // P1
  WriteData(0xFF); // P2
  WriteData(0xFF); // P3
  WriteData(0xFF); // P4
  WriteData(0xFF); // P5
  WriteData(0xFF); // P6
  WriteData(0xFF); // P7
  WriteData(0xFF); // P8
  WriteData(0xFF); // P9
  
  WriteCommand(CMD_CSRW);  // No. 30
  WriteData(0x00); // P1
  WriteData(0x01); // P2

  WriteCommand(CMD_CSRDIR_RIGHT);  // No. 31

  WriteCommand(CMD_MWRITE);  // No. 32
  WriteData(0x44); // P1
  WriteData(0x6F); // P2
  WriteData(0x74); // P3
  WriteData(0x20); // P4
  WriteData(0x4D); // P5
  WriteData(0x61); // P6
  WriteData(0x74); // P7
  WriteData(0x72); // P8
  WriteData(0x69); // P9
  WriteData(0x78); // P10
  WriteData(0x20); // P11
  WriteData(0x4C); // P12
  WriteData(0x43); // P13
  WriteData(0x44); // P14
}

void SED1330_GFX::WriteCommand(byte command) {
  _lastControl = CTRL_COMMAND_WRITE; // Used for I2C

  lcdWriteControlAndData(_lastControl, command);
}

void SED1330_GFX::WriteData(byte data) {
  _lastControl = CTRL_DATA_WRITE; // Used for I2C

  lcdWriteControlAndData(_lastControl, data);
}


// Private Functions
void SED1330_GFX::lcdClearDisplay(unsigned int StartAddress, unsigned int BlockSize, int SAD) {
  if (BlockSize == 0)
    return;

  byte data2Write = 0x20;

  if (SAD % 2 == 0 || LCD_Parameters.DisplayOnlyGraphics)
    data2Write = 0x00;

  WriteCommand(CMD_CSRW);
  WriteData(lowByte(StartAddress));
  WriteData(highByte(StartAddress));

  WriteCommand(CMD_CSRDIR_RIGHT);

  WriteCommand(CMD_MWRITE);
  for (int i = StartAddress; i < StartAddress + BlockSize; i++) {
    WriteData(data2Write);
  }
}

void SED1330_GFX::lcdWriteControlAndData(byte control, byte data) {
  if (LCD_Connection.ConnectionType == i2c) {
    Wire.beginTransmission(LCD_Connection.I2cAddress);
    Wire.write(I2C_PORT_A); // GPIOA
    Wire.write(control | 0x04); // port A
    Wire.endTransmission();

    lcdWriteJustData(data);

    Wire.beginTransmission(LCD_Connection.I2cAddress);
    Wire.write(I2C_PORT_A); // GPIOA
    Wire.write(control); // port A
    Wire.endTransmission();

    Wire.beginTransmission(LCD_Connection.I2cAddress);
    Wire.write(I2C_PORT_A); // GPIOA
    Wire.write(control | 0x04); // port A
    Wire.endTransmission();
  }
  else if (LCD_Connection.ConnectionType == parallel) {
    digitalWrite(LCD_Connection.WR, 1);

    lcdWriteJustData(data);

    digitalWrite(LCD_Connection.RESET, (control & 0x01) == 0x01);
    digitalWrite(LCD_Connection.RD, (control & 0x02) == 0x02);
    digitalWrite(LCD_Connection.A0, (control & 0x10) == 0x10);
    digitalWrite(LCD_Connection.WR, (control & 0x04) == 0x04);
    digitalWrite(LCD_Connection.CS, (control & 0x08) == 0x08);

    delayMicroseconds(20);

    digitalWrite(LCD_Connection.WR, 1);
  }
}

void SED1330_GFX::lcdWriteJustData(byte data) {
  if (LCD_Connection.ConnectionType == i2c) {
    Wire.beginTransmission(LCD_Connection.I2cAddress);
    Wire.write(I2C_PORT_B); // GPIOB
    Wire.write(data); // port B
    Wire.endTransmission();
  }
  else if (LCD_Connection.ConnectionType == parallel) {
    digitalWrite(LCD_Connection.D0, (data & 0x01) == 0x01);
    digitalWrite(LCD_Connection.D1, (data & 0x02) == 0x02);
    digitalWrite(LCD_Connection.D2, (data & 0x04) == 0x04);
    digitalWrite(LCD_Connection.D3, (data & 0x08) == 0x08);
    digitalWrite(LCD_Connection.D4, (data & 0x10) == 0x10);
    digitalWrite(LCD_Connection.D5, (data & 0x20) == 0x20);
    digitalWrite(LCD_Connection.D6, (data & 0x40) == 0x40);
    digitalWrite(LCD_Connection.D7, (data & 0x80) == 0x80);
  }
}

void SED1330_GFX::setupConnection4Read() {
  if (!_pinsSet4Write)
    return;

  if (LCD_Connection.ConnectionType == i2c) {
    Wire.beginTransmission(LCD_Connection.I2cAddress);
    Wire.write(I2C_IODIR_REGISTER_A); // IODIRA register
    Wire.write(0x00); // set all of port A to outputs
    Wire.endTransmission();

    Wire.beginTransmission(LCD_Connection.I2cAddress);
    Wire.write(I2C_IODIR_REGISTER_B); // IODIRB register
    Wire.write(0xFF); // set all of port B to inputs
    Wire.endTransmission();
  }
  else if (LCD_Connection.ConnectionType == parallel) {
    pinMode(LCD_Connection.A0, OUTPUT);
    pinMode(LCD_Connection.CS, OUTPUT);
    pinMode(LCD_Connection.WR, OUTPUT);
    pinMode(LCD_Connection.RD, OUTPUT);
    pinMode(LCD_Connection.RESET, OUTPUT);
    pinMode(LCD_Connection.D0, INPUT);
    pinMode(LCD_Connection.D1, INPUT);
    pinMode(LCD_Connection.D2, INPUT);
    pinMode(LCD_Connection.D3, INPUT);
    pinMode(LCD_Connection.D4, INPUT);
    pinMode(LCD_Connection.D5, INPUT);
    pinMode(LCD_Connection.D6, INPUT);
    pinMode(LCD_Connection.D7, INPUT);
  }

  _pinsSet4Write = false;
}

void SED1330_GFX::setupConnection4Write() {
  if (_pinsSet4Write)
    return;

  if (LCD_Connection.ConnectionType == i2c) {
    Wire.beginTransmission(LCD_Connection.I2cAddress);
    Wire.write(I2C_IODIR_REGISTER_A); // IODIRA register
    Wire.write(0x00); // set all of port A to outputs
    Wire.endTransmission();

    Wire.beginTransmission(LCD_Connection.I2cAddress);
    Wire.write(I2C_IODIR_REGISTER_B); // IODIRB register
    Wire.write(0x00); // set all of port B to outputs
    Wire.endTransmission();
  }
  else if (LCD_Connection.ConnectionType == parallel) {
    pinMode(LCD_Connection.A0, OUTPUT);
    pinMode(LCD_Connection.CS, OUTPUT);
    pinMode(LCD_Connection.WR, OUTPUT);
    pinMode(LCD_Connection.RD, OUTPUT);
    pinMode(LCD_Connection.RESET, OUTPUT);
    pinMode(LCD_Connection.D0, OUTPUT);
    pinMode(LCD_Connection.D1, OUTPUT);
    pinMode(LCD_Connection.D2, OUTPUT);
    pinMode(LCD_Connection.D3, OUTPUT);
    pinMode(LCD_Connection.D4, OUTPUT);
    pinMode(LCD_Connection.D5, OUTPUT);
    pinMode(LCD_Connection.D6, OUTPUT);
    pinMode(LCD_Connection.D7, OUTPUT);
  }

  _pinsSet4Write = true;
}
