/*!
   @file SED1330.cpp

   This is part of TeelSys SED1330 driver for the Arduino platform.  It is
   designed specifically to work with All Electronics LCD-101.

   TeeSys invests time and resources providing this open source code,
   please support TeeSys and open-source hardware by purchasing
   products from TeeSys!

   Written by Richard Teel for TeeSys LLC.

   BSD license, all text here must be included in any redistribution.

  REF: I2C http://tronixstuff.com/2011/08/26/tutorial-maximising-your-arduinos-io-ports/
*/

#include "TS_LCD.h"

size_t TS_LCD::write(uint8_t c)
{
  if (_debugTS_LCD && Serial) {
    Serial.print(F("TS_LCD::write(0x"));
    Serial.print(c < 16 ? "0" : "");
    Serial.print(c, HEX);
    Serial.println(F(") START"));
  }
  
  SetCursorDirection(CursorDirections::Right);

  if (c == 0x09) {
    // Move 3 chars
    Point p = Location();
    MoveTo(p.x + 3, p.y);
    return 1;
  }
  else if (c == 0x0D) {
    // Move to next line
    Point p = Location();
    MoveTo(0, p.y + 1);
    return 1;
  }
  else if (c < 0x10 || c > 0xDF) {
    return 1;
  }

  WriteCommand(MemoryWriteCommand);
  WriteData(c);
  
  if (_debugTS_LCD && Serial) {
    Serial.print(F("TS_LCD::write(0x"));
    Serial.print(c < 16 ? "0" : "");
    Serial.print(c, HEX);
    Serial.println(F(") END"));
  }

  return 1;
}
