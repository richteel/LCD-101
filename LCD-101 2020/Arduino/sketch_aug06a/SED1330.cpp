/*!
   @file SED1330.cpp

   This is part of TeelSys SED1330 driver for the Arduino platform.  It is
   designed specifically to work with All Electronics LCD-101.

   TeeSys invests time and resources providing this open source code,
   please support TeeSys and open-source hardware by purchasing
   products from TeeSys!

   Written by Richard Teel for TeeSys LLC.

   BSD license, all text here must be included in any redistribution.

  REF: I2C http://tronixstuff.com/2011/08/26/tutorial-maximising-your-arduinos-io-ports/
*/

#include "SED1330.h"

// ***************************************
// Public Methods
// ***************************************
// Constructors
SED1330::SED1330(byte i2cAddress) {
  Connection.ConnectionType = ConnectionTypes::I2C;
  Connection.I2cAddress = i2cAddress;
}

SED1330::SED1330(byte RESET, byte RD, byte WR, byte CS, byte A0, byte D0, byte D1, byte D2, byte D3, byte D4, byte D5, byte D6, byte D7)
{
  Connection.ConnectionType = ConnectionTypes::Parallel;
  Connection.A0 = A0;
  Connection.CS = CS;
  Connection.WR = WR;
  Connection.RD = RD;
  Connection.RESET = RESET;
  Connection.D0 = D0;
  Connection.D1 = D1;
  Connection.D2 = D2;
  Connection.D3 = D3;
  Connection.D4 = D4;
  Connection.D5 = D5;
  Connection.D6 = D6;
  Connection.D7 = D7;
}

void SED1330::begin() {
  // Set the Memory Write Command in TS_LCD
  MemoryWriteCommand = CMD_MWRITE;
  
  Wire.begin();
  Reset();
  Init();

  if (_debugSED1330 && Serial) {
    Serial.println(F("SED1330::begin()"));
  }
}

void SED1330::ClearDisplay() {
  unsigned int textMemorySize = Setting.WidthInCharacters * Setting.HeightInCharacters;
  unsigned int graphicMemorySize = Setting.WidthInCharacters * Setting.HeightInPixels;

  if (Setting.DisplayOnlyGraphics) {
    clearDisplayScreen(0, graphicMemorySize, true);
    clearDisplayScreen(graphicMemorySize, graphicMemorySize, true);
  }
  else {
    clearDisplayScreen(0, textMemorySize, false);
    clearDisplayScreen(textMemorySize, graphicMemorySize, true);
  }

  if (_debugSED1330 && Serial) {
    Serial.println(F("SED1330::ClearDisplay()"));
  }
}

void SED1330::Init() {
  WriteCommand(CMD_SYSTEMSET);  // No. 3
  WriteData(0x30); // P1
  WriteData(0x87); // P2
  WriteData(0x07); // P3
  WriteData(0x1F); // P4
  WriteData(0x23); // P5
  WriteData(0x7F); // P6
  WriteData(0x20); // P7
  WriteData(0x00); // P8

  WriteCommand(CMD_SCROLL);  // No. 4
  WriteData(0x00); // P1
  WriteData(0x00); // P2
  WriteData(0x7F); // P3
  WriteData(0x00); // P4
  WriteData(0x02); // P5
  WriteData(0x7F); // P6

  WriteCommand(CMD_HDOT_SCR);  // No. 5
  WriteData(0x00); // P1

  WriteCommand(CMD_OVLAY);  // No. 6
  WriteData(0x01); // P1 // 0x01

  WriteCommand(CMD_DISP_OFF);  // No. 7
  WriteData(0x56); // P1

  ClearDisplay(); // No. 8 & 9

  WriteCommand(CMD_CSRW);  // No. 10
  WriteData(0x00); // P1
  WriteData(0x00); // P2

  WriteCommand(CMD_CSRFORM);  // No. 11
  WriteData(0x04); // P1
  WriteData(0x86); // P2

  WriteCommand(CMD_DISP_ON);  // No. 12
  WriteData(0x56); // P1

  //lastMemPosText = 0;
  //lastMemPosGraphics = 0;

  if (_debugSED1330 && Serial) {
    Serial.println(F("SED1330::Init()"));
  }
}

Point SED1330::Location() {
  Point p;

  WriteCommand(CMD_CSRR);
  byte csrl = ReadData();
  byte csrh = ReadData();

  p.address = csrh << 8;
  p.address = p.address | csrl;

  p.y = p.address / Setting.WidthInCharacters;
  p.x = p.address - p.y * Setting.WidthInCharacters;

  if (_debugSED1330 && Serial) {
    Serial.print(F("SED1330::Location() = {address: 0x"));
    Serial.print(p.address < 4096 ? "0" : "");
    Serial.print(p.address < 256 ? "0" : "");
    Serial.print(p.address < 16 ? "0" : "");
    Serial.print(p.address, HEX);
    Serial.print(F(", x:0x"));
    Serial.print(p.x < 16 ? "0" : "");
    Serial.print(p.x, HEX);
    Serial.print(F(", y:0x"));
    Serial.print(p.y < 16 ? "0" : "");
    Serial.print(p.y, HEX);
    Serial.println(F("}"));
  }

  return p;
}

void SED1330::MoveTo(byte x, byte y) {
  byte c = x;
  byte r = y;

  if (c > Setting.WidthInCharacters)
    c = 0;

  if (r > Setting.HeightInCharacters)
    r = 0;

  unsigned int address = ((unsigned int)r * (unsigned int)Setting.WidthInCharacters) + (unsigned int)c;

  WriteCommand(CMD_CSRW);
  WriteData(lowByte(address));
  WriteData(highByte(address));

  if (_debugSED1330 && Serial) {
    Serial.print(F("SED1330::MoveTo(0x"));
    Serial.print(x < 16 ? "0" : "");
    Serial.print(x, HEX);
    Serial.print(F(", 0x"));
    Serial.print(y < 16 ? "0" : "");
    Serial.print(y, HEX);
    Serial.print(F(") / Address = 0x"));
    Serial.print(address < 4096 ? "0" : "");
    Serial.print(address < 256 ? "0" : "");
    Serial.print(address < 16 ? "0" : "");
    Serial.println(address, HEX);
  }
}

// Display data and cursor address read
// 0 0 0 A0 | /CS /WR /RD /RES
byte SED1330::ReadData() {
  _lastControl = I2C_CONTROL_READ_DATA;  // 0x15

  byte dataRead = readInfo();

  if (_debugSED1330 && Serial) {
    Serial.print(F("SED1330::ReadData() = 0x"));
    Serial.print(dataRead < 16 ? "0" : "");
    Serial.println(dataRead, HEX);
  }

  return dataRead;
}

// Status flag read
// 0 0 0 A0 | /CS /WR /RD /RES
byte SED1330::ReadStatus() {
  _lastControl = I2C_CONTROL_READ_STATUS;  // 0x05

  byte dataRead = readInfo();

  if (_debugSED1330 && Serial) {
    Serial.print(F("SED1330::ReadStatus() = 0x"));
    Serial.print(dataRead < 16 ? "0" : "");
    Serial.println(dataRead, HEX);
  }

  return dataRead;
}

// 0 0 0 A0 | /CS /WR /RD /RES
void SED1330::Reset() {
  setupConnection(true);

  if (Connection.ConnectionType == ConnectionTypes::I2C) {
    Wire.beginTransmission(Connection.I2cAddress);
    Wire.write(I2C_PORT_A); // GPIOA
    Wire.write(0x17);
    Wire.endTransmission();

    Wire.beginTransmission(Connection.I2cAddress);
    Wire.write(I2C_PORT_A); // GPIOA
    Wire.write(0x16);
    Wire.endTransmission();

    delay(2);
    Wire.beginTransmission(Connection.I2cAddress);
    Wire.write(I2C_PORT_A); // GPIOA
    Wire.write(0x17);
    Wire.endTransmission();
  }
  else if (Connection.ConnectionType == ConnectionTypes::Parallel) {
    digitalWrite(Connection.CS, 0);
    digitalWrite(Connection.WR, 1);
    digitalWrite(Connection.RD, 1);
    digitalWrite(Connection.A0, 1);
    digitalWrite(Connection.RESET, 1);

    digitalWrite(Connection.RESET, 0);

    delay(2);
    digitalWrite(Connection.RESET, 1);
  }

  delay(4);

  if (_debugSED1330 && Serial) {
    Serial.println(F("SED1330::Reset()"));
  }
}

void SED1330::SetCursorDirection(CursorDirections cDir) {
  if (_cursorDirection == cDir)
    return;

  _cursorDirection = cDir;

  switch (cDir) {
    case CursorDirections::Down:
      WriteCommand(CMD_CSRDIR_DOWN);
      break;
    case CursorDirections::Up:
      WriteCommand(CMD_CSRDIR_UP);
      break;
    case CursorDirections::Left:
      WriteCommand(CMD_CSRDIR_LEFT);
      break;
    default:  // right
      WriteCommand(CMD_CSRDIR_RIGHT);
      break;
  }

  if (_debugSED1330 && Serial) {
    Serial.print(F("SED1330::SetCursorDirection("));
    Serial.print(cDir);
    Serial.println(F(")"));
  }
}

// Command write
// 0 0 0 A0 | /CS /WR /RD /RES
void SED1330::WriteCommand(byte command) {
  if ((command == CMD_MWRITE || command == CMD_MREAD) && command == _lastCommand)
    return;

  if (_debugSED1330 && Serial) {
    Serial.print(F("SED1330::WriteCommand <command: "));
    Serial.print(command < 16 ? "0" : "");
    Serial.print(command, HEX);
    Serial.print(F(", _lastCommand: "));
    Serial.print(_lastCommand < 16 ? "0" : "");
    Serial.print(_lastCommand, HEX);
    Serial.print(F(", CMD_MWRITE: "));
    Serial.print(CMD_MWRITE < 16 ? "0" : "");
    Serial.print(CMD_MWRITE, HEX);
    Serial.println(F(">"));
  }

  _lastControl = I2C_CONTROL_WRITE_COMMAND;  // 0x13
  _lastCommand = command; // Used to speed up Print::write

  writeInfo(_lastCommand);

  if (_debugSED1330 && Serial) {
    Serial.print(F("SED1330::WriteCommand(0x"));
    Serial.print(_lastCommand < 16 ? "0" : "");
    Serial.print(_lastCommand, HEX);
    Serial.println(F(")"));
  }
}

// Display data and parameter write
// 0 0 0 A0 | /CS /WR /RD /RES
void SED1330::WriteData(byte data2Write) {
  _lastControl = I2C_CONTROL_WRITE_DATA;  // 0x03

  writeInfo(data2Write);

  if (_debugSED1330 && Serial) {
    Serial.print(F("SED1330::WriteData(0x"));
    Serial.print(data2Write < 16 ? "0" : "");
    Serial.print(data2Write, HEX);
    Serial.println(F(")"));
  }
}

// ***************************************
// Private Methods
// ***************************************
void SED1330::clearDisplayScreen(unsigned int StartAddress, unsigned int BlockSize, bool GraphicsScreen) {
  if (BlockSize == 0)
    return;

  bool debugStatus = _debugSED1330;
  _debugSED1330 = false;

  byte data2Write = 0x20;

  if (GraphicsScreen)
    data2Write = 0x00;

  WriteCommand(CMD_CSRW);
  WriteData(lowByte(StartAddress));
  WriteData(highByte(StartAddress));

  WriteCommand(CMD_CSRDIR_RIGHT);

  WriteCommand(CMD_MWRITE);
  for (int i = StartAddress; i < StartAddress + BlockSize; i++) {
    WriteData(data2Write);
  }

  _debugSED1330 = debugStatus;

  if (_debugSED1330 && Serial) {
    Serial.print(F("SED1330::clearDisplayScreen(0x"));
    Serial.print(StartAddress < 4096 ? "0" : "");
    Serial.print(StartAddress < 256 ? "0" : "");
    Serial.print(StartAddress < 16 ? "0" : "");
    Serial.print(StartAddress, HEX);
    Serial.print(F(", "));
    Serial.print(BlockSize < 4096 ? "0" : "");
    Serial.print(BlockSize < 256 ? "0" : "");
    Serial.print(BlockSize < 16 ? "0" : "");
    Serial.print(BlockSize, HEX);
    Serial.print(F(", "));
    Serial.print(GraphicsScreen);
    Serial.println(F(")"));
  }
}

// 0 0 0 A0 | /CS /WR /RD /RES
byte SED1330::readInfo() {
  byte dataRead = 0;

  if (_pinsSet4Write)
    setupConnection(false);

  if (Connection.ConnectionType == ConnectionTypes::I2C) {
    Wire.beginTransmission(Connection.I2cAddress);
    Wire.write(I2C_PORT_A); // GPIOA
    Wire.write(_lastControl | 0x02); // port A
    Wire.endTransmission();

    Wire.beginTransmission(Connection.I2cAddress);
    Wire.write(I2C_PORT_A); // GPIOA
    Wire.write(_lastControl); // port A
    Wire.endTransmission();

    dataRead = readJustData();

    Wire.beginTransmission(Connection.I2cAddress);
    Wire.write(I2C_PORT_A); // GPIOA
    Wire.write(_lastControl | 0x02); // port A
    Wire.endTransmission();
  }
  else if (Connection.ConnectionType == ConnectionTypes::Parallel) {
    digitalWrite(Connection.RD, 1);

    digitalWrite(Connection.RESET, (_lastControl & 0x01) == 0x01);
    digitalWrite(Connection.A0, (_lastControl & 0x10) == 0x10);
    digitalWrite(Connection.WR, (_lastControl & 0x04) == 0x04);
    digitalWrite(Connection.CS, (_lastControl & 0x08) == 0x08);
    digitalWrite(Connection.RD, (_lastControl & 0x02) == 0x02);

    delayMicroseconds(20);

    dataRead = readJustData();

    digitalWrite(Connection.RD, 1);
  }

  if (_debugSED1330 && Serial) {
    Serial.print(F("SED1330::readInfo() = 0x"));
    Serial.print(dataRead < 16 ? "0" : "");
    Serial.println(dataRead, HEX);
  }

  return dataRead;
}

byte SED1330::readJustData() {
  byte dataRead = 0;

  if (Connection.ConnectionType == ConnectionTypes::I2C) {
    Wire.beginTransmission(Connection.I2cAddress);
    Wire.write(I2C_PORT_B); // GPIOB
    Wire.endTransmission();

    Wire.requestFrom(Connection.I2cAddress, (uint8_t)1);

    while (Wire.available()) {
      dataRead = Wire.read(); // port B
    }
  }
  else if (Connection.ConnectionType == ConnectionTypes::Parallel) {
    dataRead = digitalRead(Connection.D0);
    dataRead = dataRead | digitalRead(Connection.D1) << 1;
    dataRead = dataRead | digitalRead(Connection.D2) << 2;
    dataRead = dataRead | digitalRead(Connection.D3) << 3;
    dataRead = dataRead | digitalRead(Connection.D4) << 4;
    dataRead = dataRead | digitalRead(Connection.D5) << 5;
    dataRead = dataRead | digitalRead(Connection.D6) << 6;
    dataRead = dataRead | digitalRead(Connection.D7) << 7;
  }

  if (_debugSED1330 && Serial) {
    Serial.print(F("SED1330::readJustData() = 0x"));
    Serial.print(dataRead < 16 ? "0" : "");
    Serial.println(dataRead, HEX);
  }

  return dataRead;
}

void SED1330::setupConnection(bool connectionWrite) {
  if (_pinsSet4Write == connectionWrite)
    return;

  if (Connection.ConnectionType == ConnectionTypes::I2C) {
    Wire.beginTransmission(Connection.I2cAddress);
    Wire.write(I2C_IODIR_REGISTER_A); // IODIRA register
    Wire.write(0x00); // set all of port A to outputs
    Wire.endTransmission();

    Wire.beginTransmission(Connection.I2cAddress);
    Wire.write(I2C_IODIR_REGISTER_B); // IODIRB register

    if (connectionWrite)
      Wire.write(0x00); // set all of port A to outputs
    else
      Wire.write(0xFF); // set all of port A to inputs

    Wire.endTransmission();
  }
  else if (Connection.ConnectionType == ConnectionTypes::Parallel) {
    pinMode(Connection.A0, OUTPUT);
    pinMode(Connection.CS, OUTPUT);
    pinMode(Connection.WR, OUTPUT);
    pinMode(Connection.RD, OUTPUT);
    pinMode(Connection.RESET, OUTPUT);

    if (connectionWrite) {
      pinMode(Connection.D0, OUTPUT);
      pinMode(Connection.D1, OUTPUT);
      pinMode(Connection.D2, OUTPUT);
      pinMode(Connection.D3, OUTPUT);
      pinMode(Connection.D4, OUTPUT);
      pinMode(Connection.D5, OUTPUT);
      pinMode(Connection.D6, OUTPUT);
      pinMode(Connection.D7, OUTPUT);
    }
    else {
      pinMode(Connection.D0, INPUT);
      pinMode(Connection.D1, INPUT);
      pinMode(Connection.D2, INPUT);
      pinMode(Connection.D3, INPUT);
      pinMode(Connection.D4, INPUT);
      pinMode(Connection.D5, INPUT);
      pinMode(Connection.D6, INPUT);
      pinMode(Connection.D7, INPUT);
    }
  }

  _pinsSet4Write = connectionWrite;

  if (_debugSED1330 && Serial) {
    Serial.print(F("SED1330::setupConnection("));
    Serial.print(connectionWrite);
    Serial.println(F(")"));
  }
}

// 0 0 0 A0 | /CS /WR /RD /RES
void SED1330::writeInfo(byte command) {
  //_lastCommand = command; // Used to speed up Print::write

  if (!_pinsSet4Write)
    setupConnection(true);

  if (Connection.ConnectionType == ConnectionTypes::I2C) {
    Wire.beginTransmission(Connection.I2cAddress);
    Wire.write(I2C_PORT_A); // GPIOA
    Wire.write(_lastControl | 0x04); // port A
    Wire.endTransmission();

    writeJustData(command);

    Wire.beginTransmission(Connection.I2cAddress);
    Wire.write(I2C_PORT_A); // GPIOA
    Wire.write(_lastControl); // port A
    Wire.endTransmission();

    Wire.beginTransmission(Connection.I2cAddress);
    Wire.write(I2C_PORT_A); // GPIOA
    Wire.write(_lastControl | 0x04); // port A
    Wire.endTransmission();
  }
  else if (Connection.ConnectionType == ConnectionTypes::Parallel) {
    digitalWrite(Connection.WR, 1);

    writeJustData(command);

    digitalWrite(Connection.RESET, (_lastControl & 0x01) == 0x01);
    digitalWrite(Connection.RD, (_lastControl & 0x02) == 0x02);
    digitalWrite(Connection.A0, (_lastControl & 0x10) == 0x10);
    digitalWrite(Connection.WR, (_lastControl & 0x04) == 0x04);
    digitalWrite(Connection.CS, (_lastControl & 0x08) == 0x08);

    delayMicroseconds(20);

    digitalWrite(Connection.WR, 1);
  }

  if (_debugSED1330 && Serial) {
    Serial.print(F("SED1330::writeInfo(0x"));
    Serial.print(command < 16 ? "0" : "");
    Serial.print(command, HEX);
    Serial.println(F(")"));
  }
}

void SED1330::writeJustData(byte data2Write) {
  if (Connection.ConnectionType == ConnectionTypes::I2C) {
    Wire.beginTransmission(Connection.I2cAddress);
    Wire.write(I2C_PORT_B); // GPIOB
    Wire.write(data2Write); // port B
    Wire.endTransmission();
  }
  else if (Connection.ConnectionType == ConnectionTypes::Parallel) {
    digitalWrite(Connection.D0, (data2Write & 0x01) == 0x01);
    digitalWrite(Connection.D1, (data2Write & 0x02) == 0x02);
    digitalWrite(Connection.D2, (data2Write & 0x04) == 0x04);
    digitalWrite(Connection.D3, (data2Write & 0x08) == 0x08);
    digitalWrite(Connection.D4, (data2Write & 0x10) == 0x10);
    digitalWrite(Connection.D5, (data2Write & 0x20) == 0x20);
    digitalWrite(Connection.D6, (data2Write & 0x40) == 0x40);
    digitalWrite(Connection.D7, (data2Write & 0x80) == 0x80);
  }

  if (_debugSED1330 && Serial) {
    Serial.print(F("SED1330::writeJustData(0x"));
    Serial.print(data2Write < 16 ? "0" : "");
    Serial.print(data2Write, HEX);
    Serial.println(F(")"));
  }
}
