#ifndef _SED1330_
#define _SED1330_

#if (ARDUINO >= 100)
  #include "Arduino.h"
#else
  #include "WProgram.h"
#endif

#include "SED1330_Commands.h"
#include "LCD_Connection.h"
#include "LCD_Setting.h"
#include "TS_LCD.h"
#include "Wire.h"





class SED1330 : public TS_LCD
{
  public:
    LCD_Connection Connection = {ConnectionTypes::I2C, 16, 3, 17, 14, 15, 8, 9, 10, 11, 4, 5, 6, 7, 0x20};
    LCD_Setting Setting = {256, 128, 32, 16, 8, 8, false};

    // Constructors
    SED1330(byte i2cAddress=0x20);
    SED1330(byte RESET, byte RD, byte WR, byte CS, byte A0, byte D0, byte D1, byte D2, byte D3, byte D4, byte D5, byte D6, byte D7);

    // Methods
    void begin();
    void ClearDisplay();
    void Init();
    void Reset();
    Point Location();
    using TS_LCD::Location; // pull in Location() from TS_LCD
    void MoveTo(byte x, byte y);
    using TS_LCD::MoveTo; // pull in MoveTo(byte x, byte y) from TS_LCD
    void SetCursorDirection(CursorDirections cDir);
    using TS_LCD::SetCursorDirection; // pull in SetCursorDirection(CursorDirections) from TS_LCD

    // Low Level Operations
    byte ReadData();                  // Display data and cursor address read
    using TS_LCD::ReadData;
    byte ReadStatus();                // Status flag read
    using TS_LCD::ReadStatus;
    void WriteCommand(byte command);  // Command write
    using TS_LCD::WriteCommand;
    void WriteData(byte data2Write);        // Display data and parameter write
    using TS_LCD::WriteData;
  
  private:
    // Fields
    CursorDirections _cursorDirection = CursorDirections::Unknown;
    bool _pinsSet4Write = false;
    byte _lastCommand = 0;
    byte _lastControl = 0;
    bool _debugSED1330 = false;

    // Methods
    void clearDisplayScreen(unsigned int StartAddress, unsigned int BlockSize, bool GraphicsScreen);
    void setupConnection(bool connectionWrite);
    
    // Low Level Operations
    byte readInfo();
    byte readJustData();
    void writeInfo(byte data2Write);
    void writeJustData(byte data2Write);
    
  
    //void lcdWriteControlAndData(byte command, byte data);
};


#endif // _SED1330_
