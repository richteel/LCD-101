#include "SED1330.h"

//SED1330 tft();  // I2C Connection Default is 0x20
//SED1330 tft(0x20);  // I2C Connection
SED1330 tft(16, 3, 17, 14, 15, 8, 9, 10, 11, 4, 5, 6, 7); // Parallel Connection

void test() {
  tft.print(F("Hello World\r\n"));
  
  Point p = tft.Location();
  
  tft.println(F("\tHello World"));
  tft.println(F("\t\tHello World"));
  
  Serial.print("Cursor Address: 0x");
  Serial.print(p.address < 4096 ? "0" : "");
  Serial.print(p.address < 256 ? "0" : "");
  Serial.print(p.address < 16 ? "0" : "");
  Serial.print(p.address, HEX);
  Serial.print(F(" ("));
  Serial.print(p.address);
  Serial.println(F(")"));

  Serial.print("Cursor X: 0x");
  Serial.print(p.x < 16 ? "0" : "");
  Serial.print(p.x, HEX);
  Serial.print(F(" ("));
  Serial.print(p.x);
  Serial.println(F(")"));

  Serial.print("Cursor Y: 0x");
  Serial.print(p.y < 16 ? "0" : "");
  Serial.print(p.y, HEX);
  Serial.print(F(" ("));
  Serial.print(p.y);
  Serial.println(F(")"));
}

void setup() {
  // put your setup code here, to run once:
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  Serial.begin(9600);
  Serial.println(F("TeelSys_SED1330 Test!"));

  tft.Setting = {256, 128, 32, 16, 8, 8, false};
  tft.begin();

  Serial.println(F("Begin Done!"));
  digitalWrite(LED_BUILTIN, LOW);

  test();

  Serial.println(F("Test Done!"));
}

void loop() {
  // put your main code here, to run repeatedly:


}
