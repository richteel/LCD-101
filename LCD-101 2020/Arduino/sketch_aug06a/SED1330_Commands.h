#ifndef _SED1330_Commands_
#define _SED1330_Commands_

// System control
#define CMD_SYSTEMSET 0x40  // Initialize device and display (8 Bytes)
#define CMD_SLEEPIN 0x53  // Enter standby mode (0 Bytes)

// Display control
#define CMD_DISP_OFF 0x58  // Enable and disable display and display flashing (1 Byte)
#define CMD_DISP_ON 0x59  // Enable and disable display and display flashing (1 Byte)

#define CMD_SCROLL 0x44  // Set display start address and display regions (10 Bytes)

#define CMD_CSRFORM 0x5D  // Set cursor type (2 Bytes)

#define CMD_CGRAM_ADR 0x5C  // Set start address of character generator RAM (2 Bytes)

#define CMD_CSRDIR_RIGHT 0x4C  // Set direction of cursor movement (0 Bytes)
#define CMD_CSRDIR_LEFT 0x4D  // Set direction of cursor movement (0 Bytes)
#define CMD_CSRDIR_UP 0x4E  // Set direction of cursor movement (0 Bytes)
#define CMD_CSRDIR_DOWN 0x4F  // Set direction of cursor movement (0 Bytes)

#define CMD_HDOT_SCR 0x5A  // Set horizontal scroll position (1 Bytes)

#define CMD_OVLAY 0x5B  // Set display overlay format (1 Bytes)

// Drawing control
#define CMD_CSRW 0x46  // Set cursor address (2 Bytes)

#define CMD_CSRR 0x47  // Read cursor address (2 Bytes)

#define CMD_MWRITE 0x42  // Write to display memory (- Bytes)

#define CMD_MREAD 0x43  // Read from display memory (- Bytes)


#endif // _SED1330_Commands_
