#ifndef _LCD_Setting_
#define _LCD_Setting_

struct LCD_Setting {
  unsigned int WidthInPixels;
  unsigned int HeightInPixels;
  unsigned int WidthInCharacters;
  unsigned int HeightInCharacters;
  byte CharacterWidthInPixels;
  byte CharacterHeightInPixels;
  bool DisplayOnlyGraphics;
};

#endif // _LCD_Setting_
