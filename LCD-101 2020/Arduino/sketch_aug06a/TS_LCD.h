#ifndef _TS_LCD_
#define _TS_LCD_

#if (ARDUINO >= 100)
  #include "Arduino.h"
#else
  #include "WProgram.h"
#endif

#include "Print.h"

struct CursorDirections {
  enum Type {
    Unknown,
    Right,
    Left,
    Up,
    Down
  };
  Type t_;
  CursorDirections(Type t) : t_(t) {}
  operator Type () const {
    return t_;
  }
};

struct Point {
  byte x = 0;
  byte y = 0;
  unsigned int address = 0;
};

class TS_LCD : public Print
{
  public:
    size_t write(uint8_t);
    byte MemoryWriteCommand = 0;
    
    //size_t write(const uint8_t *buffer, size_t size);    
    using Print::write; // pull in write(str) and write(buf, size) from Print

    virtual Point Location();
    virtual void MoveTo(byte x, byte y);
    virtual byte ReadData();                  // Display data and cursor address read
    virtual byte ReadStatus();                // Status flag read
    virtual void SetCursorDirection(CursorDirections cDir);
    virtual void WriteCommand(byte command);  // Command write
    virtual void WriteData(byte data2Write);  // Display data and parameter write

    //virtual size_t write(uint8_t) = 0;
    
  private:
    bool _debugTS_LCD = false;

  
};


#endif // _TS_LCD_
