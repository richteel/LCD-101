


byte lcdReadJustData() {
  byte dataRead = 0;

  dataRead = 1;
  dataRead = dataRead | 0 << 1;
  dataRead = dataRead | 1 << 2;
  dataRead = dataRead | 0 << 3;
  dataRead = dataRead | 1 << 4;
  dataRead = dataRead | 0 << 5;
  dataRead = dataRead | 1 << 6;
  dataRead = dataRead | 0 << 7;

  return dataRead;
}

void setup() {
  // put your setup code here, to run once:

  Serial.begin(9600);
  Serial.println(F("Shift Demo"));

  Serial.print(F("Test Value: "));
  Serial.println(lcdReadJustData());
}

void loop() {
  // put your main code here, to run repeatedly:

}
