#ifndef _SED1330_GFX_
#define _SED1330_GFX_

#if (ARDUINO >= 100)
  #include "Arduino.h"
#else
  #include "WProgram.h"
#endif

#include "Print.h"
#include "Wire.h"
#include "SED1330_Settings.h"

// Control Lines
#define CTRL_RESET 0x00
#define CTRL_NOP 0x0F
#define CTRL_DATA_WRITE 0x03
#define CTRL_STATUS_READ 0x05
#define CTRL_COMMAND_WRITE 0x13
#define CTRL_DISPLAY_READ 0x15

// Commands and Parameters
#define CMD_NOP 0x00     /// No-op register

// System control
#define CMD_SYSTEMSET 0x40  // Initialize device and display (8 Bytes)
#define CMD_SLEEPIN 0x53  // Enter standby mode (0 Bytes)

// Display control
#define CMD_DISP_OFF 0x58  // Enable and disable display and display flashing (1 Byte)
#define CMD_DISP_ON 0x59  // Enable and disable display and display flashing (1 Byte)

#define CMD_SCROLL 0x44  // Set display start address and display regions (10 Bytes)

#define CMD_CSRFORM 0x5D  // Set cursor type (2 Bytes)

#define CMD_CGRAM_ADR 0x5C  // Set start address of character generator RAM (2 Bytes)

#define CMD_CSRDIR_RIGHT 0x4C  // Set direction of cursor movement (0 Bytes)
#define CMD_CSRDIR_LEFT 0x4D  // Set direction of cursor movement (0 Bytes)
#define CMD_CSRDIR_UP 0x4E  // Set direction of cursor movement (0 Bytes)
#define CMD_CSRDIR_DOWN 0x4F  // Set direction of cursor movement (0 Bytes)

#define CMD_HDOT_SCR 0x5A  // Set horizontal scroll position (1 Bytes)

#define CMD_OVLAY 0x5B  // Set display overlay format (1 Bytes)

// Drawing control
#define CMD_CSRW 0x46  // Set cursor address (2 Bytes)

#define CMD_CSRR 0x47  // Read cursor address (2 Bytes)

#define CMD_MWRITE 0x42  // Write to display memory (- Bytes)

#define CMD_MREAD 0x43  // Read from display memory (- Bytes)




class SED1330_GFX : public Print
{
  public:
    SED1330_Connection LCD_Connection;
    SED1330_Parameters LCD_Parameters;

    // Constructors
    SED1330_GFX(byte i2cAddress=0x20);
    SED1330_GFX(byte RESET, byte RD, byte WR, byte CS, byte A0, byte D0, byte D1, byte D2, byte D3, byte D4, byte D5, byte D6, byte D7);

    // Methods
    void begin();
    void ClearDisplay();
    void Init();
    Point Location();
    void MoveTo(byte x, byte y);
    //void print(char* text);
    //void println(char* text);
    byte ReadData();
    void Reset();
    void SetCursorDirection(cursor_directions cDir);
    void SetParameters(unsigned int LCD_WidthInPixels,
                       unsigned int LCD_HeightInPixels,
                       bool LCD_IsDualDisplay,
                       bool LCD_IsPositiveDisplay,
                       byte CharacterWidthInPixels,
                       byte CharacterHeightInPixels,
                       unsigned int MemoryLayerStart_1,
                       unsigned int MemoryLayerSize_1,
                       unsigned int MemoryLayerStart_2,
                       unsigned int MemoryLayerSize_2,
                       unsigned int MemoryLayerStart_3,
                       unsigned int MemoryLayerSize_3,
                       unsigned int MemoryLayerStart_4,
                       unsigned int MemoryLayerSize_4,
                       bool UseExternalCharacterGeneratorROM,
                       unsigned int LCD_WidthInCharacters,
                       bool UseSED1336TvMode,
                       bool Use2FrameAcDrive,
                       byte CursorFlashRate,
                       byte ScreenFlashRate1,
                       byte ScreenFlashRate2,
                       byte ScreenFlashRate3,
                       byte CursorWidthInPixels,
                       byte CursorHeightInPixels,
                       bool CursorBlock,
                       byte ScreenCompositionMethod,
                       bool DisplayOnlyGraphics);
    void Test();
    void WriteCommand(byte command);
    void WriteData(byte data);
    
    size_t write(uint8_t);
    //size_t write(const uint8_t *buffer, size_t size);    
    using Print::write; // pull in write(str) and write(buf, size) from Print


  private:
    bool _pinsSet4Write = false;
    byte _lastCommand = 0;
    bool _connectionSetForRead = 0;
    cursor_directions _cursorDirection = unknown_direction;
    unsigned int lastMemPosText;
    unsigned int lastMemPosGraphics;

    // Private Methods
    void lcdClearDisplay(unsigned int StartAddress, unsigned int BlockSize, int SAD);
    byte lcdReadJustData();
    void lcdWriteControlAndData(byte command, byte data);
    void lcdWriteJustData(byte data);
    void setupConnection4Read();
    void setupConnection4Write();

};


#endif // _SED1330_GFX_
