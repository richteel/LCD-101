
#ifndef _SED1330_SETTINGS_H_
#define _SED1330_SETTINGS_H_

#define I2C_IODIR_REGISTER_A 0x00
#define I2C_IODIR_REGISTER_B 0x01
#define I2C_PORT_A 0x12
#define I2C_PORT_B 0x13

enum cursor_directions {
  unknown_direction,
  right,
  left,
  up,
  down
};

enum connection_types {
  unknown,
  parallel,
  spi,
  i2c
};

struct SED1330_Connection {
  connection_types ConnectionType = i2c;
  byte RESET = 16;
  byte RD = 3;
  byte WR = 17;
  byte CS = 14;
  byte A0 = 15;
  byte D0 = 8;
  byte D1 = 9;
  byte D2 = 10;
  byte D3 = 11;
  byte D4 = 4;
  byte D5 = 5;
  byte D6 = 6;
  byte D7 = 7;
  byte I2cAddress = 0x20;
};

struct Point {
  byte x = 0;
  byte y = 0;
  unsigned int address = 0;
};

struct SED1330_Parameters {
  unsigned int LCD_WidthInPixels = 0x100;
  unsigned int LCD_HeightInPixels = 0x80;
  bool LCD_IsDualDisplay = false;
  bool LCD_IsPositiveDisplay = true;
  byte CharacterWidthInPixels = 0x8;
  byte CharacterHeightInPixels = 0x8;
  unsigned int MemoryLayerStart_1 = 0x0;
  unsigned int MemoryLayerSize_1 = 0x200;
  unsigned int MemoryLayerStart_2 = 0x400;
  unsigned int MemoryLayerSize_2 = 0x1000;
  unsigned int MemoryLayerStart_3 = 0x200;
  unsigned int MemoryLayerSize_3 = 0x200;
  unsigned int MemoryLayerStart_4 = 0x0;
  unsigned int MemoryLayerSize_4 = 0x0;
  bool UseExternalCharacterGeneratorROM = false;
  unsigned int LCD_WidthInCharacters = 0x20;
  unsigned int LCD_HeightInCharacters = 0x10;
  bool UseSED1336TvMode = false;
  bool Use2FrameAcDrive = true;
  byte CursorFlashRate = 0x2;
  byte ScreenFlashRate1 = 0x1;
  byte ScreenFlashRate2 = 0x1;
  byte ScreenFlashRate3 = 0x1;
  byte CursorWidthInPixels = 0x5;
  byte CursorHeightInPixels = 0x7;
  bool CursorBlock = true;
  byte ScreenCompositionMethod = 0x1;
  bool DisplayOnlyGraphics = false;
  

};

#endif // _SED1330_SETTINGS_H_
