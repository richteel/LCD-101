// SED1330.h

#ifndef _SED1330_
#define _SED1330_

#if (ARDUINO >= 100)
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#include "SED1330_Commands.h"
#include "SED1330_Connection.h"
#include "SED1330_Setting.h"
#include "Wire.h"

struct CursorDirections {
  enum Type {
    Unknown,
    Right,
    Left,
    Up,
    Down
  };
  Type t_;
  CursorDirections(Type t) : t_(t) {}
  operator Type () const {
    return t_;
  }
};

struct Point {
  byte x = 0;
  byte y = 0;
  unsigned int address = 0;
};

class SED1330 : public Print
{
  public:
    // *********************************
    // ********* Support Print *********
    // *********************************
    size_t write(uint8_t);
    // *********************************
    
    SED1330_Connection Connection = {ConnectionTypes::I2C, 16, 3, 17, 14, 15, 8, 9, 10, 11, 4, 5, 6, 7, 0x20};
    SED1330_Setting Setting = {256, 128, 32, 16, 8, 8, true};

    // Constructors
    SED1330(byte i2cAddress = 0x20);
    SED1330(byte RESET, byte RD, byte WR, byte CS, byte A0, byte D0, byte D1, byte D2, byte D3, byte D4, byte D5, byte D6, byte D7);
    SED1330(SED1330_Connection connection);

    // Constructor Helpers
    void SetSED1330(byte i2cAddress = 0x20);
    void SetSED1330(SED1330_Connection connection);

    // Methods
    void begin();
    void begin(SED1330_Setting setting);
    void ClearDisplay();
    void Init();
    void InvertDisplay(bool invert);
    void Reset();
    Point Location();
    void MoveTo(byte x, byte y);
    void SetCursorDirection(CursorDirections cDir);

    // Low Level Operations
    byte ReadData();                  // Display data and cursor address read
    byte ReadStatus();                // Status flag read
    void WriteCommand(byte command);  // Command write
    void WriteData(byte data2Write);        // Display data and parameter write

  private:
    // Fields
    CursorDirections _cursorDirection = CursorDirections::Unknown;
    bool _pinsSet4Write = false;
    byte _lastCommand = 0;
    byte _lastControl = 0;
    bool _displayInverted = false;

    // Methods
    void clearDisplayScreen(unsigned int StartAddress, unsigned int BlockSize, byte data2Write);
    void setupConnection(bool connectionWrite);

    // Low Level Operations
    byte readInfo();
    byte readJustData();
    void writeInfo(byte data2Write);
    void writeJustData(byte data2Write);
};


#endif // _SED1330_
