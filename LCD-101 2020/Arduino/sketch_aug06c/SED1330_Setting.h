#ifndef _SED1330_Setting_
#define _SED1330_Setting_

struct SED1330_Setting {
  unsigned int WidthInPixels;
  unsigned int HeightInPixels;
  unsigned int WidthInCharacters;
  unsigned int HeightInCharacters;
  byte CharacterWidthInPixels;
  byte CharacterHeightInPixels;
  bool DisplayOnlyGraphics;
};

#endif // _SED1330_Setting_
