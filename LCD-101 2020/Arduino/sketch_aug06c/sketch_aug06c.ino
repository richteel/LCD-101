#include "SED1330.h"

//SED1330 tft();  // I2C Connection Default is 0x20
//SED1330 tft(0x20);  // I2C Connection
SED1330 tft(16, 3, 17, 14, 15, 8, 9, 10, 11, 4, 5, 6, 7); // Parallel Connection

bool inverted = false;
unsigned long previousMillis = 0;        // will store last time screen was inverted
const long interval = 15000;           // interval at which to blink (milliseconds)

void test() {
  tft.println(F("I've been alone with you inside my mind"));

  Point p = tft.Location();

  Serial.print("Cursor Address: 0x");
  Serial.print(p.address < 4096 ? "0" : "");
  Serial.print(p.address < 256 ? "0" : "");
  Serial.print(p.address < 16 ? "0" : "");
  Serial.print(p.address, HEX);
  Serial.print(F(" ("));
  Serial.print(p.address);
  Serial.println(F(")"));

  Serial.print("Cursor X: 0x");
  Serial.print(p.x < 16 ? "0" : "");
  Serial.print(p.x, HEX);
  Serial.print(F(" ("));
  Serial.print(p.x);
  Serial.println(F(")"));

  Serial.print("Cursor Y: 0x");
  Serial.print(p.y < 16 ? "0" : "");
  Serial.print(p.y, HEX);
  Serial.print(F(" ("));
  Serial.print(p.y);
  Serial.println(F(")"));

  tft.println(F("And in my dreams I've kissed your lips a thousand times"));
  tft.println(F("I sometimes see you pass outside my door"));
  tft.println(F("Hello, is it me you're looking for?"));
  tft.println(F(""));
  tft.println(F("I can see it in your eyes, I can see it in your smile"));
  tft.println(F("You're all I've ever wanted and my arms are open wide"));
  tft.println(F("'Cause you know just what to say, and you know just what to do"));
  /*
  tft.println(F("And I want to tell you so much, I love you"));
    tft.println(F(""));
    tft.println(F("I long to see the sunlight in your hair"));
    tft.println(F("And tell you time and time again how much I care"));
    tft.println(F("Sometimes I feel my heart will overflow"));
  */
}

void setup() {
  // put your setup code here, to run once:
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  Serial.begin(9600);
  Serial.println(F("TeelSys_SED1330 Test!"));

  //tft.Setting = {256, 128, 32, 16, 8, 8, false};
  tft.begin({256, 128, 32, 16, 8, 8, false});

  Serial.println(F("Begin Done!"));
  digitalWrite(LED_BUILTIN, LOW);

  test();

  Serial.println(F("Test Done!"));
}

void loop() {
  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis >= interval) {
    // save the last time you inverted the screen
    previousMillis = currentMillis;

    inverted = !inverted;

    tft.InvertDisplay(inverted);
  }
}
