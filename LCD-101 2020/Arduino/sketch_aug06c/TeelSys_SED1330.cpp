/*!
   @file TeelSys_SED1330.cpp

   This is part of TeelSys SED1330 driver for the Arduino platform.  It is
   designed specifically to work with All Electronics LCD-101.

   TeeSys invests time and resources providing this open source code,
   please support TeeSys and open-source hardware by purchasing
   products from TeeSys!

   Written by Richard Teel for TeeSys LLC.

   BSD license, all text here must be included in any redistribution.

  REF: I2C http://tronixstuff.com/2011/08/26/tutorial-maximising-your-arduinos-io-ports/
*/

#include "TeelSys_SED1330.h"


// SOME DEFINES AND STATIC VARIABLES USED INTERNALLY -----------------------


#define sed1330_swap(a, b)                                                     \
  (((a) ^= (b)), ((b) ^= (a)), ((a) ^= (b))) ///< No-temp-var swap operation






// ***************************************
// Public Methods
// ***************************************
// Constructors
TeelSys_SED1330::TeelSys_SED1330(uint8_t w, uint8_t h, byte i2cAddress=0x20) :  Adafruit_GFX(w, h) {
  _tft.SetSED1330(i2cAddress);
}

TeelSys_SED1330::TeelSys_SED1330(uint8_t w, uint8_t h, byte RESET, byte RD, byte WR, byte CS, byte A0, byte D0, byte D1, byte D2, byte D3, byte D4, byte D5, byte D6, byte D7) :  Adafruit_GFX(w, h)
{
  _tft.SetSED1330((SED1330_Connection) {
    ConnectionTypes::Parallel, RESET, RD, WR, CS, A0, D0, D1, D2, D3, D4, D5, D6, D7, 0x20
  });
}

TeelSys_SED1330::TeelSys_SED1330(uint8_t w, uint8_t h, SED1330_Connection connection) :  Adafruit_GFX(w, h) {
  _tft.SetSED1330(connection);
}

// Methods
void TeelSys_SED1330::begin(SED1330_Setting setting = {256, 128, 32, 16, 8, 8, true}) {
  _tft.begin(setting);
}

// *************** START ***************
//         From Adafruit_GFX
// *************************************
// TRANSACTION API / CORE DRAW API
// These MAY be overridden by the subclass to provide device-specific
// optimized code.  Otherwise 'generic' versions are used.
void TeelSys_SED1330::drawPixel(int16_t x, int16_t y, uint16_t color) {
  if ((x >= 0) && (x < width()) && (y >= 0) && (y < height())) {
    // Pixel is in-bounds. Rotate coordinates if needed.
    switch (getRotation()) {
    case 1:
      sed1330_swap(x, y);
      x = WIDTH - x - 1;
      break;
    case 2:
      x = WIDTH - x - 1;
      y = HEIGHT - y - 1;
      break;
    case 3:
      sed1330_swap(x, y);
      y = HEIGHT - y - 1;
      break;
    }
    switch (color) {
    case SED1330_WHITE:
      buffer[x + (y / 8) * WIDTH] |= (1 << (y & 7));
      break;
    case SED1330_BLACK:
      buffer[x + (y / 8) * WIDTH] &= ~(1 << (y & 7));
      break;
    case SED1330_INVERSE:
      buffer[x + (y / 8) * WIDTH] ^= (1 << (y & 7));
      break;
    }
  }
}

//void TeelSys_SED1330::startWrite(void) { }

// void TeelSys_SED1330::writePixel(int16_t x, int16_t y, uint16_t color) { }

//void TeelSys_SED1330::writeFillRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color) { }

// void TeelSys_SED1330::writeFastVLine(int16_t x, int16_t y, int16_t h, uint16_t color) { }

// void TeelSys_SED1330::writeFastHLine(int16_t x, int16_t y, int16_t w, uint16_t color) { }

// void TeelSys_SED1330::writeLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t color) { }

// void TeelSys_SED1330::endWrite(void) { }


// CONTROL API
// These MAY be overridden by the subclass to provide device-specific
// optimized code.  Otherwise 'generic' versions are used.
//void TeelSys_SED1330::setRotation(uint8_t r) { }

void TeelSys_SED1330::invertDisplay(bool i) {
  _tft.InvertDisplay(i);
}


// BASIC DRAW API
// These MAY be overridden by the subclass to provide device-specific
// optimized code.  Otherwise 'generic' versions are used.

// It's good to implement those, even if using transaction API
void TeelSys_SED1330::drawFastVLine(int16_t x, int16_t y, int16_t h, uint16_t color) {

}

void TeelSys_SED1330::drawFastHLine(int16_t x, int16_t y, int16_t w, uint16_t color) {

}

//void TeelSys_SED1330::fillRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color) { }

//void TeelSys_SED1330::fillScreen(uint16_t color) { }

// Optional and probably not necessary to change
//void TeelSys_SED1330::drawLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t color) { }

// void TeelSys_SED1330::drawRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color) { }
// *************************************
//         From Adafruit_GFX
// **************** END ****************
