// TeelSys_SED1330.h

#ifndef _TeelSys_SED1330_
#define _TeelSys_SED1330_

#include <Adafruit_GFX.h>
#include <Wire.h>
#include "SED1330.h"



/// The following "raw" color names are kept for backwards client compatability
/// They can be disabled by predefining this macro before including the Adafruit
/// header client code will then need to be modified to use the scoped enum
/// values directly
#ifndef NO_ADAFRUIT_SED1330_COLOR_COMPATIBILITY
#define BLACK SED1330_BLACK     ///< Draw 'off' pixels
#define WHITE SED1330_WHITE     ///< Draw 'on' pixels
#define INVERSE SED1330_INVERSE ///< Invert pixels
#endif
/// fit into the SED1330_ naming scheme
#define SED1330_BLACK 0   ///< Draw 'off' pixels
#define SED1330_WHITE 1   ///< Draw 'on' pixels
#define SED1330_INVERSE 2 ///< Invert pixels




class TeelSys_SED1330 : public Adafruit_GFX
{
  public:
    SED1330_Connection Connection = {ConnectionTypes::I2C, 16, 3, 17, 14, 15, 8, 9, 10, 11, 4, 5, 6, 7, 0x20};
    SED1330_Setting Setting = {256, 128, 32, 16, 8, 8, true};

    // Constructors
    TeelSys_SED1330(uint8_t w, uint8_t h, byte i2cAddress = 0x20);
    TeelSys_SED1330(uint8_t w, uint8_t h, byte RESET, byte RD, byte WR, byte CS, byte A0, byte D0, byte D1, byte D2, byte D3, byte D4, byte D5, byte D6, byte D7);
    TeelSys_SED1330(uint8_t w, uint8_t h, SED1330_Connection connection);

    // Methods
    void begin(SED1330_Setting setting = {256, 128, 32, 16, 8, 8, true});

    // *************** START ***************
    //         From Adafruit_GFX
    // *************************************
    // TRANSACTION API / CORE DRAW API
    // These MAY be overridden by the subclass to provide device-specific
    // optimized code.  Otherwise 'generic' versions are used.
    void drawPixel(int16_t x, int16_t y, uint16_t color);
    //void startWrite(void);
    //void writePixel(int16_t x, int16_t y, uint16_t color);
    //void writeFillRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color);
    //void writeFastVLine(int16_t x, int16_t y, int16_t h, uint16_t color);
    //void writeFastHLine(int16_t x, int16_t y, int16_t w, uint16_t color);
    //void writeLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t color);
    //void endWrite(void);

    // CONTROL API
    // These MAY be overridden by the subclass to provide device-specific
    // optimized code.  Otherwise 'generic' versions are used.
    //void setRotation(uint8_t r);
    void invertDisplay(bool i);

    // BASIC DRAW API
    // These MAY be overridden by the subclass to provide device-specific
    // optimized code.  Otherwise 'generic' versions are used.

    // It's good to implement those, even if using transaction API
    void drawFastVLine(int16_t x, int16_t y, int16_t h, uint16_t color);
    void drawFastHLine(int16_t x, int16_t y, int16_t w, uint16_t color);
    //void fillRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color);
    //void fillScreen(uint16_t color);
    // Optional and probably not necessary to change
    //void drawLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t color);
    //void drawRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color);


    // *************************************
    //         From Adafruit_GFX
    // **************** END ****************


  private:
    // Fields
    SED1330 _tft;
    uint8_t *buffer;

    // Methods
    void sed1330_command1(uint8_t c);

};



#endif // _TeelSys_SED1330_
