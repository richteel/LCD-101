// SED1330_Connection.h

#ifndef _SED1330_Connection_
#define _SED1330_Connection_

// **************** START ****************
// Microchip MCP23017 Registers and Ports
// ***************************************
#define I2C_IODIR_REGISTER_A 0x00
#define I2C_IODIR_REGISTER_B 0x01
#define I2C_PORT_A 0x12
#define I2C_PORT_B 0x13

#define I2C_CONTROL_READ_DATA 0x15
#define I2C_CONTROL_READ_STATUS 0x05
#define I2C_CONTROL_WRITE_COMMAND 0x13
#define I2C_CONTROL_WRITE_DATA 0x03
// ***************************************
// Microchip MCP23017 Registers and Ports
// ***************** END *****************

struct ConnectionTypes {
  enum Type {
    Unknown,
    Parallel,
    SPI,
    I2C
  };
  Type t_;
  ConnectionTypes(Type t) : t_(t) {}
  operator Type () const {
    return t_;
  }
};

struct SED1330_Connection {
  ConnectionTypes ConnectionType;
  byte RESET;
  byte RD;
  byte WR;
  byte CS;
  byte A0;
  byte D0;
  byte D1;
  byte D2;
  byte D3;
  byte D4;
  byte D5;
  byte D6;
  byte D7;
  byte I2cAddress;
};

#endif // _SED1330_Connection_
